package banko;

import java.util.ArrayList;
import java.util.Random;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class MainApp extends Application {

    private ArrayList<Plade> plader = new ArrayList<>();
    private ArrayList<PladeGUI> pladeGUI = new ArrayList<>();
    private ArrayList<Integer> numbers = new ArrayList<>();
    private int count = 0;
    private Scene scene;
    private GridPane root;
    private Button btnDraw;
    private Label lblCount, lblNr;
    private boolean finished;
    private PladeGUI winner;
    
    public static void main(String[] args) {
        
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {

        scene = new Scene(createContent(), 1280, 720);
        stage.setScene(scene);
        stage.show();
        stage.setTitle("Bingo");
    }
    
    private Parent createContent() {

        finished = false;
        // root
        root = new GridPane();
        root.setPadding(new Insets(10));
        root.setVgap(20);
        root.setHgap(75);
        // bingo label
        // Label title = new Label("Bingo");
        // title.setFont(Font.font("IMPACT", FontWeight.BOLD, 72));
        // title.setTextFill(Color.DARKRED);
        // title.setAlignment(Pos.BASELINE_CENTER);
        // draw button and draw counter
        btnDraw = new Button("DRAW");
        btnDraw.setOnAction(event -> drawSingle());
        btnDraw.setFont(Font.font("IMPACT", FontWeight.BOLD, 36));
        lblNr = new Label("");
        lblNr.setFont(Font.font("IMPACT", FontWeight.BOLD, 48));
        lblNr.setTextFill(Color.GREEN);
        lblCount = new Label("");
        lblCount.setFont(Font.font("ARIAL", FontWeight.BOLD, 14));
        lblCount.setTextFill(Color.DARKRED);
        //
        VBox vbox = new VBox();
        vbox.getChildren().addAll(btnDraw, lblNr, lblCount);
        vbox.setAlignment(Pos.BASELINE_CENTER);
        vbox.setPadding(new Insets(250, 0, 0, 0));
        vbox.setSpacing(50);
        root.add(vbox, 0, 0, 1, 4);
        for (int i = 0; i < 4; i++) {
            Plade p = new Plade(i);
            plader.add(p);
            p.generateNumbers();
            p.printNumre();
            Label lblNavn = new Label(p.getNavn());
            lblNavn.setFont(Font.font("ARIAL", FontWeight.BOLD, 36));
            lblNavn.setTextFill(Color.GREY);
            lblNavn.setAlignment(Pos.BASELINE_CENTER);
            PladeGUI pg = new PladeGUI(p);
            pladeGUI.add(pg);
            root.add(lblNavn, 1, i);
            root.add(pg, 2, i);
        }
        BackgroundImage tableImage =
            new BackgroundImage(new Image("/banko/table.jpg/", 1280, 720, false, true),
                BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);
        Background table = new Background(tableImage);
        root.setBackground(table);
        return root;
    }
    
    public void drawSingle() {
        
        Random r = new Random();
        int nr = r.nextInt(90) + 1;
        while (numbers.contains(nr)) {
            nr = r.nextInt(90) + 1;
        }
        numbers.add(nr);
        count++;
        lblNr.setText("" + nr);
        if (nr == 90) {
            lblNr.setText("Ole!");
        }
        lblCount.setText("Numbers drawn: " + count);
        for (PladeGUI pg : pladeGUI) {
            if (pg.hasNumber(nr)) {
                if (pg.getPlade().isFuld()) {
                    finished = true;
                    winner = pg;
                }
            }
        }
        if (finished || count == 90) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setHeaderText("Game finished");
            if (winner != null) {
                alert.setContentText("BINGOOOOOOOOOOOOO!! " + winner.getPlade().getNavn()
                    + " har udfyldt pladen. Tillykke! ");
            }
            else {
                alert.setContentText("Ingen vinder fundet. Bedre held næste gang!");
            }
            alert.showAndWait();
            Platform.exit();
        }
    }
}
