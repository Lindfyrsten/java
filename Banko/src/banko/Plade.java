package banko;

import java.util.Random;

public class Plade {

    private boolean fuld;
    private int counter;
    private int[] numre;
    private String navn;

    public Plade(int i) {

        fuld = false;
        numre = new int[90];
        counter = 0;
        navn = "Plade " + (i + 1);
    }

    public boolean tjekNummer(int i) {

        if (numre[i - 1] != 0) {
            counter++;
            if (counter == 27) {
                fuld = true;
            }
            return true;
        }
        return false;
    }

    public void generateNumbers() {

        Random r = new Random();
        for (int i = 0; i < 27; i++) {
            int n = r.nextInt(90) + 1;
            while (numre[n - 1] > 0) {
                n = r.nextInt(90) + 1;
            }
            numre[n - 1] = n;
        }
    }

    public int[] getNumre() {
        
        return numre;
    }

    public void printNumre() {

        System.out.print(navn + "'s numre: ");
        for (int element : numre) {
            if (element != 0) {
                System.out.print(element + ", ");
            }
        }
        System.out.println("");
    }

    public boolean isFuld() {

        return fuld;
    }

    public String getNavn() {

        return navn;
    }

    public int getCounter() {
        
        return counter;
    }
}
