/**
 * @author Kristian Lindbjerg Aug 28, 2017
 */
package banko;
import java.util.ArrayList;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
public class PladeGUI extends GridPane {

	private Plade plade;
	private ArrayList<Label> numre = new ArrayList<>();

	public PladeGUI(Plade plade) {
		this.plade = plade;
		setStyle("-fx-border-style: solid inside;" + "-fx-border-width: 2;" + "-fx-border-radius: 10;"
				+ "-fx-border-color: black;");
		setup();
	}

	public void setup() {
		setHgap(50);
		setVgap(15);
		int row = 0;
		int count = 0;
		Label lblNr;
		for (int i = 0; i < plade.getNumre().length; i++) {
			int nr = plade.getNumre()[i];
			if (nr != 0) {
				lblNr = new Label("" + nr);
				lblNr.setFont(Font.font("ARIAL", FontWeight.BOLD, 36));
				lblNr.setTextFill(Color.GREEN);
				numre.add(lblNr);
				add(lblNr, count, row);
				count++;
			}
			if (count == 9) {
				count = 0;
				row++;
			}
		}
	}

	public boolean hasNumber(int nr) {
		boolean found = plade.tjekNummer(nr);
		if (found) {
			for (Label lbl : numre) {
				if (lbl.getText().equals(Integer.toString(nr))) {
					lbl.setTextFill(Color.RED);
				}
			}
		}
		return found;
	}

	public Plade getPlade() {
		return plade;
	}
}
