package opgave_Kannibal;
import java.util.NoSuchElementException;
/**
 * An implementation of a queue as a circular array.
 */
public class EatingQueueList {

	private Node head, tail, startNode;
	private int currentSize;

	/**
	 * Constructs an empty queue.
	 */
	public EatingQueueList() {
		head = null;
		tail = null;
		currentSize = 0;
	}

	/**
	 * Adds an person to the tail of this queue.
	 *
	 * @param p
	 *            the person to add
	 */
	public void addPerson(Person p) {
		Node newNode = new Node();
		newNode.data = p;
		if (head == null) {
			head = newNode;
		} else {
			tail.next = newNode;
		}
		tail = newNode;
		tail.next = head;
		currentSize++;
	}

	/**
	 * Prints the persons on the list in order of index
	 */
	public void print() {
		if (currentSize > 0) {
			Node temp = head;
			for (int i = currentSize; i > 0; i--) {
				if (startNode != null && temp == startNode.next.next.next.next.next) {
					System.out.print("*" + temp.data + "* ");
				} else {
					System.out.print(temp.data + "  ");
				}
				temp = temp.next;
			}
		}
		System.out.println("");
	}

	/**
	 * Selects a random person as startPerson
	 *
	 * @return
	 */
	public Node randomStart() {
		if (currentSize > 0) {
			int start = (int) (Math.random() * (currentSize));
			startNode = head;
			while (start > 0) {
				startNode = startNode.next;
				start--;
			}
		}
		return startNode;
	}

	/**
	 * Removes a person from the head of this queue.
	 *
	 * @return the removed person
	 */
	public Person eatPerson(int count) {
		if (currentSize == 0) {
			throw new NoSuchElementException("Ingen personer på menuen");
		}
		if (startNode == null) {
			throw new NoSuchElementException("Ingen startperson valgt");
		}
		Person removed = null;
		if (currentSize == 1) {
			removed = head.data;
			head = null;
			tail = null;
		} else {
			for (int i = 0; i < count - 1; i++) {
				startNode = startNode.next;
			}
			removed = startNode.next.data;
			if (startNode.next == head) {
				head = startNode.next.next;
			}
			startNode.next = startNode.next.next;
			startNode = startNode.next;
		}
		currentSize--;
		return removed;
	}
	class Node {

		public Person data;
		public Node next;
	}
}
