package opgave_Kannibal;
import java.util.NoSuchElementException;
/**
 * An implementation of a queue as a circular array.
 */
public class EatingQueueArray {

	private Person[] persons;
	private int currentSize;
	private int start;

	/**
	 * Constructs an empty queue.
	 */
	public EatingQueueArray() {
		final int INITIAL_SIZE = 10;
		persons = new Person[INITIAL_SIZE];
		currentSize = 0;
		start = -2;
	}

	/**
	 * Adds an person to the tail of this queue.
	 *
	 * @param p
	 *            the person to add
	 */
	public void addPerson(Person p) {
		growIfNecessary();
		persons[currentSize] = p;
		currentSize++;
	}

	/**
	 * Prints the persons on the list in order of index
	 */
	public void print() {
		System.out.print("[  ");
		for (int i = 0; i < currentSize; i++) {
			if (start != -2 && i == (start + 5) % currentSize) {
				System.out.print("*" + persons[i].getNavn() + "*  ");
			} else {
				System.out.print(persons[i].getNavn() + "  ");
			}
		}
		System.out.println("]");
	}

	public Person randomStart() {
		if (currentSize > 0) {
			start = (int) (Math.random() * (currentSize));
			return persons[start];
		}
		return null;
	}

	/**
	 * Removes a person from the head of this queue.
	 *
	 * @return the removed person
	 */
	public Person eatPerson(int count) {
		if (currentSize == 0) {
			throw new NoSuchElementException("Ingen personer i kø");
		}
		if (start == -2) {
			throw new NoSuchElementException("Ingen startperson valgt");
		}
		start = (start + count) % currentSize;
		Person removed = persons[start];
		for (int i = start; i < currentSize; i++) {
			persons[i] = persons[i + 1];
		}
		currentSize--;
		return removed;
	}

	/**
	 * Grows the element array if the current size equals the capacity.
	 */
	private void growIfNecessary() {
		if (currentSize == persons.length) {
			Person[] newElements = new Person[2 * persons.length];
			for (int i = 0; i < persons.length; i++) {
				newElements[i] = persons[i];
			}
			persons = newElements;
		}
	}
}
