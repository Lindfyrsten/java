package bagopgave;
public class LinkedBag<T> implements Bag<T> {

	private Node first;
	// private Node tail;
	private int currentSize = 0;
	private final int MAX_SIZE = 10;

	public LinkedBag() {
		first = null;
	}

	@Override
	public int getCurrentSize() {
		return currentSize;
	}

	@Override
	public boolean isFull() {
		return currentSize == MAX_SIZE;
	}

	@Override
	public boolean isEmpty() {
		return currentSize == 0;
	}

	@Override
	public boolean add(T newEntry) {
		boolean added = false;
		if (currentSize < MAX_SIZE) {
			Node newNode = new Node();
			newNode.data = newEntry;
			newNode.next = first;
			first = newNode;
			currentSize++;
			added = true;
		}
		return added;
	}

	@Override
	public T remove() {
		T t = null;
		if (currentSize > 0) {
			t = first.data;
			first = first.next;
			currentSize--;
		}
		return t;
	}

	@Override
	public boolean remove(T anEntry) {
		boolean found = false;
		if (first != null) {
			if (first.data.equals(anEntry)) {
				first = first.next;
			} else {
				Node temp = first;
				while (!found && temp.next != null) {
					if (temp.next.data.equals(anEntry)) {
						found = true;
					} else {
						temp = temp.next;
					}
				}
				if (found) {
					Node temp2 = temp.next;
					temp.next = temp2.next;
					temp2.next = null;
					currentSize--;
				}
			}
		}
		return found;
	}

	@Override
	public void clear() {
		first = null;
		currentSize = 0;
	}

	@Override
	public int getFrequencyOf(T anEntry) {
		int count = 0;
		Node temp = first;
		while (temp != null) {
			if (temp.data.equals(anEntry)) {
				count++;
			}
			temp = temp.next;
		}
		return count;
	}

	@Override
	public boolean contains(T anEntry) {
		boolean found = false;
		Node temp = first;
		while (!found && temp != null) {
			if (temp.data.equals(anEntry)) {
				found = true;
			}
			temp = temp.next;
		}
		return found;
	}

	@Override
	public T[] toArray() {
		@SuppressWarnings("unchecked")
		T[] newArray = (T[]) new Object[currentSize];
		Node temp = first;
		for (int i = 0; i < currentSize; i++) {
			newArray[i] = temp.data;
			i++;
		}
		return newArray;
	}
	private class Node {

		Node next;
		T data;
	}
}
