package setoperationsopgave;
import java.util.HashSet;
import java.util.Set;
public class SetOperations {

	public static <T> Set<T> union(Set<T> s1, Set<T> s2) {
		Set<T> newSet = new HashSet<>();
		for (T type : s1) {
			if (s2.contains(type)) {
				newSet.add(type);
			}
		}
		return newSet;
	}

	public static <T> Set<T> differens(Set<T> s1, Set<T> s2) {
		Set<T> newSet = new HashSet<>();
		for (T type : s1) {
			if (!s2.contains(type)) {
				newSet.add(type);
			}
		}
		for (T type : s2) {
			if (!s1.contains(type)) {
				newSet.add(type);
			}
		}
		return newSet;
	}

	public static <T> Set<T> intesection(Set<T> s1, Set<T> s2) {
		Set<T> newSet = new HashSet<>();
		for (T type : s1) {
			newSet.add(type);
		}
		for (T type : s2) {
			if (!newSet.contains(type)) {
				newSet.add(type);
			}
		}
		return newSet;
	}
}
