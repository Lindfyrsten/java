package person;
public class Navn implements Comparable<Navn> {

	private String fornavn, efternavn;

	public Navn(String fornavn, String efternavn) {
		this.fornavn = fornavn;
		this.efternavn = efternavn;
	}

	public String getFornavn() {
		return fornavn;
	}

	public String getEfternavn() {
		return efternavn;
	}

	@Override
	public String toString() {
		return fornavn + " " + efternavn;
	}

	@Override
	public int compareTo(Navn o) {
		if (fornavn.compareTo(o.getFornavn()) == 0) {
			return efternavn.compareTo(o.getEfternavn());
		}
		return fornavn.compareTo(o.getFornavn());
	}
}
