package thread;
public class MainApp {

	public static void main(String[] args) {
		ThreadTest t1 = new ThreadTest("Per", 3);
		ThreadTest t2 = new ThreadTest("Ole", 1);
		ThreadTest t3 = new ThreadTest("Bent", 5);
		ThreadTest t4 = new ThreadTest("Søren", 8);
		t1.start();
		t2.start();
		t3.start();
		t4.start();
	}
}
