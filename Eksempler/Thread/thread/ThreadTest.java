package thread;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;
public class ThreadTest extends Thread {

	private String navn;
	private Timeline timer;
	private int seconds;

	public ThreadTest(String navn, int seconds) {
		this.navn = navn;
		this.seconds = seconds;
	}

	public void printName() {
		System.out.println(navn);
	}

	@Override
	public void run() {
		timer = new Timeline(new KeyFrame(Duration.seconds(seconds)));
		timer.play();
		timer.setOnFinished(event -> printName());
	}
}
