package exceptions;
public class NewException extends Exception {

	public NewException() {
	}

	public NewException(String message) {
		super();
	}

	public NewException(Throwable cause) {
		super();
	}

	public NewException(String message, Throwable cause) {
		super();
	}
}
