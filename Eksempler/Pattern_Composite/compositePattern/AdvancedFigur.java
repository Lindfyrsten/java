package compositePattern;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
public class AdvancedFigur extends Figur {

	private List<Figur> list;

	public AdvancedFigur(String navn) {
		super(navn);
		list = new ArrayList<>();
	}

	public AdvancedFigur() {
		super("Samling");
		list = new ArrayList<>();
	}

	@Override
	public void tegn() {
		System.out.println(getNavn() + " - " + Arrays.toString(list.toArray()));
		for (Figur f : list) {
			f.tegn();
		}
	}

	@Override
	public double getAreal() {
		double result = 0;
		for (Figur f : list) {
			result += f.getAreal();
		}
		return result;
	}

	@Override
	public void add(Figur figur) {
		list.add(figur);
	}

	@Override
	public void remove(Figur figur) {
		list.remove(figur);
	}
}
