package compositePattern;
public abstract class Figur {

	private String navn;

	public Figur(String navn) {
		this.navn = navn;
	}

	public void add(Figur figur) {
		throw new UnsupportedOperationException();
	}

	public void remove(Figur figur) {
		throw new UnsupportedOperationException();
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public abstract void tegn();

	public abstract double getAreal();

	@Override
	public String toString() {
		return navn;
	}
}
