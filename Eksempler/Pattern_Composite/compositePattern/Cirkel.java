package compositePattern;
public class Cirkel extends SimpleFigur {

	public Cirkel(double radius) {
		super("Cirkel", Math.PI * Math.pow(radius, 2));
	}
}
