package compositePattern;
public class MainApp {

	public static void main(String[] args) {
		Figur mainTegning = new AdvancedFigur("Hele tegning");
		Figur simple1 = new Trekant(30, 30);
		Figur simple2 = new Kvadrat(280);
		Figur simple3 = new Trekant(30, 60);
		Figur simple4 = new Cirkel(400);
		Figur simple5 = new Rectangel(500, 5);
		Figur simple6 = new Kvadrat(450);
		Figur simple7 = new Trekant(22, 90);
		Figur simple8 = new Trekant(333, 9);
		Figur advanced1 = new AdvancedFigur();
		advanced1.add(simple4);
		advanced1.add(simple5);
		Figur advanced2 = new AdvancedFigur();
		advanced2.add(simple1);
		advanced2.add(simple2);
		Figur advanced3 = new AdvancedFigur();
		advanced3.add(advanced2);
		advanced3.add(advanced1);
		mainTegning.add(simple3);
		mainTegning.add(simple6);
		mainTegning.add(simple7);
		mainTegning.add(simple8);
		mainTegning.add(advanced3);
		mainTegning.tegn();
		System.out.println("Samlet areal = " + String.format("%.2f", mainTegning.getAreal()));
	}
}
