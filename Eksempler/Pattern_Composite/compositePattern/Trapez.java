package compositePattern;
public class Trapez extends SimpleFigur {

	public Trapez(double widthTop, double widthBot, double height) {
		super("Trapez", (0.5 * (widthTop + widthBot) * height));
	}
}
