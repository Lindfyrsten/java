package compositePattern;
public class SimpleFigur extends Figur {

	private double areal;

	public SimpleFigur(String navn, double areal) {
		super(navn);
		this.areal = areal;
	}

	@Override
	public void tegn() {
		System.out.println(getNavn());
	}

	@Override
	public double getAreal() {
		return areal;
	}
}
