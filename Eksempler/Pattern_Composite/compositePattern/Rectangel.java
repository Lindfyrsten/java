package compositePattern;
public class Rectangel extends SimpleFigur {

	public Rectangel(double height, double length) {
		super("Rectangel", height * length);
	}
}
