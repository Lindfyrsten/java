package compositePattern;
public class Kvadrat extends SimpleFigur {

	public Kvadrat(double side) {
		super("Kvadrat", side * side);
	}
}
