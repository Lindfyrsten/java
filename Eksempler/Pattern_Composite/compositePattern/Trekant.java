package compositePattern;
public class Trekant extends SimpleFigur {

	public Trekant(double height, double length) {
		super("Trekant", 0.5 * height * length);
	}
}
