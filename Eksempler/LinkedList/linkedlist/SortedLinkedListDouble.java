package linkedlist;
public class SortedLinkedListDouble {

	private Node first;
	private Node last;
	private int size;

	public SortedLinkedListDouble() {
		first = new Node();
		last = new Node();
		first.next = last;
		last.prev = first;
		size = 0;
	}

	/**
	 * Tilføjer e til listen, så listen fortsat er sorteret i henhold til den naturlige ordning på elementerne
	 */
	public void addElement(String e) {
		Node newNode = new Node();
		newNode.data = e;
		if (size > 0) {
			Node temp = first.next;
			boolean found = false;
			while (!found && temp != last) {
				if (e.compareTo(temp.data) <= 0) {
					temp.prev.next = newNode;
					newNode.next = temp;
					newNode.prev = temp.prev;
					temp.prev = newNode;
					found = true;
				} else {
					temp = temp.next;
				}
			}
			if (!found) {
				newNode.next = last;
				newNode.prev = temp.prev;
				temp.prev.next = newNode;
				last.prev = newNode;
			}
		} else {
			first.next = newNode;
			newNode.prev = first;
			newNode.next = last;
			last.prev = newNode;
		}
		size++;
	}

	/**
	 * Udskriver elementerne fra listen i sorteret rækkefølge
	 */
	public void udskrivElements() {
		if (size > 0) {
			Node temp = first;
			while (temp.next != last) {
				System.out.print(temp.next.data + "");
				temp = temp.next;
			}
		}
	}

	/**
	 * Returnerer antallet af elementer i listen
	 */
	public int countElements() {
		return size;
	}

	/**
	 * Fjerner det sidste (altså det største) element i listen. Listen skal fortsat være sorteret i henhold til den
	 * naturlige ordning på elementerne.
	 *
	 * @return true der blev fjernet et element blev fjernet ellers returneres false.
	 */
	public boolean removeLast() {
		boolean removed = false;
		if (size > 0) {
			Node toBeRemoved = last.prev;
			System.out.println(toBeRemoved.prev.data);
			System.out.println(toBeRemoved.data);
			last.prev = toBeRemoved.prev;
			toBeRemoved.prev.next = last;
			System.out.println(toBeRemoved.prev.next.data);
			System.out.println(last.prev.data);
			size--;
		}
		return removed;
	}

	/**
	 * Fjerner den første forekomst af e i listen. Listen skal fortsat være sorteret i henhold til den naturlige ordning
	 * på elementerne.
	 *
	 * @return true hvis e blev fjernet fra listen ellers returneres false.
	 */
	public boolean removeElement(String e) {
		boolean found = false;
		if (size > 0) {
			Node temp = first.next;
			while (!found && temp != last) {
				if (temp.data.equals(e)) {
					found = true;
				} else {
					temp = temp.next;
				}
			}
			if (found) {
				temp.prev.next = temp.next;
				temp.next.prev = temp.prev;
			}
		}
		System.out.println("\nRemoved: " + e);
		return found;
	}
	class Node {

		public String data;
		public Node next;
		public Node prev;
	}
}
