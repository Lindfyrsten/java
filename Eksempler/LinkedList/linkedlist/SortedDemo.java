package linkedlist;
public class SortedDemo {

	public static void main(String[] args) {
		String a = "A";
		String b = "B";
		String c = "C";
		String d = "D";
		String e = "E";
		String f = "F";
		String g = "G";
		SortedLinkedListDouble list = new SortedLinkedListDouble();
		list.addElement(c);
		list.addElement(a);
		list.addElement(e);
		list.addElement(b);
		list.addElement(d);
		list.addElement(f);
		list.addElement(g);
		System.out.println("\nAntal: " + list.countElements());
		System.out.print("Elementer i listen: ");
		list.udskrivElements();
		System.out.println("");
		list.removeLast();
		System.out.println("\nAntal: " + list.countElements());
		System.out.print("Elementer i listen: ");
		list.udskrivElements();
		list.removeElement(d);
		System.out.print("Elementer i listen: ");
		list.udskrivElements();
		// System.out.println("\nAntal: " + list.countElements());
		// list.removeElement(e);
		// list.removeElement(d);
		// list.udskrivElements();
		// SortedLinkedListDouble list2 = new SortedLinkedListDouble();
		// list2.addElement(e);
		// list2.addElement(a);
		// list2.addElement(c);
		// list2.addElement(d);
		// list2.addElement(g);
		// System.out.print("\nElementer i liste 2: ");
		// list2.udskrivElements();
		// // list2.addAll(list);
		// System.out.print("\nElementer i liste 2: ");
		// list2.udskrivElements();
	}
}
