package vare;
import java.util.ArrayList;
import spiritusadapter.Spiritus;
public class MainApp {

	public static void main(String[] args) {
		Vare v1 = new ElVare(300, "DiskMan");
		Vare v2 = new FoedeVare(2, "Banan");
		Vare v3 = new SpiritusToVareAdapter(new Spiritus(120, "GAJOL"));
		Vare v4 = new ElVare(5, "Stik");
		ArrayList<Vare> varer = new ArrayList<>();
		varer.add(v1);
		varer.add(v2);
		varer.add(v3);
		varer.add(v4);
		for (Vare v : varer) {
			System.out.println(v.getNavn() + " - Pris: " + v.getPris() + " (moms: " + v.beregnMoms() + ")");
		}
	}
}
