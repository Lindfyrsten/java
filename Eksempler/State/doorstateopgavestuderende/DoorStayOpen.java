package doorstateopgavestuderende;
public class DoorStayOpen extends DoorState {

	private Door door;

	public DoorStayOpen(Door door) {
		this.door = door;
	}

	@Override
	public String toString() {
		return "STAY OPEN";
	}

	@Override
	public void click() {
		door.setState(door.getClosingState());
		door.getClosingTimer().play();
	}

	@Override
	public void timeout() {
		// do nothing
	}
}
