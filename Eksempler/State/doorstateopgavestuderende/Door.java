package doorstateopgavestuderende;
import java.util.ArrayList;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;
public class Door {

	private DoorState closedState, openState, openingState, closingState, stayOpenState;
	private DoorState state;
	private Timeline openTimer, openingTimer, closingTimer;
	private ArrayList<Observer> observers;

	public Door() {
		closedState = new DoorClosed(this);
		closingState = new DoorClosing(this);
		openState = new DoorOpen(this);
		openingState = new DoorOpening(this);
		stayOpenState = new DoorStayOpen(this);
		// Be sure that the door is in the right state at start
		openingTimer = new Timeline(new KeyFrame(Duration.millis(3000), ae -> complete()));
		openTimer = new Timeline(new KeyFrame(Duration.millis(2000), ae -> timeout()));
		closingTimer = new Timeline(new KeyFrame(Duration.millis(4000), ae -> complete()));
		observers = new ArrayList<>();
		state = closedState;
	}

	public DoorState getState() {
		return state;
	}

	// timer-getters...
	public Timeline getOpenTimer() {
		return openTimer;
	}

	public Timeline getOpeningTimer() {
		return openingTimer;
	}

	public Timeline getClosingTimer() {
		return closingTimer;
	}

	// state getters
	public DoorState getOpenState() {
		return openState;
	}

	public DoorState getOpeningState() {
		return openingState;
	}

	public DoorState getClosedState() {
		return closedState;
	}

	public DoorState getClosingState() {
		return closingState;
	}

	public DoorState getStayOpenState() {
		return stayOpenState;
	}

	// actions...
	public void timeout() {
		state.timeout();
	}

	public void complete() {
		state.complete();
	}

	public void click() {
		state.click();
	}

	// method called by state-objects to set state for door
	public void setState(DoorState state) {
		this.state = state;
		notifyObservers();
	}

	// methods to handle observers...
	public void addObserver(Observer obs) {
		observers.add(obs);
	}

	private void notifyObservers() {
		for (Observer obs : observers) {
			obs.update(this);
		}
	}
}