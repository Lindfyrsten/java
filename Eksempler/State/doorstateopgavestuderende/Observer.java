package doorstateopgavestuderende;

public interface Observer {
	public void update(Door door);
}
