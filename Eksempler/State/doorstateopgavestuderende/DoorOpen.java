package doorstateopgavestuderende;
public class DoorOpen extends DoorState {

	private Door door;

	public DoorOpen(Door door) {
		this.door = door;
	}

	@Override
	public String toString() {
		return "OPEN";
	}

	@Override
	public void click() {
		door.setState(door.getStayOpenState());
		door.getOpeningTimer().stop();
	}

	@Override
	public void timeout() {
		door.setState(door.getClosingState());
		door.getClosingTimer().play();
	}
}
