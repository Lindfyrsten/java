package doorstateopgavestuderende;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
public class MainApp extends Application implements Observer {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Gui Demo TextArea");
		GridPane pane = new GridPane();
		initContent(pane);
		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}
	// -------------------------------------------------------------------------
	// private TextArea txaDescription;
	private Label lblDoorState;
	private Button btnPush;
	private Door door;

	private void initContent(GridPane pane) {
		door = new Door();
		door.addObserver(this);
		pane.setGridLinesVisible(false);
		pane.setPadding(new Insets(20));
		pane.setHgap(20);
		pane.setVgap(10);
		lblDoorState = new Label("CLOSED:");
		pane.add(lblDoorState, 0, 0);
		// lblDoorState.setBackground(arg0);
		btnPush = new Button("PUSH");
		pane.add(btnPush, 0, 1);
		btnPush.setOnAction(event -> pushAction());
	}

	private void pushAction() {
		door.click();
	}

	@Override
	public void update(Door door) {
		DoorState state = door.getState();
		lblDoorState.setText(state.toString());
	}
}
