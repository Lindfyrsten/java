package doorstateopgavestuderende;
public class DoorOpening extends DoorState {

	private Door door;

	public DoorOpening(Door door) {
		this.door = door;
	}

	@Override
	public String toString() {
		return "OPENING...";
	}

	@Override
	public void click() {
		door.setState(door.getClosingState());
		door.getOpeningTimer().stop();
		door.getClosingTimer().play();
	}

	@Override
	public void complete() {
		door.setState(door.getOpenState());
		door.getOpenTimer().play();
	}
}
