package bag_hashMap;
import java.util.HashMap;
import java.util.Observable;
public class Bag extends Observable implements ObservableBag {

	private HashMap<String, Integer> list;

	public Bag() {
		list = new HashMap<>();
	}

	@Override
	public void add(String s) {
		if (list.containsKey(s)) {
			list.put(s, list.get(s).intValue() + 1);
		} else {
			list.put(s, 1);
		}
		setChanged();
		notifyObservers(s + ": " + list.get(s));
	}

	@Override
	public void remove(String s) {
		if (list.containsKey(s)) {
			int count = list.get(s);
			if (count > 1) {
				list.put(s, list.get(s).intValue() - 1);
				setChanged();
				notifyObservers(s + ": " + list.get(s));
			} else {
				list.remove(s);
				setChanged();
				notifyObservers(s + " removed from bag");
			}
		}
	}

	@Override
	public int getCount(String s) {
		if (list.containsKey(s)) {
			return list.get(s);
		} else {
			return 0;
		}
	}
}
