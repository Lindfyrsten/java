package bag_hashMap;
public class BagDemo {

	public static void main(String[] args) {
		Bag b = new Bag();
		b.addObserver(new BagObserver());
		b.add("A");
		b.add("A");
		b.add("B");
		b.add("B");
		b.remove("A");
		b.remove("A");
		// System.out.println(b.getCount("A"));
	}
}
