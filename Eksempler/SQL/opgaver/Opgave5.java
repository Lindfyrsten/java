package opgaver;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Opgave5 {
    
    public static void main(String[] args) {
        try {
            System.out.println("Vi vil nu oprette en ny Medarbejder");
            BufferedReader inLine = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Indtast cprNr");
            String medarbejderCpr = inLine.readLine();
            System.out.println("Indtast regNr på afdeling: skal være oprettet");
            String medarbejderAfdeling = inLine.readLine();
//            System.out.println("Indtast navn på medarbejder");
//            String medarbejderNavn = inLine.readLine();
            Connection minConnection;
//          Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            minConnection = DriverManager.getConnection(
                "jdbc:sqlserver://LINDBJERG-PC;databaseName=AndebyBank;user=sa;password=Krille1987;");
            Statement stmt = minConnection.createStatement();
            stmt.execute("insert into Medarbejder values ('" + medarbejderCpr + "', "
                + medarbejderAfdeling + "'");
            System.out.println("Medarbejderen er nu oprettet");
            if (stmt != null) {
                stmt.close();
            }
            if (minConnection != null) {
                minConnection.close();
            }
        }
        catch (SQLException e) {
            switch (e.getErrorCode()) {
            case 547: {
                if (e.getMessage().indexOf("Kunde") != -1) {
                    System.out.println("Kunde er ikke oprettet");
                }
                if (e.getMessage().indexOf("Vare") != -1) {
                    System.out.println("Vare er ikke oprettet");
                }
                break;
            }
            case 2627: {
                System.out.println("den pågældende ordre er allerede oprettet");
                break;
            }
            default:
                System.out.println("fejlSQL:  " + e.getMessage());
            }
        }
        catch (Exception e) {
            System.out.println("fejl:  " + e.getMessage());
        }
    }
}
