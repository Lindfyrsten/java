package sierpinski;

import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class TriangleWindow extends Stage {

	public TriangleWindow(int n, double size) {
		initStyle(StageStyle.UTILITY);
		initModality(Modality.APPLICATION_MODAL);
		setMinHeight(100);
		setMinWidth(200);
		setResizable(false);

		setTitle("Sierpinski");
		// Creates the window
		GridPane root = drawPicture(n, size);

		Scene scene = new Scene(root);
		setScene(scene);
	}

	// Draws the shape for the canvas
	private void drawShapes(GraphicsContext gc, int n, double size) {
		sierpinski(10, size - 50, size, n, gc);
	}

	// Draws a sierpinski triangle
	private void sierpinski(double x, double y, double width, int n, GraphicsContext gc) {
		if (n == 0) {
			// Only draw a big triangle with the specified dimensions
			triangle(x, y, width, gc);
		} else {
			// Draw sierpinski left corner
			sierpinski(x, y, width / 2, n - 1, gc);

			// Draw sierpinski right corner
			sierpinski(x + width / 2, y, width / 2, n - 1, gc);

			// Help to draw top
			double a = Math.sqrt((width / 2 * width / 2) - (width / 4 * width / 4));

			// Draw sierpinski top corner
			sierpinski(x + width / 4, y - a, width / 2, n - 1, gc);
		}
	}

	private void triangle(double x, double y, double width, GraphicsContext gc) {
		double a = Math.sqrt((width * width) - ((width / 2) * (width / 2)));
		gc.strokeLine(x, y, x + width, y);
		gc.strokeLine(x, y, x + (width / 2), y - a);
		gc.strokeLine(x + width, y, x + (width / 2), y - a);
	}

	// Creates a canvas in the window
	private GridPane drawPicture(int n, double size) {
		GridPane pane = new GridPane();
		Canvas canvas = new Canvas(size + 20, size);
		pane.add(canvas, 0, 0);
		this.drawShapes(canvas.getGraphicsContext2D(), n, size);
		return pane;
	}
}
