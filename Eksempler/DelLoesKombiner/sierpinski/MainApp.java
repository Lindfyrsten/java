package sierpinski;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainApp extends Application {
	public static void main(String[] args) {
		Application.launch(args);
	}

	private GridPane pane = new GridPane();

	private int ohTo = 7;

	@Override
	public void start(Stage stage) {

		initContent(pane);
		Scene scene = new Scene(pane);

		stage.setTitle("Sierpinske");
		stage.setScene(scene);
		stage.show();
	}

	// ------------------------------------------------------------------------

	private TextField txfNum, txfWindowSize;

	private void initContent(GridPane pane) {
		txfNum = new TextField();
		txfNum.setText("7");
		this.pane.add(txfNum, 1, 0);
		Label lblNum = new Label("Sierpinski Degree");
		this.pane.add(lblNum, 0, 0);

		txfWindowSize = new TextField();
		txfWindowSize.setText("700");
		this.pane.add(txfWindowSize, 1, 1);
		Label lblWindow = new Label("Window Size");
		this.pane.add(lblWindow, 0, 1);

		Button btnDraw = new Button("Draw");
		btnDraw.setOnAction(event -> drawAction());
		this.pane.add(btnDraw, 0, 3);

		Button btnShow = new Button("0-" + ohTo);
		btnShow.setOnAction(event -> showAction());
		this.pane.add(btnShow, 0, 4);

	}

	private void drawAction() {
		int num = -1;
		double size = -1;
		try {
			num = Integer.parseInt(txfNum.getText().trim());
			size = Double.parseDouble(txfWindowSize.getText().trim());
		} catch (NumberFormatException e) {
			System.out.println("Only numbers, in all textfields");
		}
		if (num >= 0 && size >= 0) {
			TriangleWindow dia = new TriangleWindow(num, size);
			dia.showAndWait();
			dia.close();
		}
	}

	private void showAction() {
		for (int i = ohTo; i >= 0; i--) {
			TriangleWindow dia = new TriangleWindow(i, 700);
			dia.show();
		}
	}
}
