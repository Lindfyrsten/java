package splitsolvejoin;
import java.util.ArrayList;
public class MainApp {

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(1);
		list.add(0);
		list.add(32);
		list.add(2);
		list.add(0);
		list.add(0);
		list.add(5);
		list.add(9);
		list.add(52);
		System.out.println(Methods.sum(list));
		System.out.println(Methods.numZero(list));
	}
}
