package splitsolvejoin;
import java.util.ArrayList;
public class Methods {

	public static int sum(ArrayList<Integer> list) {
		return sumAll(list, 0, list.size() - 1);
	}

	private static int sumAll(ArrayList<Integer> list, int l, int h) {
		if (l == h) {
			return list.get(l);
		} else {
			int m = (l + h) / 2;
			int sum1 = sumAll(list, l, m);
			int sum2 = sumAll(list, m + 1, h);
			return sum1 + sum2;
		}
	}

	public static int numZero(ArrayList<Integer> list) {
		return zeroes(list, 0, list.size() - 1);
	}

	private static int zeroes(ArrayList<Integer> list, int l, int r) {
		int result = 0;
		if (l == r) {
			if (list.get(l) == 0) {
				result++;
			}
		} else {
			int m = (l + r) / 2;
			result += zeroes(list, l, m);
			result += zeroes(list, m + 1, r);
		}
		return result;
	}
}
