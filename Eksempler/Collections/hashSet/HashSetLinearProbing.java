package hashSet;
/**
 * This class implements a hash set using separate chaining.
 */
public class HashSetLinearProbing {

	private Object[] buckets;
	private int currentSize;
	private static final String DELETED = "DELETED";

	/**
	 * Constructs a hash table.
	 *
	 * @param bucketsLength
	 *            the length of the buckets array
	 */
	public HashSetLinearProbing(int bucketsLength) {
		buckets = new Object[bucketsLength];
		currentSize = 0;
	}

	/**
	 * Tests for set membership.
	 *
	 * @param x
	 *            an object
	 * @return true if x is an element of this set
	 */
	public boolean contains(Object x) {
		int h = hashValue(x);
		int elementCount = buckets.length;
		Object current = buckets[h];
		boolean found = false;
		int i = h;
		while (!found && current != null && elementCount > 0) {
			if (current.equals(x)) {
				found = true;
			} else {
				h++;
				elementCount--;
				if (h == i) {
					current = null;
				} else {
					if (h == buckets.length) {
						h = 0;
					}
					current = buckets[h];
				}
			}
		}
		return found;
	}

	/**
	 * Adds an element to this set.
	 *
	 * @param x
	 *            an object
	 * @return true if x is a new object, false if x was already in the set
	 */
	public boolean add(Object x) {
		int h = hashValue(x);
		int elementCount = buckets.length;
		Object current = buckets[h];
		boolean found = false;
		while (!found && current != null && !current.equals(DELETED) && elementCount > 0) {
			if (current.equals(x)) {
				found = true;
			} else {
				h++;
				if (h == buckets.length) {
					h = 0;
				}
				current = buckets[h];
				elementCount--;
			}
		}
		if (!found) {
			buckets[h] = x;
			currentSize++;
		}
		return found;
	}

	/**
	 * Removes an object from this set.
	 *
	 * @param x
	 *            an object
	 * @return true if x was removed from this set, false if x was not an element of this set
	 */
	public boolean remove(Object x) {
		int h = hashValue(x);
		int elementCount = buckets.length;
		Object current = buckets[h];
		boolean found = false;
		int i = h;
		while (!found && current != null && elementCount > 0) {
			if (current.equals(x)) {
				found = true;
			} else {
				h++;
				elementCount--;
				if (h == i) {
					current = null;
				} else {
					if (h == buckets.length) {
						h = 0;
					}
					current = buckets[h];
				}
			}
		}
		if (found) {
			buckets[h] = DELETED;
			currentSize--;
		}
		return found;
	}

	/**
	 * Gets the number of elements in this set.
	 *
	 * @return the number of elements
	 */
	public int size() {
		return currentSize;
	}

	private int hashValue(Object x) {
		int h = x.hashCode();
		if (h < 0) {
			h = -h;
		}
		h = h % buckets.length;
		return h;
	}

	// method only for test purpose
	public void writeOut() {
		for (int i = 0; i < buckets.length; i++) {
			System.out.println(i + "\t" + buckets[i]);
		}
	}
}
