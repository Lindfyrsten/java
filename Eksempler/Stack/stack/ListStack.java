package stack;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
public class ListStack implements StackI {

	private List<Object> stack;

	public ListStack() {
		stack = new ArrayList<>();
	}

	@Override
	public void push(Object element) {
		stack.add(element);
	}

	@Override
	public Object pop() {
		if (stack.size() == 0) {
			throw new NoSuchElementException();
		}
		return stack.remove(stack.size() - 1);
	}

	@Override
	public Object peek() {
		if (stack.size() == 0) {
			throw new NoSuchElementException();
		}
		return stack.get(0);
	}

	@Override
	public boolean isEmpty() {
		return stack.size() == 0;
	}

	@Override
	public int size() {
		return stack.size();
	}
}
