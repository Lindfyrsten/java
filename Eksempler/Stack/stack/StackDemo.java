package stack;
public class StackDemo {

	public static void main(String[] args) {
		// StackI s = new NodeStack();
		StackI s = new ListStack();
		// StackI s = new ArrayStack(5);
		s.push("Tom");
		s.push("Diana");
		s.push("Harry");
		s.push("Thomas");
		s.push("Bob");
		s.push("Brian");
		System.out.println(s.peek());
		System.out.println(s.isEmpty() + " " + s.size());
		while (!s.isEmpty()) {
			System.out.println(s.pop());
		}
		System.out.println();
		System.out.println(s.isEmpty() + " " + s.size());
		System.out.println();
		// -------- test af reverse --------
		// Integer[] tal = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		// reverse(tal);
		// for (Integer element : tal) {
		// System.out.print(element + " ");
		// }
		String str = "(3+{5{99{*}}[23[{67}67]]})";
		String no = "(}){";
		System.out.println("Parentheses of " + "\"" + str + "\" match: " + checkParantes(str));
		System.out.println("Parentheses of " + "\"" + no + "\" match: " + checkParantes(no));
	}

	public static void reverse(Object[] tabel) {
		// StackI stack = new ArrayStack(tabel.length);
		StackI stack = new ListStack();
		// StackI stack = new NodeStack();
		for (Object element : tabel) {
			stack.push(element);
		}
		int i = 0;
		while (!stack.isEmpty()) {
			tabel[i] = stack.pop();
			i++;
		}
	}

	public static boolean checkParantes(String s) {
		StackI stack = new ListStack();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c == '{' || c == '[' || c == '(') {
				stack.push(c);
			} else if (c == '}' || c == ']' || c == ')') {
				if (!stack.isEmpty()) {
					char ch = (char) stack.pop();
					if ((c == '}' && ch != '{') || (c == ']' && ch != '[') || (c == ')' && ch != '(')) {
						return false;
					}
				} else {
					return false;
				}
			}
		}
		if (stack.isEmpty()) {
			return true;
		}
		return false;
	}
}
