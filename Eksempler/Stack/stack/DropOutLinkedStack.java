package stack;
public class DropOutLinkedStack implements StackI {

	private Node first;
	private final int max;
	private int size;

	public DropOutLinkedStack(int max) {
		first = null;
		this.max = max;
		size = 0;
	}

	@Override
	public void push(Object element) {
		if (size == max) {
			Node temp = first;
			while (temp.next.next != null) {
				temp = temp.next;
			}
			temp.next = null;
		} else {
			size++;
		}
		Node newNode = new Node();
		newNode.data = element;
		newNode.next = first;
		first = newNode;
	}

	@Override
	public Object pop() {
		Object popped = null;
		if (first != null) {
			popped = first.data;
			first = first.next;
			size--;
		}
		return popped;
	}

	@Override
	public Object peek() {
		return first.data;
	}

	@Override
	public boolean isEmpty() {
		return first == null;
	}

	@Override
	public int size() {
		return size;
	}
	class Node {

		Object data;
		Node next;
	}
}
