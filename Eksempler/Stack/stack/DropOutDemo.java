package stack;
public class DropOutDemo {

	public static void main(String[] args) {
		// StackI s = new DropOutLinkedStack(5);
		// StackI s = new DropOutArrayStack(5);
		StackI s = new DropOutDoubleLinkedStack(5);
		s.push("Tom");
		s.push("Diana");
		s.push("Harry");
		s.push("Thomas");
		s.push("Bob");
		s.push("Brian");
		s.push("Simon");
		System.out.println(s.peek());
		System.out.println(s.isEmpty() + " " + s.size());
		while (!s.isEmpty()) {
			System.out.println(s.pop());
		}
		System.out.println();
		System.out.println(s.isEmpty() + " " + s.size());
		System.out.println();
	}
}
