package stack;
public class DropOutDoubleLinkedStack implements StackI {

	private Node first, last;
	private final int max;
	private int size;

	public DropOutDoubleLinkedStack(int max) {
		first = new Node();
		last = new Node();
		first.next = last;
		last.prev = first;
		this.max = max;
		size = 0;
	}

	@Override
	public void push(Object element) {
		Node newNode = new Node();
		newNode.data = element;
		if (size == 0) {
			first = newNode;
			last = newNode;
			first.next = last;
			last.prev = first;
		} else if (size == 1) {
			first = newNode;
			first.next = last;
			last.prev = first;
		} else {
			newNode.next = first;
			first.prev = newNode;
			first = newNode;
		}
		size++;
		if (size > max) {
			last.prev.next = null;
			last = last.prev;
			size--;
		}
	}

	@Override
	public Object pop() {
		Object o = first.data;
		first = first.next;
		size--;
		return o;
	}

	@Override
	public Object peek() {
		return first.data;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public int size() {
		return size;
	}
	class Node {

		Object data;
		Node next, prev;
	}
}
