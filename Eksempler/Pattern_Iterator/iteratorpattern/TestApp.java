package iteratorpattern;
public class TestApp {

	public static void main(String[] args) {
		ArrayedList<String> list = new ArrayedList<>();
		list.add("A");
		list.add("B");
		list.add("C");
		list.add("D");
		list.add("E");
		// System.out.println(list.size());
		for (String s : list) {
			System.out.print(s);
			// list.add("F");
		}
	}
}
