package eksempel;
public class Singleton_Eksempel {

	private static Singleton_Eksempel instance;

	private Singleton_Eksempel() {
	}

	public static Singleton_Eksempel getInstance() {
		if (instance == null) {
			instance = new Singleton_Eksempel();
		}
		return instance;
	}
}
