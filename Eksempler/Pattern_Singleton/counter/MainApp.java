package counter;

public class MainApp {

    public static void main(String[] args) {
        Counter counter = Counter.getInstance();
        System.out.println(counter.getValue());
        counter.count();
        counter.count();
        System.out.println(counter.getValue());
        counter.times2();
        System.out.println(counter.getValue());
        Counter.getInstance().count();
        System.out.println(counter.getValue());
        counter.zero();
        System.out.println(counter.getValue());
    }

}
