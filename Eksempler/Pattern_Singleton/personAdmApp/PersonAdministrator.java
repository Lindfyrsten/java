package personAdmApp;

import java.util.HashSet;
import java.util.Set;

public class PersonAdministrator {
    private Set<Person> personer;
    private static PersonAdministrator pa;

    private PersonAdministrator() {
        personer = new HashSet<>();
    }

    public static PersonAdministrator getInstance() {
        if (pa == null) {
            pa = new PersonAdministrator();
        }
        return pa;
    }

    public HashSet<Person> getPersoner() {
        return new HashSet<>(personer);
    }

    public void addPerson(Person person) {
        personer.add(person);
    }

    public void removePerson(Person person) {
        personer.remove(person);
    }
}
