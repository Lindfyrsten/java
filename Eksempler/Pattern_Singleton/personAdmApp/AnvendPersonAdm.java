package personAdmApp;

public class AnvendPersonAdm {

    public static void main(String[] args) {
        Person p1 = new Person("Bent");
        Person p2 = new Person("Ole");
        Person p3 = new Person("Svend");
        PersonAdministrator pa = PersonAdministrator.getInstance();
        pa.addPerson(p1);
        pa.addPerson(p2);
        pa.addPerson(p3);
        System.out.println(pa.getPersoner().toString());

    }

}
