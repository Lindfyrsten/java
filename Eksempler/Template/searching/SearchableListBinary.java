package searching;
import java.util.List;
public class SearchableListBinary<E extends Comparable<E>> extends SearchPattern<E> {

	private List<E> list;
	private int left, middle, right;

	public SearchableListBinary(List<E> list) {
		this.list = list;
	}

	@Override
	protected void init() {
		left = 0;
		right = list.size() - 1;
		middle = -1;
	}

	@Override
	protected boolean isEmpty() {
		return right < left;
	}

	@Override
	protected E select() {
		middle = (left + right) / 2;
		return list.get(middle);
	}

	@Override
	protected void split(E m) {
		if (select().compareTo(m) > 0) {
			right = middle - 1;
		} else {
			left = middle + 1;
		}
	}
}
