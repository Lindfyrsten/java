package searching;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
public class MainApp {

	public static void main(String[] args) {
		List<String> testList = new ArrayList<>();
		testList.add("Holme");
		testList.add("Skåde");
		testList.add("Viby");
		testList.add("Beder");
		testList.add("Stautrup");
		testList.add("Engdal");
		testList.add("Forældreskolen");
		testList.add("Malling");
		SearchableList<String> searchList = new SearchableList<>(testList);
		System.out.println("----- Linear List -----");
		System.out.println("\nMalling: " + searchList.search("Malling"));
		System.out.println("Viby: " + searchList.search("Viby"));
		System.out.println("Risskov: " + searchList.search("Risskov"));
		System.out.println();
		String[] testArray = {"Holme", "Skåde", "Viby", "Beder", "Stautrup", "Engdal", "Forældreskolen", "Malling"};
		SearchableArray<String> searchArray = new SearchableArray<>(testArray);
		System.out.println("----- Linear Array -----");
		System.out.println("\nMalling: " + searchArray.search("Malling"));
		System.out.println("Viby: " + searchArray.search("Viby"));
		System.out.println("Risskov: " + searchArray.search("Risskov"));
		System.out.println();
		Collections.sort(testList);
		SearchableListBinary<String> searchListBin = new SearchableListBinary<>(testList);
		System.out.println("----- Binary List -----");
		System.out.println("\nMalling: " + searchListBin.search("Malling"));
		System.out.println("Viby: " + searchListBin.search("Viby"));
		System.out.println("Risskov: " + searchListBin.search("Risskov"));
	}
}
