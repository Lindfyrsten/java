package dictionary;
import java.util.ArrayList;
public class DictionaryArrayList<K, V> implements Dictionary<K, V> {

	private ArrayList<Word>[] tabel;
	private static final int N = 10;

	@SuppressWarnings("unchecked")
	public DictionaryArrayList() {
		tabel = new ArrayList[N];
		for (int i = 0; i < N; i++) {
			tabel[i] = new ArrayList<>();
		}
	}

	@Override
	public V get(K key) {
		V value = null;
		ArrayList<Word> list = tabel[key.hashCode() % N];
		boolean found = false;
		int i = 0;
		if (!list.isEmpty()) {
			while (!found) {
				Word w = list.get(i);
				if (w.key.equals(key)) {
					value = w.value;
					found = true;
				} else {
					i++;
				}
			}
		}
		return value;
	}

	@Override
	public boolean isEmpty() {
		boolean empty = true;
		int i = 0;
		while (empty && i < N) {
			if (!tabel[i].isEmpty()) {
				empty = false;
			} else {
				i++;
			}
		}
		return empty;
	}

	@Override
	public V put(K key, V value) {
		V oldValue = null;
		int i = key.hashCode() % N;
		boolean copy = false;
		for (Word w : tabel[i]) {
			if (w.key.equals(key)) {
				oldValue = w.value;
				w.value = value;
				copy = true;
			}
		}
		if (!copy) {
			Word w = new Word();
			w.key = key;
			w.value = value;
			tabel[i].add(w);
		}
		return oldValue;
	}

	@Override
	public V remove(K key) {
		V value = null;
		int i = key.hashCode() % N;
		for (Word w : tabel[i]) {
			if (w.key.equals(key)) {
				value = w.value;
				tabel[i].remove(w);
			}
		}
		return value;
	}

	@Override
	public int size() {
		int count = 0;
		for (ArrayList<Word> list : tabel) {
			count += list.size();
		}
		return count;
	}
	class Word {

		V value;
		K key;
	}
}
