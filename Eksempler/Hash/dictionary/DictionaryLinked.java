package dictionary;
public class DictionaryLinked<K, V> implements Dictionary<K, V> {

	private Node start;
	private int size;

	/**
	 * HashingMap constructor comment.
	 */
	public DictionaryLinked() {
		// Sentinel (tomt listehoved - der ikke indeholder data)
		start = new Node();
		size = 0;
	}

	@Override
	public V get(K key) {
		V value = null;
		boolean found = false;
		Node temp = start;
		while (!found && temp != null) {
			if (temp.key.equals(key)) {
				found = true;
				value = temp.value;
			}
			temp = temp.next;
		}
		return value;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public V put(K key, V value) {
		Node newNode = new Node();
		newNode.key = key;
		newNode.value = value;
		newNode.next = start;
		start = newNode;
		return value;
	}

	@Override
	public V remove(K key) {
		V value = null;
		boolean found = false;
		Node temp = start;
		Node prev = null;
		while (!found && temp != null) {
			if (temp.key.equals(key)) {
				value = temp.value;
				if (prev == null) {
					start = temp.next;
				} else {
					prev.next = temp.next;
				}
				found = true;
			}
			prev = temp;
			temp = temp.next;
		}
		return value;
	}

	@Override
	public int size() {
		return size;
	}
	private class Node {

		Node next;
		K key;
		V value;
	}
}
