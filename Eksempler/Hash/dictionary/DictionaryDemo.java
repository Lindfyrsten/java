package dictionary;
public class DictionaryDemo {

	public static void main(String[] args) {
		// Dictionary<Integer, String> dictionary = new DictionaryArrayList<>();
		Dictionary<Integer, String> dictionary = new DictionaryHashMap<>();
		System.out.println("true = " + dictionary.isEmpty());
		System.out.println("0 = " + dictionary.size());
		dictionary.put(8, "Hans");
		System.out.println("false = " + dictionary.isEmpty());
		dictionary.put(3, "Viggo");
		System.out.println("2 = " + dictionary.size());
		dictionary.put(7, "Bent");
		dictionary.put(2, "Lene");
		System.out.println("4 = " + dictionary.size());
		System.out.println("Viggo: " + dictionary.remove(3));
		System.out.println("3 = " + dictionary.size());
		System.out.println("Hans: " + dictionary.put(8, "Ida"));
		System.out.println("Ida: " + dictionary.get(8));
		System.out.println("3 = " + dictionary.size());
		dictionary.remove(8);
		dictionary.remove(7);
		dictionary.remove(2);
		System.out.println("0 = " + dictionary.size());
		System.out.println("true = " + dictionary.isEmpty());
	}
}
