package test;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Bøde {
    private int pris;

    /*
     * Returnerer størrelsen af bøden beregnet i henhold til skemaet
     * ovenfor
     * krav: beregnetDato og faktiskDato indeholder lovlige datoer og
     * beregnetDato < faktiskDato
     * (beregnetDato er forventet afleveringsdato og
     * faktiskDato er den dag bogen blev afleveret; voksen er
     * sand, hvis det er en voksen og falsk ellers)
     */
    public int beregnBøde(LocalDate beregnetDato,
        LocalDate faktiskDato, boolean voksen) {
        long days = ChronoUnit.DAYS.between(beregnetDato, faktiskDato);
        if (days < 1) {
            throw new RuntimeException("Bogen er ikke overskredet afleveringsdato");
        }

        if (!voksen) {
            if (days < 8) {
                pris = 10;
            }
            else if (days < 15) {
                pris = 30;
            }
            else {
                pris = 45;
            }

        }
        else {
            if (days < 8) {
                pris = 20;
            }
            else if (days < 15) {
                pris = 60;
            }
            else {
                pris = 90;
            }
        }
        return pris;
        
    }
    
}
