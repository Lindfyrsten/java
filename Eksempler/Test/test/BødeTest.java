package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class BødeTest {
    private Bøde b;
    
    @Before
    public void setUp() throws Exception {
        b = new Bøde();
    }

    @Test
    public void testBeregnBøde() {
        assertEquals(10, b.beregnBøde(LocalDate.now().minusDays(1), LocalDate.now(), false));
        assertEquals(30, b.beregnBøde(LocalDate.now().minusDays(8), LocalDate.now(), false));
        assertEquals(45, b.beregnBøde(LocalDate.now().minusDays(15), LocalDate.now(), false));
        assertEquals(20, b.beregnBøde(LocalDate.now().minusDays(1), LocalDate.now(), true));
        assertEquals(60, b.beregnBøde(LocalDate.now().minusDays(8), LocalDate.now(), true));
        assertEquals(90, b.beregnBøde(LocalDate.now().minusDays(15), LocalDate.now(), true));
    }
    
    @Test(expected = RuntimeException.class)
    public void testDato() {
        assertEquals(10, b.beregnBøde(LocalDate.now(), LocalDate.now(), false));

    }
}
