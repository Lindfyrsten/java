package queue;
import java.util.NoSuchElementException;
/**
 * An implementation of a queue as a array.
 */
public class ArrayQueue implements QueueI {

	private Object[] queue;
	private int size;

	/**
	 * Constructs an empty queue.
	 */
	public ArrayQueue() {
		queue = new Object[10];
		size = 0;
	}

	/**
	 * Checks whether this queue is empty.
	 *
	 * @return true if this queue is empty
	 */
	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Adds an element to the tail of this queue.
	 *
	 * @param newElement
	 *            the element to add
	 */
	@Override
	public void enqueue(Object newElement) {
		queue[size] = newElement;
		size++;
		growIfNecessary();
	}

	/**
	 * Removes an element from the head of this queue.
	 *
	 * @return the removed element
	 */
	@Override
	public Object dequeue() {
		if (size == 0) {
			throw new NoSuchElementException();
		} else {
			Object o = queue[0];
			moveLeft();
			size--;
			return o;
		}
	}

	/**
	 * Returns the head of this queue. The queue is unchanged.
	 *
	 * @return the head element
	 */
	@Override
	public Object getFront() {
		if (size == 0) {
			throw new NoSuchElementException();
		} else {
			return queue[0];
		}
	}

	/**
	 * The number of elements on the queue.
	 *
	 * @return the number of elements in the queue
	 */
	@Override
	public int size() {
		return size;
	}

	/**
	 * Grows the element array if the current size equals the capacity.
	 */
	private void growIfNecessary() {
		if (size == queue.length) {
			Object[] newElements = new Object[2 * queue.length];
			for (int i = 0; i < queue.length; i++) {
				newElements[i] = queue[i];
			}
			queue = newElements;
		}
	}

	private void moveLeft() {
		for (int i = 0; i <= size; i++) {
			queue[i] = queue[i + 1];
		}
	}
}
