package queue;

import java.util.NoSuchElementException;

public class LinkedListQueue implements QueueI {

	private Node head;
	private Node tail;
	public LinkedListQueue() {
		head = null;
		tail = null;
	}

	@Override
	public boolean isEmpty() {
		if (head == null) {
			return true;
		}
		return false;
	}

	@Override
	public void enqueue(Object newElement) {
		Node newNode = new Node();
		newNode.data = newElement;
		if (head == null) {
			head = newNode;
			tail = newNode;
		} else {
			tail.next = newNode;
			tail = newNode;
		}

	}

	@Override
	public Object dequeue() {
		if (head == null) {
			throw new NoSuchElementException("Listen er tom");
		}
		Node h = head;
		if (head.next != null) {
			head = head.next;
		} else {
			head = null;
		}
		return h.data;
	}

	@Override
	public Object getFront() {
		return head.data;
	}

	@Override
	public int size() {
		int count = 0;
		if (head != null) {
			count++;
			Node temp = head;
			while (temp.next != null) {
				count++;
				temp = temp.next;
			}
		}
		return count;
	}
	/**
	 * Udskriver elementerne fra listen i sorteret rækkefølge
	 */
	public void udskrivElements() {
		if (head != null) {
			boolean finished = false;
			Node temp = head;
			while (!finished) {
				if (temp.next != null) {
					System.out.print(temp.data + ", ");
					temp = temp.next;
				} else {
					System.out.print(temp.data);
					finished = true;
				}

			}
		}
	}

	class Node {
		public Object data;
		public Node next;
	}
}
