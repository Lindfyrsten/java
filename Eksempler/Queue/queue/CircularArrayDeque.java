package queue;
import java.util.NoSuchElementException;
public class CircularArrayDeque implements DequeI {

	private Object[] deque;
	private int head;
	private int tail;
	private int currentSize;

	public CircularArrayDeque() {
		deque = new Object[5];
		head = 0;
		tail = 0;
		currentSize = 0;
	}

	@Override
	public boolean isEmpty() {
		return currentSize == 0;
	}

	@Override
	public void addFirst(Object newElement) {
		if (currentSize == deque.length) {
			grow();
		}
		head--;
		if (head == -1) {
			head = deque.length - 1;
		}
		deque[head] = newElement;
		currentSize++;
	}

	@Override
	public void addLast(Object newElement) {
		if (currentSize == deque.length) {
			grow();
		}
		deque[tail] = newElement;
		tail = (tail + 1) % deque.length;
		currentSize++;
	}

	@Override
	public Object removeFirst() {
		if (currentSize == 0) {
			throw new NoSuchElementException("Listen er tom");
		}
		Object o = deque[head];
		head++;
		if (head == deque.length) {
			head = 0;
		}
		currentSize--;
		return o;
	}

	@Override
	public Object removeLast() {
		if (currentSize == 0) {
			throw new NoSuchElementException("Listen er tom");
		}
		tail = (tail - 1) % deque.length;
		Object o = deque[tail];
		currentSize--;
		return o;
	}

	@Override
	public Object getFirst() {
		if (currentSize == 0) {
			throw new NoSuchElementException("Listen er tom");
		}
		return deque[head];
	}

	@Override
	public Object getLast() {
		if (currentSize == 0) {
			throw new NoSuchElementException("Listen er tom");
		}
		int temp = tail - 1;
		if (temp == -1) {
			temp = deque.length - 1;
		}
		return deque[temp];
	}

	@Override
	public int size() {
		return currentSize;
	}

	private void grow() {
		Object[] newElements = new Object[2 * deque.length];
		for (int i = 0; i < deque.length; i++) {
			newElements[i] = deque[(head + i) % deque.length];
		}
		deque = newElements;
		head = 0;
		tail = currentSize;
	}
}
