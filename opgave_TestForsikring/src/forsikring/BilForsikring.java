
package forsikring;

public class BilForsikring {
    private double grundPraemie;
    
    public double getGrundPraemie() {
        return grundPraemie;
    }
    
    public void setGrundpaemie(double grundPr) {
        if (grundPr <= 0) {
            throw new RuntimeException("grundPr skal vaere positiv");
        }
        grundPraemie = grundPr;
    }
    
    public double beregnPraemie(int alder, boolean isKvinde, int skadeFrieaer) {
        double praemie = grundPraemie;
        if (praemie == 0) {
            throw new RuntimeException("GrundPraemie har ikke faeet en vaerdi");
        }
        if (alder < 18) {
            throw new RuntimeException("Du er for ung til at tegne en forsikring");
        }
        if (alder - skadeFrieaer < 18) {
            throw new RuntimeException("Du kan ikke have kaert skadefri saelaenge");
        }
        if (skadeFrieaer < 0) {
            throw new RuntimeException("Antal skade frie aer skal vaere positiv");
        }

        if (alder < 25 && isKvinde) {
            praemie = 1.25 * grundPraemie;
        }

        else if (isKvinde) {
            praemie = praemie * 1.05;
        }

        if (skadeFrieaer < 6 && skadeFrieaer > 2) {
            praemie = praemie * 1.15;
        }
        else if (skadeFrieaer < 9 && skadeFrieaer > 5) {
            praemie = praemie * 1.25;
        }
        else if (skadeFrieaer > 8) {
            praemie = praemie * 1.35;
        }

        return praemie;

    }
    
}
