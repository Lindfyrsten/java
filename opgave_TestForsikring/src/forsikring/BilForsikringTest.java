package forsikring;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class BilForsikringTest {
    private BilForsikring b;
    
    @Before
    public void setUp() throws Exception {
        b = new BilForsikring();
    }

    @Test
    public void testBeregnPraemie() {
        b.setGrundpaemie(100);
        assertEquals(125.00, b.beregnPraemie(24, true, 0), 0.00001);

    }

    @Test
    public void testBeregnPraemie1() {
        b.setGrundpaemie(100);
        assertEquals(143.75, b.beregnPraemie(24, true, 3), 0.00001);

    }

    @Test
    public void testBeregnPraemie2() {
        b.setGrundpaemie(100);
        assertEquals(156.25, b.beregnPraemie(24, true, 6), 0.00001);

    }

    @Test
    public void testBeregnPraemie3() {
        b.setGrundpaemie(100);
        assertEquals(105, b.beregnPraemie(30, true, 0), 0.00001);

    }

    @Test
    public void testBeregnPraemie4() {
        b.setGrundpaemie(100);
        assertEquals(120.75, b.beregnPraemie(30, true, 3), 0.00001);

    }

    @Test
    public void testBeregnPraemie5() {
        b.setGrundpaemie(100);
        assertEquals(131.25, b.beregnPraemie(30, true, 6), 0.00001);

    }

    @Test
    public void testBeregnPraemie6() {
        b.setGrundpaemie(100);
        assertEquals(141.75, b.beregnPraemie(30, true, 10), 0.00001);

    }

    @Test
    public void testBeregnPraemie7() {
        b.setGrundpaemie(100);
        assertEquals(100, b.beregnPraemie(30, false, 0), 0.00001);
    }

    @Test
    public void testBeregnPraemie8() {
        b.setGrundpaemie(100);
        assertEquals(115, b.beregnPraemie(30, false, 3), 0.00001);
    }

    @Test
    public void testBeregnPraemie9() {
        b.setGrundpaemie(100);
        assertEquals(125, b.beregnPraemie(30, false, 6), 0.00001);
    }

    @Test
    public void testBeregnPraemie10() {
        b.setGrundpaemie(100);
        assertEquals(135, b.beregnPraemie(30, false, 10), 0.00001);
    }

    @Test(expected = RuntimeException.class)
    public void testBeregnPraemie11() {
        b.setGrundpaemie(100);
        assertEquals(97, b.beregnPraemie(17, false, 10), 0.00001);
    }

    @Test(expected = RuntimeException.class)
    public void testBeregnSkade() {
        b.setGrundpaemie(100);
        assertEquals(97, b.beregnPraemie(24, false, 10), 0.00001);
    }
    
    @Test(expected = RuntimeException.class)
    public void testBeregnSkade2() {
        b.setGrundpaemie(100);
        assertEquals(97, b.beregnPraemie(24, false, -1), 0.00001);
    }
    
    @Test(expected = RuntimeException.class)
    public void testBeregnPraemien() {
        b.setGrundpaemie(0);
        assertEquals(97, b.beregnPraemie(24, false, 10), 0.00001);
    }
}
