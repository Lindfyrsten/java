package application.service;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import application.model.Hotel;
import application.model.HotelTilvalg;
import application.model.Konference;
import application.model.Udflugt;

public class ServiceTest {
    private Konference k;
    private ArrayList<HotelTilvalg> tilvalg = new ArrayList<>();
    private ArrayList<Udflugt> udflugter = new ArrayList<>();
    private Hotel h;
    private HotelTilvalg t1, t2, t3;
    private Udflugt u;
    
    @Before
    public void setUp() throws Exception {
        t1 = new HotelTilvalg("Mad", 20);
        t2 = new HotelTilvalg("Bad", 25);
        t3 = new HotelTilvalg("WiFi", 15);

        k = Service.createKonference("Klima ændring", 800, LocalDate.of(2017, 3, 14),
            LocalDate.of(2017, 3, 15), LocalDate.of(2017, 3, 10));
        u = new Udflugt("Tur i zoo", 499, LocalDate.of(2017, 3, 14));
        k.addUdflugt(u);
        h = Service.createHotel("Radison", "blabla2", 150, 250, tilvalg);

    }

    //
    // Priser : Hotel pr nat (single: 150, double: 250). Konference 800. Udflugt 499
    //
    @Test(expected = RuntimeException.class)
    public void testIngenKonferenceValgt() {
        assertEquals(0, Service.samletPris(false, null, null, false, tilvalg, udflugter), 0.01);
    }
    
    @Test(expected = RuntimeException.class)
    public void testTilvalgUdenHotel() {
        tilvalg.add(t1);
        assertEquals(0, Service.samletPris(false, k, null, true, tilvalg, udflugter), 0.01);
        assertEquals(0, Service.samletPris(false, k, h, false, tilvalg, udflugter), 0.01);
    }
    
    @Test(expected = RuntimeException.class)
    public void testUdflugtUdenLedsager() {
        udflugter.add(u);
        assertEquals(0, Service.samletPris(false, k, h, false, tilvalg, udflugter), 0.01);
    }
    
    @Test(expected = RuntimeException.class)
    public void testFristOverskredet() {
        k.setTilmeldningsfrist(LocalDate.now().minusDays(1));
        assertEquals(0, Service.samletPris(false, k, h, false, tilvalg, udflugter), 0.01);
    }

    @Test(expected = AssertionError.class)
    public void testKonferenceKronologi() {
        k.setSlutDate(k.getStartDate().minusDays(1));
        assertEquals(0, Service.samletPris(false, k, h, false, tilvalg, udflugter), 0.01);
    }
    
    @Test
    public void testSamletPris() {
        //foredragsholder uden hotel, ledsager, eller udflugter
        assertEquals(0, Service.samletPris(true, k, null, false, tilvalg, udflugter), 0.01);
        //deltager uden hotel, ledsager eller udflugter
        assertEquals(800, Service.samletPris(false, k, null, false, tilvalg, udflugter), 0.01);

    }
    
    @Test
    public void testSamletPris1Dag() {
        //foredragsholder med hotel
        k.setSlutDate(LocalDate.of(2017, 3, 14));
        assertEquals(150, Service.samletPris(true, k, h, false, tilvalg, udflugter), 0.01);
        //deltager med hotel
        assertEquals(150 + 800, Service.samletPris(false, k, h, false, tilvalg, udflugter), 0.01);
        //foredragsholder med hotel og ledsager
        assertEquals(250, Service.samletPris(true, k, h, true, tilvalg, udflugter), 0.01);
        //deltager med hotel og ledsager
        assertEquals(250 + 800, Service.samletPris(false, k, h, true, tilvalg, udflugter), 0.01);
        tilvalg.add(t1);
        //foredragsholder med hotel og 1 tilvalg til 20kr pr dag
        assertEquals(150 + 20, Service.samletPris(true, k, h, false, tilvalg, udflugter), 0.01);
        //foredragsholder med hotel, ledsager og 1 tilvalg til 20kr pr dag
        assertEquals(250 + 20, Service.samletPris(true, k, h, true, tilvalg, udflugter), 0.01);
        //deltager med hotel og 1 tilvalg til 20kr pr dag
        assertEquals(150 + 20 + 800, Service.samletPris(false, k, h, false, tilvalg, udflugter),
            0.01);
        //deltager med hotel, ledsager og 1 tilvalg til 20kr pr dag
        assertEquals(250 + 20 + 800, Service.samletPris(false, k, h, true, tilvalg, udflugter),
            0.01);
        tilvalg.add(t2);
        tilvalg.add(t3);
        //foredragsholder med hotel og 3 tilvalg til 60kr pr dag
        assertEquals(210, Service.samletPris(true, k, h, false, tilvalg, udflugter), 0.01);
        //foredragsholder med hotel og ledsager og 3 tilvalg til 60kr pr dag
        assertEquals(310, Service.samletPris(true, k, h, true, tilvalg, udflugter), 0.01);
        //deltager med hotel og 3 tilvalg til 60kr pr dag
        assertEquals(150 + 60 + 800, Service.samletPris(false, k, h, false, tilvalg, udflugter),
            0.01);
        //deltager med hotel, ledsager og 3 tilvalg til 60kr pr dag
        assertEquals(250 + 60 + 800, Service.samletPris(false, k, h, true, tilvalg, udflugter),
            0.01);
        udflugter.add(u);
        //foredragsholder med hotel, ledsager og 3 tilvalg til 60kr pr dag og udflugt til 499
        assertEquals(310 + 499, Service.samletPris(true, k, h, true, tilvalg, udflugter), 0.01);
        //deltager med hotel, ledsager og 3 tilvalg til 60kr pr dag og udflugt til 499
        assertEquals(310 + 499 + 800, Service.samletPris(false, k, h, true, tilvalg, udflugter),
            0.01);
        
    }
    
    @Test
    public void testSamletPris2Dage() {
        //foredragsholder med hotel
        k.setSlutDate(LocalDate.of(2017, 3, 15));
        assertEquals(300, Service.samletPris(true, k, h, false, tilvalg, udflugter), 0.01);
        //foredragsholder med hotel og ledsager
        assertEquals(500, Service.samletPris(true, k, h, true, tilvalg, udflugter), 0.01);
        //foredragsholder med hotel og ledsager og 1 tilvalg til 20kr pr dag
        tilvalg.add(t1);
        assertEquals(540, Service.samletPris(true, k, h, true, tilvalg, udflugter), 0.01);
        //foredragsholder med hotel og ledsager og 3 tilvalg til 60kr pr dag
        tilvalg.add(t2);
        tilvalg.add(t3);
        assertEquals(620, Service.samletPris(true, k, h, true, tilvalg, udflugter), 0.01);
        udflugter.add(u);
        //deltager med hotel, ledsager og 3 tilvalg til 60kr pr dag og udflugt til 499
        assertEquals(250 * 2 + 60 * 2 + 499 + 800,
            Service.samletPris(false, k, h, true, tilvalg, udflugter),
            0.01);
    }
    
    @Test
    public void testSamletPris4Dage() {
        //foredragsholder med hotel
        k.setSlutDate(LocalDate.of(2017, 3, 17));
        assertEquals(600, Service.samletPris(true, k, h, false, tilvalg, udflugter), 0.01);
        //foredragsholder med hotel og ledsager
        assertEquals(1000, Service.samletPris(true, k, h, true, tilvalg, udflugter), 0.01);
        //foredragsholder med hotel og ledsager og 1 tilvalg til 20kr pr dag
        tilvalg.add(t1);
        assertEquals(1080, Service.samletPris(true, k, h, true, tilvalg, udflugter), 0.01);
        //foredragsholder med hotel og ledsager og 3 tilvalg til 60kr pr dag
        tilvalg.add(t2);
        tilvalg.add(t3);
        assertEquals(1240, Service.samletPris(true, k, h, true, tilvalg, udflugter), 0.01);
        udflugter.add(u);
        //deltager med hotel, ledsager og 3 tilvalg til 60kr pr dag og udflugt til 499
        assertEquals(250 * 4 + 60 * 4 + 499 + 800,
            Service.samletPris(false, k, h, true, tilvalg, udflugter),
            0.01);
    }
}
