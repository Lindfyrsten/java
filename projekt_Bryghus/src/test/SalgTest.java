package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.IngenRabat;
import model.Pris;
import model.PrisRabat;
import model.Prisliste;
import model.Produkt;
import model.ProduktType;
import model.Salg;
import model.Salgslinje;
import service.Service;

public class SalgTest {
	private Salg s;
	private Pris p1, p2, p3;
	@Before
	public void setUp() throws Exception {
		s = new Salg();
		ProduktType flaske = new ProduktType("Flaskeøl", false, true);
		ProduktType fustage = new ProduktType("Fustage", true, true);
		Produkt blondie = flaske.createProdukt("Blondie", 100);
		Produkt klosterbryg = flaske.createProdukt("Klosterbryg", 100);
		Produkt jazz = fustage.createPantProdukt("Jazz Classic, 25 liter", 10, 100);
		Prisliste fb = new Prisliste("Fredagsbar");
		p1 = fb.addPris(blondie, 50);
		p2 = fb.addPris(klosterbryg, 100);
		p3 = fb.addPris(jazz, 200);

	}

	@Test
	public void testSalg() {
		assertNotNull(s);
	}
	@Test
	public void testCreateSalgslinje() {
		Salgslinje sl = s.createSalgslinje(p1, 1, new IngenRabat());
		assertTrue(sl != null);
	}
	@Test(expected = RuntimeException.class)
	public void createSalgslinjeInvalidAntal() {
		Service.createSalgslinje(s, p1, 0, new IngenRabat());
	}

	@Test(expected = RuntimeException.class)
	public void createSalgslinjeInvalidRabat() {
		Service.createSalgslinje(s, p1, 1, new PrisRabat(100));
	}
	@Test
	public void addSalgslinje() {
		assertTrue(s.getSalgslinjer().isEmpty());
		s.createSalgslinje(p1, 1, new IngenRabat());
		assertTrue(!s.getSalgslinjer().isEmpty());
	}

	@Test
	public void testRemoveSalgslinje() {
		Salgslinje sl = s.createSalgslinje(p1, 1, new IngenRabat());
		assertTrue(!s.getSalgslinjer().isEmpty());
		s.removeSalgslinje(sl);
		assertTrue(s.getSalgslinjer().isEmpty());
	}

	@Test
	public void testUdregnSamletPris() {
		s.createSalgslinje(p1, 2, new IngenRabat());
		assertEquals(100, s.udregnSamletPris(), 0.1);
		s.createSalgslinje(p2, 1, new IngenRabat());
		assertEquals(200, s.udregnSamletPris(), 0.1);
		s.createSalgslinje(p3, 1, new PrisRabat(100));
		assertEquals(300, s.udregnSamletPris(), 0.1);

	}

	@Test
	public void testUdregnSamletPant() {
		s.createSalgslinje(p1, 2, new IngenRabat());
		s.createSalgslinje(p2, 1, new IngenRabat());
		s.createSalgslinje(p3, 1, new PrisRabat(100));
		assertEquals(100, s.udregnSamletPant(), 0.1);
	}
	@Test
	public void testUdregnSamletPantFejl() {
		s.createSalgslinje(p1, 2, new IngenRabat());
		s.createSalgslinje(p2, 1, new IngenRabat());
		assertEquals(0, s.udregnSamletPant(), 0.1);
		s.createSalgslinje(p3, 1, new IngenRabat());
		assertEquals(100, s.udregnSamletPant(), 0.1);
	}

}
