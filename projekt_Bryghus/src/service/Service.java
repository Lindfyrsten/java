package service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;

import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import model.Betalingsform;
import model.IngenRabat;
import model.PantProdukt;
import model.Pris;
import model.PrisRabat;
import model.Prisliste;
import model.ProcentRabat;
import model.Produkt;
import model.ProduktType;
import model.Rabat;
import model.Rundvisning;
import model.Salg;
import model.Salgslinje;
import model.Sampakning;
import model.Udlejning;
import storage.Storage;

public class Service {

	private static Storage storage;

	public static void initializeStorage() {
		storage = Storage.getInstance();
	}

	// Produkt

	/**
	 * Laver et nyt produkt, og tilføjer det til produkttype Pre: pt != null
	 * Pre: navn.length > 0 Pre: lagerAntal >= 0
	 *
	 * @param pt
	 * @param navn
	 * @param lagerAntal
	 * @return ny oprettet produkt
	 * @throws RuntimeException
	 */
	public static Produkt createProdukt(ProduktType pt, String navn, int lagerAntal) throws RuntimeException {
		if (pt == null) {
			throw new RuntimeException("Ikke gyldig ProduktType");
		}
		if (navn.trim().length() == 0) {
			throw new RuntimeException("Ikke gyldigt navn");
		}
		if (lagerAntal < 0) {
			throw new RuntimeException("Lager skal være positivt");
		}
		Produkt p = pt.createProdukt(navn, lagerAntal);
		storage.addProdukt(p);
		return p;
	}

	/**
	 * Returnerer et hashset med alle produkter i systemet
	 *
	 * @return HashSet
	 */
	public static HashSet<Produkt> getProdukter() {
		return storage.getProdukter();
	}

	/**
	 * Fjerner et produkt fra en produkttype
	 *
	 * @param produkt
	 * @param pt
	 * @throws RuntimeException
	 */
	public static void removeProdukt(Produkt produkt, ProduktType pt) throws RuntimeException {
		if (pt == null) {
			throw new RuntimeException("Ugyldig produkttype");
		}
		if (produkt == null) {
			throw new RuntimeException("Ugyldigt produkt");
		}
		pt.deleteProdukt(produkt);
		produkt.setProduktType(null);
		storage.removeProdukt(produkt);
	}

	/**
	 * Opdaterer et givent produkt
	 *
	 * @param produkt
	 * @param navn
	 * @param lagerAntal
	 * @throws RuntimeException
	 */
	public static void updateProdukt(Produkt produkt, String navn, int lagerAntal) throws RuntimeException {
		if (produkt == null) {
			throw new RuntimeException("Ikke gyldig Produkt");
		}
		if (navn.trim().length() == 0) {
			throw new RuntimeException("Ikke gyldigt navn");
		}
		if (lagerAntal < 0) {
			throw new RuntimeException("Lager kan ikke være negativt");
		}
		produkt.setNavn(navn);
		produkt.setLagerAntal(lagerAntal);
	}

	/**
	 * Opretter et nyt pantprodukt
	 *
	 * @param pt
	 * @param navn
	 * @param lagerAntal
	 * @param pantPris
	 * @return nyt pantprodukt
	 * @throws RuntimeException
	 */
	public static PantProdukt createPantProdukt(ProduktType pt, String navn, int lagerAntal, double pantPris)
			throws RuntimeException {
		if (pt == null) {
			throw new RuntimeException("Ikke gyldig ProduktType");
		}
		if (navn.trim().length() == 0) {
			throw new RuntimeException("Ikke gyldigt navn");
		}
		if (lagerAntal < 0) {
			throw new RuntimeException("Lager kan ikke være negativt");
		}
		if (pantPris <= 0) {
			throw new RuntimeException("Pantpris skal være positiv");
		}
		PantProdukt pp = pt.createPantProdukt(navn, lagerAntal, pantPris);
		storage.addProdukt(pp);
		return pp;
	}

	/**
	 * Opdaterer et givent pantprodukt
	 *
	 * @param pp
	 * @param navn
	 * @param lagerAntal
	 * @param pantPris
	 * @throws RuntimeException
	 */
	public static void updatePantProdukt(PantProdukt pp, String navn, int lagerAntal, double pantPris)
			throws RuntimeException {
		if (pp == null) {
			throw new RuntimeException("Ikke gyldig PantProdukt");
		}
		if (navn.trim().length() == 0) {
			throw new RuntimeException("Ikke gyldigt navn");
		}
		if (lagerAntal < 0) {
			throw new RuntimeException("Lager kan ikke være negativt");
		}
		if (pantPris <= 0) {
			throw new RuntimeException("Pantpris skal være positiv");
		}
		pp.setLagerAntal(lagerAntal);
		pp.setNavn(navn);
		pp.setPantPris(pantPris);
	}

	/**
	 * Tjekker om lagerantallet for et givent produkt bliver 0 eller mindre,
	 * hvis antal bliver solgt
	 *
	 * @param produkt
	 * @param antal
	 * @return
	 * @throws RuntimeException
	 */
	public static boolean checkLager(Produkt produkt, int antal) throws RuntimeException {
		if (produkt == null) {
			throw new RuntimeException("Ikke gyldigt Produkt");
		}
		if (produkt.getLagerAntal() >= antal) {
			return true;
		}
		return false;
	}

	/**
	 * Tjekker om lagerantallet på et givent produkt er 0
	 *
	 * @param produkt
	 * @return
	 * @throws RuntimeException
	 */
	public static boolean checkTomtLager(Produkt produkt) throws RuntimeException {
		if (produkt == null) {
			throw new RuntimeException("Ikke gyldigt Produkt");
		}
		if (produkt.getLagerAntal() > 0) {
			return false;
		}
		return true;
	}

	/**
	 * Opretter en sampakning
	 *
	 * @param pt
	 * @param navn
	 * @param lagerAntal
	 * @param flaskAntal
	 * @param glasAntal
	 * @return ny sampakning
	 * @throws RuntimeException
	 */
	public static Sampakning createSampakning(ProduktType pt, String navn, int lagerAntal, int flaskAntal,
			int glasAntal) throws RuntimeException {
		if (pt == null) {
			throw new RuntimeException("Ikke gyldig ProduktType");
		}
		if (navn.trim().length() == 0) {
			throw new RuntimeException("Ikke gyldigt navn");
		}
		if (lagerAntal < 0) {
			throw new RuntimeException("Lager kan ikke være negativt");
		}
		if (flaskAntal + glasAntal <= 0) {
			throw new RuntimeException("Kan ikke lave en Sampakning som ikke skal indeholde noget");
		}
		Sampakning sp = pt.createSampakning(navn, lagerAntal, flaskAntal, glasAntal);
		return sp;
	}

	/**
	 * Opretter en kopi af Sampakningsobjekt
	 *
	 * @param s
	 * @return
	 */
	public static Sampakning createSampakningKopi(Sampakning s) {
		Sampakning sp = new Sampakning(s.getNavn(), s.getLagerAntal(), s.getFlaskAntal(), s.getGlasAntal());
		return sp;
	}

	/**
	 * TIlføjer en liste af produkter til en sampakning
	 *
	 * @param sp
	 * @param produkter
	 * @throws RuntimeException
	 */
	public static void addToSampakning(Sampakning sp, List<Produkt> produkter) throws RuntimeException {
		if (produkter.size() == sp.getAntalProdukter()) {
			int flasker = 0;
			int glas = 0;

			for (Produkt p : produkter) {
				if (!p.getProduktType().isPakkeProdukt()) {
					throw new RuntimeException("Produkt skal være flaske eller glas");
				}
				String typenavn = p.getProduktType().getNavn();
				if (typenavn.equalsIgnoreCase("Flaske")) {
					flasker++;
				}
				if (typenavn.equalsIgnoreCase("Glas")) {
					glas++;
				}
			}
			if (sp.getFlaskAntal() != flasker || sp.getGlasAntal() != glas) {
				throw new RuntimeException("Flasker og glasantal passer ikke");
			}
			sp.addProdukter(produkter);
		} else {
			throw new RuntimeException("Ikke korrekt antal produkter. Der skal være præcist " + sp.getAntalProdukter());
		}
	}

	// Produkttype

	/**
	 * Opretter en ny produkttype
	 *
	 * @param navn
	 * @param udlejningsProdukt
	 * @param salgsProdukt
	 * @return ny produkttype
	 * @throws RuntimeException
	 */
	public static ProduktType createProduktType(String navn, boolean udlejningsProdukt, boolean salgsProdukt)
			throws RuntimeException {
		if (navn.trim().length() == 0) {
			throw new RuntimeException("Ikke gyldigt navn");
		}
		ProduktType pt = new ProduktType(navn, udlejningsProdukt, salgsProdukt);
		storage.addProduktType(pt);
		return pt;
	}

	/**
	 * Opdaterer en give produkttype
	 *
	 * @param pt
	 * @param navn
	 * @param udlejningsProdukt
	 * @throws RuntimeException
	 */
	public static void updateProduktType(ProduktType pt, String navn, boolean salgsprodukt, boolean udlejningsProdukt)
			throws RuntimeException {
		if (pt == null) {
			throw new RuntimeException("Ikke gyldig ProduktType");
		}
		if (navn.trim().length() == 0) {
			throw new RuntimeException("Ikke gyldigt navn");
		}
		pt.setNavn(navn);
		pt.setUdlejningsProdukt(udlejningsProdukt);
		pt.setSalgsProdukt(salgsprodukt);
	}

	/**
	 * Returnerer alle produkttyper i systemet
	 *
	 * @return
	 */
	public static TreeSet<ProduktType> getProduktTyper() {
		return storage.getProduktTyper();
	}

	// Prisliste

	/**
	 * Opretter en ny prisliste
	 *
	 * @param navn
	 * @return ny prisliste
	 * @throws RuntimeException
	 */
	public static Prisliste createPrisliste(String navn) throws RuntimeException {
		if (navn.trim().length() == 0) {
			throw new RuntimeException("Ikke gyldigt navn");
		}
		Prisliste p = new Prisliste(navn);
		storage.addPrisliste(p);
		return p;
	}

	/**
	 * Fjerner en prislite fra storage
	 *
	 * @param liste
	 */
	public static void removePrisliste(Prisliste liste) {
		storage.removePrisliste(liste);
	}

	/**
	 * Returnerer alle prisliste i systemet
	 *
	 * @return
	 */
	public static ArrayList<Prisliste> getPrislister() {
		return storage.getPrislister();
	}

	/**
	 * Returnerer en arrayliste med alle produkttyper i en given prisliste
	 *
	 * @param pl
	 * @return
	 * @throws RuntimeException
	 */
	public static ArrayList<ProduktType> getPrislistTyper(Prisliste pl) throws RuntimeException {
		if (pl == null) {
			throw new RuntimeException("Ugyldig prisliste");
		}
		ArrayList<ProduktType> typer = new ArrayList<>();
		for (Pris p : pl.getPriser()) {
			ProduktType pt = p.getProdukt().getProduktType();
			if (!typer.contains(pt)) {
				typer.add(pt);
			}
		}
		return typer;
	}

	// Salg

	/**
	 * Oprettet et salg
	 *
	 * @return nyt salg
	 */
	public static Salg createSalg() {
		Salg s = new Salg();
		return s;
	}

	/**
	 * Sletter et salg fra lageret
	 *
	 * @param salg
	 */
	public static void deleteSalg(Salg salg) {
		storage.removeSalg(salg);
	}

	/**
	 * Gemmer et salg i lageret
	 *
	 * @param salg
	 * @param betalingsform
	 * @throws RuntimeException
	 */
	public static void saveSalg(Salg salg, Betalingsform betalingsform) throws RuntimeException {
		if (salg == null) {
			throw new RuntimeException("Ugyldigt salg");
		}
		if (!(salg instanceof Udlejning) && (betalingsform == null || !betalingsform.isUsable())) {
			throw new RuntimeException("Ugyldig betalingsform");
		}
		if (salg instanceof Udlejning) {
			storage.addUdlejning((Udlejning) salg);
		} else {
			storage.addSalg(salg);
		}
		for (Salgslinje sl : salg.getSalgslinjer()) {
			sl.getProdukt().setLagerAntal(sl.getProdukt().getLagerAntal() - sl.getAntal());
		}
		salg.setDato(LocalDate.now());
		salg.setBetalingsform(betalingsform);
	}

	/**
	 * Returnerer en arrayliste med alle salg på en given dato
	 *
	 * @param dato
	 * @return
	 */
	public static ArrayList<Salg> getSalgPaaDato(LocalDate dato) {
		ArrayList<Salg> list = new ArrayList<>();
		for (Salg s : storage.getSalg()) {
			if (s.getDato().equals(dato)) {
				list.add(s);
			}
		}
		return list;
	}

	// Salgslinje

	/**
	 * Oprettet en ny salgslinje
	 *
	 * @param salg
	 * @param pris
	 * @param antal
	 * @param rabat
	 * @return ny salgslinje
	 * @throws RuntimeException
	 */
	public static Salgslinje createSalgslinje(Salg salg, Pris pris, int antal, Rabat rabat) throws RuntimeException {
		if (salg == null) {
			throw new RuntimeException("Ugyldigt salg");
		}
		if (pris == null) {
			throw new RuntimeException("Ugyldig pris");
		}
		if (antal < 1) {
			throw new RuntimeException("Ugyldigt antal");
		}
		if (rabat == null) {
			rabat = new IngenRabat();
		}
		return salg.createSalgslinje(pris, antal, rabat);
	}

	/**
	 * Sletter en given salgslinje fra et salg
	 *
	 * @param salg
	 * @param sl
	 */
	public static void deleteSalgslinje(Salg salg, Salgslinje sl) {
		salg.removeSalgslinje(sl);
	}

	/**
	 * Returnerer all salgslinjer på et givent salg
	 *
	 * @param salg
	 * @return
	 * @throws RuntimeException
	 */
	public static ArrayList<Salgslinje> getSalgslinjer(Salg salg) throws RuntimeException {
		if (salg == null) {
			throw new RuntimeException("Ugyldigt salg");
		}
		return salg.getSalgslinjer();
	}

	// Pris

	/**
	 * Opretter en ny pris for et givent produkt, på en given prisliste
	 *
	 * @param prisliste
	 * @param produkt
	 * @param pris
	 * @return ny pris
	 * @throws RuntimeException
	 */
	public static Pris createPris(Prisliste prisliste, Produkt produkt, double pris) throws RuntimeException {
		if (prisliste == null) {
			throw new RuntimeException("Ugyldig prisliste");
		}
		if (produkt == null) {
			throw new RuntimeException("Ugyldigt produkt");
		}
		if (pris <= 0) {
			throw new RuntimeException("Ugyldig pris");
		}
		return prisliste.addPris(produkt, pris);
	}

	/**
	 * Opretter Pris til brug i sampakning
	 *
	 * @param produkt
	 * @param pris
	 * @return
	 */
	public static Pris createPrisKopi(Produkt produkt, double pris) {
		Pris p = new Pris(produkt, pris);
		return p;
	}

	/**
	 * Fjerner en given pris fra en given prisliste
	 *
	 * @param prisliste
	 * @param pris
	 */
	public static void removePris(Prisliste prisliste, Pris pris) {
		prisliste.removePris(pris);
	}

	/**
	 * Returnerer et treeset med alle priser for en given prisliste
	 *
	 * @param pl
	 * @return
	 */
	public static TreeSet<Pris> getPriser(Prisliste pl) {
		return pl.getPriser();
	}

	/**
	 * Opdaterer en given pris
	 *
	 * @param produktPris
	 * @param pris
	 * @throws RuntimeException
	 */
	public static void updatePris(Pris produktPris, double pris) throws RuntimeException {
		if (produktPris == null) {
			throw new RuntimeException("Ugyldig pris-objekt");
		}
		if (pris <= 0) {
			throw new RuntimeException("Prisen skal være positiv");
		}
		produktPris.setPris(pris);
	}

	/**
	 * Returnerer en arrayliste med alle produkter af en given type, på en given
	 * prisliste
	 *
	 * @param pl
	 * @param pt
	 * @return
	 * @throws RuntimeException
	 */
	public static ArrayList<Pris> getPriserAfType(Prisliste pl, ProduktType pt) throws RuntimeException {
		if (pl == null) {
			throw new RuntimeException("Ugyldig prisliste");
		}
		if (pt == null) {
			throw new RuntimeException("Ugyldig produkttype");
		}
		ArrayList<Pris> list = new ArrayList<>();
		for (Pris p : pl.getPriser()) {
			if (p.getProdukt().getProduktType() == pt) {
				list.add(p);
			}
		}
		return list;
	}
	// Betalingsform

	/**
	 * Returnerer en arrayliste med alle betalingsformer i systemet
	 *
	 * @return
	 */
	public static ArrayList<Betalingsform> getBetalingsformer() {
		return storage.getBetalingsformer();
	}

	/**
	 * Returnerer alle betalingsformer i systemet, som kan bruges til at betale
	 * med
	 *
	 * @return
	 */
	public static ArrayList<Betalingsform> getUsableBetalingsformer() {
		ArrayList<Betalingsform> list = new ArrayList<>();
		for (Betalingsform b : storage.getBetalingsformer()) {
			if (b.isUsable()) {
				list.add(b);
			}
		}
		return list;
	}

	/**
	 * Opretter en ny betalingsform
	 *
	 * @param navn
	 * @return ny betalingsform
	 * @throws RuntimeException
	 */
	public static Betalingsform createBetalingsform(String navn) throws RuntimeException {
		if (navn.trim().length() == 0) {
			throw new RuntimeException("Ugyldigt navn");
		}
		Betalingsform b = new Betalingsform(navn);
		storage.addBetalingsform(b);
		return b;
	}

	/**
	 * Gør en given betalingsform ubrugelig til at betale med i systemet
	 *
	 * @param betalingsform
	 * @throws RuntimeException
	 */
	public static void disableBetalingsform(Betalingsform betalingsform) throws RuntimeException {
		if (betalingsform == null) {
			throw new RuntimeException("Ugyldig betalingsform");
		}
		betalingsform.setUsable(false);
	}

	/**
	 * Gør en given betalingsform brugbag til at betale med i systemet
	 *
	 * @param betalingsform
	 * @throws RuntimeException
	 */
	public static void enableBetalingsform(Betalingsform betalingsform) throws RuntimeException {
		if (betalingsform == null) {
			throw new RuntimeException("Ugyldig betalingsform");
		}
		betalingsform.setUsable(true);
	}

	// Rundvisning

	/**
	 * Opretter en ny rundvisning
	 *
	 * @param antalPersoner
	 * @param prisPrPerson
	 * @param startTid
	 * @param slutTid
	 * @param kontaktNr
	 * @return ny rundvisning
	 * @throws RuntimeException
	 */
	public static Rundvisning createRundvisning(int antalPersoner, double prisPrPerson, int kontaktNr, LocalDate dato,
			LocalTime start, LocalTime slut) throws RuntimeException {
		if (antalPersoner <= 0) {
			throw new RuntimeException("Antal personer skal være positiv");
		}
		if ((int) (Math.log10(kontaktNr) + 1) != 8) {
			throw new RuntimeException("Kontakt nummer skal bestå af 8 tal");
		}
		if (prisPrPerson <= 0) {
			throw new RuntimeException("Kan ikke lave gratis rundvisning");
		}
		if (dato == null) {
			throw new RuntimeException("Dato mangler");
		}
		if (start == null) {
			throw new RuntimeException("Start tidspunkt mangler");
		}
		if (slut == null) {
			throw new RuntimeException("Slut tidspunkt mangler");
		}
		if (!slut.isAfter(start)) {
			throw new RuntimeException("Start tid skal være før sluttid");
		}
		Rundvisning r = new Rundvisning(antalPersoner, prisPrPerson, LocalDateTime.of(dato, start),
				LocalDateTime.of(dato, slut), kontaktNr);
		storage.addRundvisning(r);
		return r;
	}

	/**
	 * Betaler for en given rundvisning, med en given betalingsform
	 *
	 * @param rundvisning
	 * @param betalingsform
	 * @throws RuntimeException
	 */
	public static void betalRundvisning(Rundvisning rundvisning, Betalingsform betalingsform) throws RuntimeException {
		if (rundvisning == null) {
			throw new RuntimeException("Ugyldig rundvisning");
		}
		if (betalingsform == null || !betalingsform.isUsable()) {
			throw new RuntimeException("Ugyldig betalingsform");
		}
		rundvisning.setBetalingsform(betalingsform);
	}

	/**
	 * Returnerer en arrayliste med alle rundvisninger i systemet
	 *
	 * @return
	 */
	public static ArrayList<Rundvisning> getRundvisninger() {
		return storage.getRundvisninger();
	}

	/**
	 * Returnerer en arrayliste med alle aktive rundvisninger i systemet. Det
	 * vil sige alle rundvisninger som ikke er holdt endnu.
	 *
	 * @return
	 */
	public static ArrayList<Rundvisning> getAktiveRundvisninger() {
		ArrayList<Rundvisning> list = new ArrayList<>();
		for (Rundvisning r : storage.getRundvisninger()) {
			if (r.getBetalingsform() == null) {
				list.add(r);
			} else if (r.getStartTid().isAfter(LocalDateTime.now())) {
				list.add(r);
			}
		}
		Collections.sort(list);
		return list;
	}

	/**
	 * Opdaterer en given rundvisning
	 *
	 * @param rundvisning
	 * @param antalPersoner
	 * @param prisPrPerson
	 * @param startTid
	 * @param slutTid
	 * @param kontaktNr
	 * @throws RuntimeException
	 */
	public static void updateRundvisning(Rundvisning rundvisning, int antalPersoner, double prisPrPerson, int kontaktNr,
			LocalTime startTid, LocalTime slutTid, LocalDate dato) throws RuntimeException {
		if (antalPersoner <= 0) {
			throw new RuntimeException("Antal personer skal være positiv");
		}
		if ((int) (Math.log10(kontaktNr) + 1) != 8) {
			throw new RuntimeException("Kontakt nummer skal bestå af 8 tal");
		}
		if (prisPrPerson <= 0) {
			throw new RuntimeException("Kan ikke lave gratis rundvisning");
		}
		if (dato == null) {
			throw new RuntimeException("Dato mangler");
		}
		if (startTid == null) {
			throw new RuntimeException("Start tidspunkt mangler");
		}
		if (slutTid == null) {
			throw new RuntimeException("Slut tidspunkt mangler");
		}
		if (!slutTid.isAfter(startTid)) {
			throw new RuntimeException("Start tid skal være før sluttid");
		}
		rundvisning.setAntalPersoner(antalPersoner);
		rundvisning.setPrisPrPerson(prisPrPerson);
		rundvisning.setStartTid(LocalDateTime.of(dato, startTid));
		rundvisning.setSlutTid(LocalDateTime.of(dato, slutTid));
		rundvisning.setKontaktNr(kontaktNr);
	}

	// Udlejning

	/**
	 * Opretter en ny udlejning
	 *
	 * @param navn
	 * @param cprNr
	 * @param kontaktNr
	 * @param udlejningsDato
	 * @param afleveringsDato
	 * @return ny udlejning
	 * @throws RuntimeException
	 */
	public static Udlejning createUdlejning(String navn, String cprNr, int kontaktNr, LocalDate udlejningsDato,
			LocalDate afleveringsDato) throws RuntimeException {
		if (navn.trim().length() <= 0) {
			throw new RuntimeException("Ugyldigt navn");
		}
		if (cprNr.trim().length() != 10) {
			throw new RuntimeException("Ugyldigt cpr nummer (10 cifre krævet)");
		}
		if ((int) (Math.log10(kontaktNr) + 1) != 8) {
			throw new RuntimeException("Kontakt nummer skal bestå af 8 tal");
		}
		if (udlejningsDato == null || afleveringsDato == null) {
			throw new RuntimeException("Dato skal være valgt");
		}
		if (udlejningsDato.isAfter(afleveringsDato) || udlejningsDato.isEqual(afleveringsDato)) {
			throw new RuntimeException("Udlejningsdato skal være før afleveringsdato");
		}

		Udlejning u = new Udlejning(navn, cprNr, kontaktNr, udlejningsDato, afleveringsDato);
		return u;
	}

	/**
	 * Returnerer en arrayliste med alle udlejninger i systemet
	 *
	 * @return
	 */
	public static ArrayList<Udlejning> getUdlejninger() {
		return storage.getUdlejninger();
	}

	/**
	 * Registrerer en udlejning er afleveret, og hvilken betalingsform er brugt
	 * til at afregne med
	 *
	 * @param u
	 * @param betalingsform
	 * @throws Exception
	 */
	public static void afleverUdlejning(Udlejning u, Betalingsform betalingsform) throws Exception {
		if (u == null) {
			throw new RuntimeException("Ugyldig udlejning");
		}
		if (betalingsform == null || !betalingsform.isUsable()) {
			throw new RuntimeException("Ugyldig betalingsform");
		}
		u.setAfleveret(true);
		u.setAfregningsBetaling(betalingsform);
	}

	/**
	 * Returnerer en arrayliste med udlejninger, som ikke er afleveret endnu
	 *
	 * @return
	 */
	public static ArrayList<Udlejning> getUdlejningerIkkeAfleveret() {
		ArrayList<Udlejning> ikkeAfleveret = new ArrayList<>();
		for (Udlejning u : storage.getUdlejninger()) {
			if (!u.isAfleveret()) {
				ikkeAfleveret.add(u);
			}
		}
		return ikkeAfleveret;
	}

	/**
	 * Returnerer en arrayliste med alle udlejninger der blev registreret på en
	 * given dato
	 *
	 * @param dato
	 * @return
	 */
	public static ArrayList<Udlejning> getUdlejningerPaaDato(LocalDate dato) {
		ArrayList<Udlejning> list = new ArrayList<>();
		for (Udlejning u : storage.getUdlejninger()) {
			if (u.getDato().equals(dato)) {
				list.add(u);
			}
		}
		return list;
	}
	// Klippekort

	/**
	 * Returnerer en arraylist med salg, hvor betalingsform er klipperkort i
	 * perioden mellem startDato og slutDato
	 *
	 * @param startDato
	 * @param slutDato
	 * @return
	 */
	public static ArrayList<Salg> getKlipBrugt(LocalDate startDato, LocalDate slutDato) throws RuntimeException {
		if (startDato.isAfter(slutDato)) {
			throw new RuntimeException("Startdato kan ikke være efter slutdato");
		}
		ArrayList<Salg> list = new ArrayList<>();
		for (Salg s : storage.getSalg()) {
			if (s.getBetalingsform().getNavn().equals("Klip")) {
				LocalDate salgsDato = s.getDato();
				if (!salgsDato.isBefore(startDato) && !salgsDato.isAfter(slutDato)) {
					list.add(s);
				}
			}
		}
		return list;
	}

	/**
	 * Returnerer antal klippekort solgt i perioden startDat -> slutDato
	 *
	 * @param startDato
	 * @param slutDato
	 * @return
	 */
	public static int getKlipSolgtPeriode(LocalDate startDato, LocalDate slutDato) throws RuntimeException {
		if (startDato.isAfter(slutDato)) {
			throw new RuntimeException("Startdato kan ikke være efter slutdato");
		}
		int antal = 0;
		for (Salg s : storage.getSalg()) {
			LocalDate salgsDato = s.getDato();
			if (!salgsDato.isBefore(startDato) && !salgsDato.isAfter(slutDato)) {
				for (Salgslinje sl : s.getSalgslinjer()) {
					if (sl.getProdukt().getProduktType().getNavn().equalsIgnoreCase("Klippekort")) {
						antal += sl.getAntal();
					}
				}
			}
		}
		return antal;
	}

	// Rabat

	/**
	 * Opretter ny IngenRabat
	 *
	 * @return
	 */
	public static Rabat createIngenRabat() {
		Rabat r = new IngenRabat();
		return r;
	}

	/**
	 * Opretter ny PrisRabat
	 *
	 * @param pris
	 * @return
	 */
	public static Rabat createPrisRabat(double pris) {
		if (pris <= 0) {
			throw new RuntimeException("Ugyldig rabat");
		}
		Rabat r = new PrisRabat(pris);
		return r;
	}

	/**
	 * Opretter ny ProcentRabat
	 *
	 * @param procent
	 * @return
	 */
	public static Rabat createProcentRabat(double procent) {
		if (procent <= 0) {
			throw new RuntimeException("Ugyldig rabat");
		}
		Rabat r = new ProcentRabat(procent);
		return r;
	}

	// Diverse

	/**
	 *
	 * Gør alle datoer før startDate utilgængelige
	 *
	 * @param dpDate
	 * @param startDate
	 * @return
	 */
	public static DatePicker fjernTidligereDato(DatePicker dpDate, LocalDate startDate) throws RuntimeException {
		if (dpDate == null) {
			throw new RuntimeException("Ugyldig kalender");
		}

		dpDate.setDayCellFactory((p) -> new DateCell() {
			@Override
			public void updateItem(LocalDate ld, boolean bln) {
				super.updateItem(ld, bln);
				setDisable(ld.isBefore(startDate));
			}
		});

		return dpDate;
	}

	// Initialize objects

	public static void initializeObjects() {
		// Betalingsformer
		Betalingsform b1 = createBetalingsform("Dankort");
		createBetalingsform("Kontant");
		createBetalingsform("MobilePay");
		Betalingsform b2 = createBetalingsform("Klip");
		// ProduktTyper
		ProduktType flaske = createProduktType("Flaske", false, true);
		flaske.setPakkeProdkukt(true);
		ProduktType fad = createProduktType("Fadøl", false, true);
		ProduktType spiritus = createProduktType("Spiritus", false, true);
		ProduktType fustage = createProduktType("Fustage", true, true);
		ProduktType kulsyre = createProduktType("Kulsyre", true, true);
		ProduktType malt = createProduktType("Malt", false, true);
		ProduktType beklaedning = createProduktType("Beklædning", false, true);
		ProduktType anlaeg = createProduktType("Anlæg", true, false);
		ProduktType glas = createProduktType("Glas", true, true);
		glas.setPakkeProdkukt(true);
		ProduktType klippekort = createProduktType("Klippekort", false, true);
		// Prislister
		Prisliste butik = createPrisliste("Butik");
		Prisliste fb = createPrisliste("Fredagsbar");
		// Rundvisninger
		createRundvisning(5, 100, 25480745, LocalDate.of(2017, 4, 20), LocalTime.of(13, 0), LocalTime.of(14, 0));
		Rundvisning r1 = createRundvisning(15, 50, 86151515, LocalDate.of(2017, 4, 28), LocalTime.of(10, 0),
				LocalTime.of(12, 0));
		createRundvisning(5, 100, 12345678, LocalDate.of(2017, 4, 5), LocalTime.of(13, 0), LocalTime.of(14, 0));
		betalRundvisning(r1, b1);
		// Produkter - Flasker
		Produkt p1 = createProdukt(flaske, "Klosterbryg", 100);
		Produkt p2 = createProdukt(flaske, "Sweet Georgia Brown", 50);
		Produkt p3 = createProdukt(flaske, "Extra Pilsner", 50);
		Produkt p4 = createProdukt(flaske, "Celebration", 50);
		Produkt p5 = createProdukt(flaske, "Blondie", 50);
		Produkt p6 = createProdukt(flaske, "Forårsbryg", 50);
		Produkt p7 = createProdukt(flaske, "India Pale Ale", 50);
		Produkt p8 = createProdukt(flaske, "Julebryg", 50);
		Produkt p9 = createProdukt(flaske, "Juletønden", 50);
		Produkt p10 = createProdukt(flaske, "Old Strong Ale", 50);
		Produkt p11 = createProdukt(flaske, "Fregatten Jylland", 50);
		Produkt p12 = createProdukt(flaske, "Imperial Stout", 50);
		Produkt p13 = createProdukt(flaske, "Tribute", 50);
		Produkt p14 = createProdukt(flaske, "Black Monster", 50);
		// Produkter - Fadøl
		Produkt p15 = createProdukt(fad, "Klosterbryg", 10);
		Produkt p16 = createProdukt(fad, "Jazz Classic", 10);
		Produkt p17 = createProdukt(fad, "Extra Pilsner", 10);
		Produkt p18 = createProdukt(fad, "Celebration", 10);
		Produkt p19 = createProdukt(fad, "Blondie", 10);
		Produkt p20 = createProdukt(fad, "Forårsbryg", 10);
		Produkt p21 = createProdukt(fad, "India Pale Ale ", 10);
		Produkt p22 = createProdukt(fad, "Julebryg", 10);
		Produkt p23 = createProdukt(fad, "Imperial Stout ", 10);
		Produkt p24 = createProdukt(fad, "Special", 10);
		Produkt p25 = createProdukt(fad, "Æblebrus", 10);
		Produkt p26 = createProdukt(fad, "Chips", 10);
		Produkt p27 = createProdukt(fad, "Peanuts", 10);
		Produkt p28 = createProdukt(fad, "Cola", 10);
		Produkt p29 = createProdukt(fad, "Nikoline", 10);
		Produkt p30 = createProdukt(fad, "7-Up", 10);
		Produkt p31 = createProdukt(fad, "Vand", 10);
		// Produkter - Spiritus
		Produkt p32 = createProdukt(spiritus, "Spirit of Aarhus ", 10);
		Produkt p33 = createProdukt(spiritus, "SOA med pind", 10);
		Produkt p34 = createProdukt(spiritus, "Whisky", 10);
		Produkt p35 = createProdukt(spiritus, "Liquor of Aarhus ", 10);
		// Produkter - Fustage
		Produkt p36 = createPantProdukt(fustage, "Klosterbryg, 20 liter", 10, 200);
		Produkt p37 = createPantProdukt(fustage, "Jazz Classic, 25 liter", 10, 200);
		Produkt p38 = createPantProdukt(fustage, "Extra Pilsner, 25 liter", 10, 200);
		Produkt p39 = createPantProdukt(fustage, "Celebration, 20 liter", 10, 200);
		Produkt p40 = createPantProdukt(fustage, "Blondie, 25 liter", 10, 200);
		Produkt p41 = createPantProdukt(fustage, "Forårsbryg, 20 liter", 10, 200);
		Produkt p42 = createPantProdukt(fustage, "India Pale Ale, 20 liter", 10, 200);
		Produkt p43 = createPantProdukt(fustage, "Julebryg, 20 liter ", 10, 200);
		Produkt p44 = createPantProdukt(fustage, "Imperial Stout, 20 liter ", 10, 200);
		// Produkter - Kulsyre
		Produkt p46 = createPantProdukt(kulsyre, "Kulsyre, 4kg", 10, 1000);
		Produkt p47 = createPantProdukt(kulsyre, "Kulsyre, 6kg", 10, 1000);
		Produkt p48 = createPantProdukt(kulsyre, "Kulsyre, 10kg", 10, 1000);
		// Produkter - Malt
		Produkt p50 = createProdukt(malt, "25 kg sæk", 10);
		// Produkter - Beklædning
		Produkt p51 = createProdukt(beklaedning, "T-Shirt", 10);
		Produkt p52 = createProdukt(beklaedning, "Polo", 10);
		Produkt p53 = createProdukt(beklaedning, "Cap", 10);
		// Produkter - Anlæg
		Produkt p54 = createProdukt(anlaeg, "1-hane", 10);
		Produkt p55 = createProdukt(anlaeg, "2-haner", 10);
		Produkt p56 = createProdukt(anlaeg, "Bar med flere haner ", 10);
		Produkt p57 = createProdukt(anlaeg, "Levering", 10);
		Produkt p58 = createProdukt(anlaeg, "Krus", 10);
		// Produkter - Glas
		Produkt p59 = createProdukt(glas, "Uanset størrelse", 50);
		// Produkter - Diverse
		Produkt p67 = createProdukt(klippekort, "Klippekort", 90);
		// Produkttyper - Sampakninger
		ProduktType sp1 = createProduktType("Sampakning (Gaveæske)", false, true);
		ProduktType sp2 = createProduktType("Sampakning (Trækasse)", false, true);
		ProduktType sp3 = createProduktType("Sampakning (Papkasse)", false, true);

		// Sampakninger
		Sampakning p60 = createSampakning(sp1, "2 øl, 2 glas", 100, 2, 2);
		Sampakning p61 = createSampakning(sp1, "4 øl", 100, 4, 0);
		Sampakning p62 = createSampakning(sp1, "6 øl, 2 glas", 100, 6, 2);
		Sampakning p63 = createSampakning(sp2, "6 øl", 100, 6, 0);
		Sampakning p64 = createSampakning(sp2, "6 øl, 6 glas", 100, 6, 6);
		Sampakning p65 = createSampakning(sp2, "12 øl", 100, 12, 0);
		Sampakning p66 = createSampakning(sp3, "12 øl", 100, 12, 0);
		// Priser - FredagsBar
		Pris pris1 = createPris(fb, p1, 50);
		Pris pris2 = createPris(fb, p2, 50);
		Pris pris3 = createPris(fb, p3, 50);
		createPris(fb, p4, 50);
		createPris(fb, p5, 50);
		createPris(fb, p6, 50);
		createPris(fb, p7, 50);
		createPris(fb, p8, 50);
		createPris(fb, p9, 50);
		createPris(fb, p10, 50);
		createPris(fb, p11, 50);
		createPris(fb, p12, 50);
		createPris(fb, p13, 50);
		createPris(fb, p14, 50);
		createPris(fb, p15, 30);
		createPris(fb, p16, 30);
		createPris(fb, p17, 30);
		createPris(fb, p18, 30);
		createPris(fb, p19, 30);
		createPris(fb, p20, 30);
		createPris(fb, p21, 30);
		createPris(fb, p22, 30);
		createPris(fb, p23, 30);
		createPris(fb, p24, 30);
		createPris(fb, p25, 15);
		createPris(fb, p26, 10);
		createPris(fb, p27, 10);
		createPris(fb, p28, 15);
		createPris(fb, p29, 15);
		createPris(fb, p30, 15);
		createPris(fb, p31, 10);
		createPris(fb, p32, 300);
		createPris(fb, p33, 350);
		createPris(fb, p34, 500);
		createPris(fb, p35, 175);
		createPris(fb, p46, 400);
		createPris(fb, p47, 1000);
		// createPris(fb, p48, 0);
		createPris(fb, p51, 70);
		createPris(fb, p52, 100);
		createPris(fb, p53, 30);
		createPris(fb, p60, 100);
		createPris(fb, p61, 130);
		createPris(fb, p62, 240);
		createPris(fb, p63, 250);
		createPris(fb, p64, 290);
		createPris(fb, p65, 390);
		createPris(fb, p66, 360);
		Pris pKlip = createPris(fb, p67, 100);
		// Priser - Butik
		createPris(butik, p1, 36);
		createPris(butik, p2, 36);
		createPris(butik, p3, 36);
		createPris(butik, p4, 36);
		createPris(butik, p5, 36);
		createPris(butik, p6, 36);
		createPris(butik, p7, 36);
		createPris(butik, p8, 36);
		createPris(butik, p9, 36);
		createPris(butik, p10, 36);
		createPris(butik, p11, 36);
		createPris(butik, p12, 36);
		createPris(butik, p13, 36);
		createPris(butik, p14, 50);
		createPris(butik, p32, 300);
		createPris(butik, p33, 350);
		createPris(butik, p34, 500);
		createPris(butik, p35, 175);
		createPris(butik, p36, 775);
		createPris(butik, p37, 625);
		createPris(butik, p38, 575);
		createPris(butik, p39, 775);
		Pris udlej1 = createPris(butik, p40, 700);
		createPris(butik, p41, 775);
		createPris(butik, p42, 775);
		createPris(butik, p43, 775);
		createPris(butik, p44, 775);
		Pris udlej2 = createPris(butik, p46, 400);
		createPris(butik, p47, 1000);
		createPris(butik, p48, 1000);
		createPris(butik, p50, 300);
		createPris(butik, p51, 70);
		createPris(butik, p52, 100);
		createPris(butik, p53, 30);
		createPris(butik, p54, 250);
		createPris(butik, p55, 400);
		createPris(butik, p56, 500);
		createPris(butik, p57, 500);
		createPris(butik, p58, 60);
		createPris(butik, p59, 15);
		createPris(butik, p60, 100);
		createPris(butik, p61, 130);
		createPris(butik, p62, 240);
		createPris(butik, p63, 250);
		createPris(butik, p64, 290);
		createPris(butik, p65, 390);
		createPris(butik, p66, 360);

		// nogle salg

		Salg salg1 = new Salg();
		Salg salg2 = new Salg();
		Salg salg3 = new Salg();
		Salg udlejning = new Udlejning("Per Toft", "1305822713", 98510464, LocalDate.now().minusDays(2),
				LocalDate.now().plusDays(6));
		Salg udlejning2 = new Udlejning("Lars Rasmussen", "1507859313", 86151515, LocalDate.now().minusDays(2),
				LocalDate.now().plusDays(6));

		createSalgslinje(udlejning, udlej1, 1, new IngenRabat());
		createSalgslinje(udlejning, udlej2, 2, new PrisRabat(100));
		createSalgslinje(salg3, pKlip, 1, new IngenRabat());
		createSalgslinje(salg1, pris1, 2, new ProcentRabat(25));
		createSalgslinje(salg1, pris2, 3, new PrisRabat(50));
		createSalgslinje(salg2, pris3, 2, new IngenRabat());

		saveSalg(salg1, b1);
		saveSalg(salg2, b2);
		saveSalg(salg3, b1);
		saveSalg(udlejning, null);
		saveSalg(udlejning2, null);

	}
}
