package guifx;

import java.time.LocalDate;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import model.Salg;
import model.Salgslinje;
import service.Service;

public class KlipOversigtPane extends GridPane {

	private final DatePicker dpStart;
	private final DatePicker dpSlut;
	private final ListView<Salg> lvwSalg;
	private final ListView<Salgslinje> lvwSalgslinjer;
	private final TextField txfKlip, txfKlipSolgt;

	public KlipOversigtPane() {
		setPadding(new Insets(10, 10, 10, 20));
		setVgap(10);
		setHgap(10);

		DropShadow ds = new DropShadow();
		ds.setOffsetY(3.0f);
		ds.setColor(Color.color(0.4f, 0.4f, 0.4f));

		Label lblTitel = new Label("Klippekort");
		Label lblDatoFra = new Label("Fra");
		Label lblDatoTil = new Label("Til");
		Label lblSalg = new Label("Salg");
		Label lblProdukt = new Label("Produkter");
		Label lblKlip = new Label("Klip anvendt");
		Label lblKlipSolgt = new Label("Klippekort solgt");

		lblTitel.setFont(Font.font("GARAMOND", FontWeight.BOLD, 42));
		lblTitel.setEffect(ds);
		lblTitel.setCache(true);
		lblSalg.setFont(Font.font("GARAMOND", FontWeight.BOLD, 26));
		lblSalg.setEffect(ds);
		lblSalg.setCache(true);
		lblProdukt.setFont(Font.font("GARAMOND", FontWeight.BOLD, 26));
		lblProdukt.setEffect(ds);
		lblProdukt.setCache(true);

		txfKlip = new TextField();
		txfKlip.setEditable(false);
		txfKlip.setPrefWidth(30);
		txfKlipSolgt = new TextField();
		txfKlipSolgt.setEditable(false);
		txfKlipSolgt.setPrefWidth(30);

		lvwSalg = new ListView<>();
		lvwSalgslinjer = new ListView<>();
		lvwSalg.getSelectionModel().selectedItemProperty().addListener(event -> salgChanged());

		dpStart = new DatePicker();
		dpStart.setPrefWidth(110);
		dpStart.setOnAction(event -> startDatoChanged());
		dpSlut = new DatePicker();
		dpSlut.setOnAction(event -> dateChanged());
		dpSlut.setPrefWidth(110);
		dpSlut.setDisable(true);

		HBox hbxDatoFra = new HBox(30, lblDatoFra, dpStart, lblDatoTil, dpSlut);
		HBox hbxKlip = new HBox(20, lblKlip, txfKlip, lblKlipSolgt, txfKlipSolgt);

		add(lblTitel, 0, 0, 4, 1);
		add(lblSalg, 0, 1);
		add(lblProdukt, 2, 1);
		add(lvwSalg, 0, 2, 1, 1);
		add(lvwSalgslinjer, 2, 2);
		add(hbxDatoFra, 0, 3, 4, 1);
		add(hbxKlip, 0, 4, 4, 1);

		hbxDatoFra.setAlignment(Pos.BASELINE_CENTER);
		hbxKlip.setAlignment(Pos.BASELINE_CENTER);
		GridPane.setHalignment(lblSalg, HPos.CENTER);
		GridPane.setHalignment(lblProdukt, HPos.CENTER);
		GridPane.setHalignment(lblTitel, HPos.CENTER);

	}

	// Sætter listview<salg> til eventuelle salg, med klip som betalingsform, i
	// perioden startdato -> slutdato. Sætter txfKlip til samlede antal klip
	// brugt for perioden.
	private void dateChanged() {
		LocalDate startDato = dpStart.getValue();
		LocalDate slutDato = dpSlut.getValue();

		// hvis både start- og slutdato er valgt
		if (slutDato != null && startDato != null) {
			// tjekker at slutdato ikke er før startdato
			if (!startDato.isAfter(slutDato)) {
				lvwSalg.getItems().setAll(Service.getKlipBrugt(startDato, slutDato));
				int klip = 0;
				for (Salg s : lvwSalg.getItems()) {
					for (Salgslinje sl : s.getSalgslinjer()) {
						klip += sl.getAntal();
					}
				}
				txfKlip.setText("" + klip);
				txfKlipSolgt.setText("" + Service.getKlipSolgtPeriode(startDato, slutDato));
			} else { // hvis startdato sættes til senere end slutdato, sættes
						// slutdato til valgte startdato.
				dpSlut.setValue(startDato);
			}
		}
	}

	// Sæt lvwSalgslinjer til markerede salgs' salgslinjer. Hvis intet salg er
	// valgt, rydes lysten.
	private void salgChanged() {

		if (lvwSalg.getSelectionModel().getSelectedItem() != null) {
			Salg salg = lvwSalg.getSelectionModel().getSelectedItem();
			lvwSalgslinjer.getItems().setAll(salg.getSalgslinjer());
		} else {
			lvwSalgslinjer.getItems().clear();
		}
	}

	// Gør datoer før startdato utilgængelig ved valg af slutdato. Hvis ingen
	// startDato er valgt, gøres valg a slutdato utilgængelig
	private void startDatoChanged() {
		LocalDate start = dpStart.getValue();
		if (start != null) {
			dpSlut.setDisable(false);
			Service.fjernTidligereDato(dpSlut, dpStart.getValue());
		} else {
			dpSlut.setDisable(true);
		}
		dateChanged();
	}
}
