package guifx;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import model.Betalingsform;
import model.Udlejning;
import service.Service;

public class UdlejningsKalenderPane extends GridPane {
	private final Button btnPrevious, btnNext;
	private final Label lblMonth;
	private LocalDate dato;
	private final ArrayList<Hyperlink> hyperlinks = new ArrayList<>();
	private final ArrayList<Udlejning> udlejninger;
	private final HashMap<LocalDate, ArrayList<Udlejning>> afleveringsMap = new HashMap<>();

	public UdlejningsKalenderPane() {
		setPadding(new Insets(10, 10, 10, 30));
		setVgap(20);
		setHgap(20);

		DropShadow ds = new DropShadow();
		ds.setOffsetY(3.0f);
		ds.setColor(Color.color(0.4f, 0.4f, 0.4f));

		Label lblTitel = new Label("Udlejningskalender");
		lblTitel.setFont(Font.font("GARAMOND", FontWeight.BOLD, 42));
		lblTitel.setAlignment(Pos.BASELINE_CENTER);
		lblTitel.setEffect(ds);
		lblTitel.setCache(true);
		lblMonth = new Label();
		lblMonth.setFont(Font.font("ARIAL", 24));
		lblMonth.setEffect(ds);
		lblMonth.setCache(true);
		lblMonth.setMinWidth(290);
		lblMonth.setAlignment(Pos.BASELINE_CENTER);
		lblMonth.setTextFill(Color.FIREBRICK);
		Label lblUdlejet = new Label("Udlejningdato");
		Label lblAfleveret = new Label("Afleveret");
		Label lblIkkeAfleveret = new Label("Ikke afleveret");
		Label lblAfl = new Label("Afleveringsfrist: ");
		Label lblSlash = new Label("/");
		lblAfleveret.setFont(Font.font("ARIAL", FontWeight.BOLD, 12));
		lblAfleveret.setTextFill(Color.GREEN);
		lblIkkeAfleveret.setFont(Font.font("ARIAL", FontWeight.BOLD, 12));
		lblIkkeAfleveret.setTextFill(Color.RED);
		lblUdlejet.setFont(Font.font("ARIAL", FontWeight.BOLD, 12));
		lblUdlejet.setTextFill(Color.PURPLE);
		lblAfl.setFont(Font.font("ARIAL", FontWeight.BOLD, 12));

		String[] ugeDage = {"M", "T", "O", "T", "F", "L", "S"};

		dato = LocalDate.now();

		btnPrevious = new Button("<");
		btnPrevious.setPrefSize(80, 30);
		btnPrevious.setFont(Font.font(18));
		btnPrevious.setOnAction(event -> previousMonth());
		btnNext = new Button(">");
		btnNext.setFont(Font.font(18));
		btnNext.setPrefSize(80, 30);
		btnNext.setOnAction(event -> nextMonth());

		HBox hbxDato = new HBox(20, btnPrevious, lblMonth, btnNext);
		HBox hbxInfo = new HBox(5, lblAfl, lblAfleveret, lblSlash, lblIkkeAfleveret);

		add(lblTitel, 0, 0, 7, 1);
		add(hbxDato, 0, 1, 7, 1);
		add(hbxInfo, 2, 9, 5, 1);
		add(lblUdlejet, 0, 9, 2, 1);

		for (int i = 0; i < ugeDage.length; i++) {
			Label lblUgeDag = new Label(ugeDage[i]);
			lblUgeDag.setFont(Font.font("ARIAL", FontWeight.BOLD, 24));
			add(lblUgeDag, i, 2);
		}

		udlejninger = Service.getUdlejninger();
		hbxInfo.setAlignment(Pos.BASELINE_CENTER);
		GridPane.setHalignment(btnPrevious, HPos.CENTER);
		GridPane.setHalignment(lblMonth, HPos.CENTER);
		GridPane.setHalignment(btnNext, HPos.CENTER);
		GridPane.setHalignment(lblTitel, HPos.CENTER);
		GridPane.setHalignment(lblUdlejet, HPos.CENTER);

		monthChanged();
	}

	// Fjerner alle labels over dage og tilføjes igen ifht antallet af dage i
	// den nye måned. Hvis der enten er en udlejnings- eller afleveringsdato på
	// kalenderdatoen, ændres font farven til grøn og rød, samt et tooltip
	// tilføjes med yderligere info omkring udlejning.
	private void monthChanged() {
		lblMonth.setText(dato.getMonth().toString() + "  " + dato.getYear());
		LocalDate tempDato = LocalDate.of(dato.getYear(), dato.getMonth(), 1);

		// clear hyperlinks
		getChildren().removeAll(hyperlinks);
		hyperlinks.clear();

		// add nye links og tilføj dem til en arraylist, så de kan
		// fjernes igen senere
		int row = 3;
		while (tempDato.getMonthValue() == dato.getMonthValue()) {
			int day = tempDato.getDayOfWeek().getValue();
			Hyperlink hDay = new Hyperlink("" + tempDato.getDayOfMonth());
			hDay.setFont(Font.font("ARIAL", 24));
			hDay.setTextFill(Color.BLACK);
			add(hDay, day - 1, row);
			hyperlinks.add(hDay);

			if (tempDato.equals(LocalDate.now())) {
				hDay.setFont(Font.font("ARIAL", FontWeight.BOLD, 24));
			}
			// tjekker om der findes udlejningsdatoer på den nyoprettede dato
			// og sætter farve og tooltip
			for (Udlejning u : udlejninger) {
				LocalDate date = u.getAfleveringsDato();
				if (date.equals(tempDato)) {
					if (!afleveringsMap.containsKey(tempDato)) {
						afleveringsMap.put(date, new ArrayList<Udlejning>());
					}
					if (!afleveringsMap.get(date).contains(u)) {
						afleveringsMap.get(date).add(u);
					}
					Tooltip t = hDay.getTooltip();
					// hvis der ikke findes en aflevering på datoen i forvejen
					if (t == null) {
						hDay.setTextFill(Color.GREEN);
						t = new Tooltip("");
						hDay.setTooltip(t);
						hDay.setOnAction(event -> aflever(afleveringsMap.get(date), date));
					}
					t.setText(t.getText() + "Aflevering: " + u.getNavn() + " " + u.getKontaktNr() + "\n");
					hDay.setFont(Font.font("ARIAL", FontWeight.BOLD, 24));
					for (Udlejning udl : afleveringsMap.get(date)) {
						if (udl.isAfleveret()) {
							hDay.setTextFill(Color.RED);
						}
					}
					//// hvis ikke afleveret, kan aflever() metoden køres

				} else if (u.getUdlejningsDato().equals(tempDato)) {
					Tooltip t = hDay.getTooltip();
					if (t == null) {
						t = new Tooltip("");
						hDay.setTooltip(t);
					}
					t.setText(t.getText() + "Udlejning: " + u.getNavn() + " " + u.getKontaktNr() + "\n");
					hDay.setFont(Font.font("ARIAL", FontWeight.BOLD, 24));
					hDay.setTextFill(Color.PURPLE);
				}
			}
			tempDato = tempDato.plusDays(1);
			// næste row hvis ny uge
			if (day == 7) {
				row++;
			}
		}
	}

	// Sætter datoen en måned tilbage
	private void previousMonth() {
		dato = dato.minusMonths(1);
		monthChanged();
	}

	// Sætter datoen en måned frem
	private void nextMonth() {
		dato = dato.plusMonths(1);
		monthChanged();
	}

	private void aflever(ArrayList<Udlejning> list, LocalDate date) {
		ArrayList<Udlejning> active = new ArrayList<>();
		for (Udlejning u : list) {
			if (!u.isAfleveret()) {
				active.add(u);
			}
		}
		if (!active.isEmpty()) {
			Udlejning u = list.get(0);
			if (list.size() > 1) {
				ChoiceDialog<Udlejning> afleveringer = new ChoiceDialog<>();
				afleveringer.getItems().setAll(active);
				afleveringer.setSelectedItem(afleveringer.getItems().get(0));
				afleveringer.setHeaderText("Vælg aflevering");
				afleveringer.setContentText("Flere afleveringer på dato:");
				Optional<Udlejning> result = afleveringer.showAndWait();
				if (result.isPresent()) {
					u = result.get();
				}
			}
			ChoiceDialog<Betalingsform> betalingsformer = new ChoiceDialog<>();
			betalingsformer.getItems().setAll(Service.getBetalingsformer());
			betalingsformer.setSelectedItem(betalingsformer.getItems().get(0));
			betalingsformer.setHeaderText("Vælg betalingsform.");
			betalingsformer.setContentText("At betale: " + u.udregnSamletPris());
			Optional<Betalingsform> result = betalingsformer.showAndWait();
			if (result.isPresent()) {
				// kalder service metode og viser bekræftelse
				try {
					Service.afleverUdlejning(u, result.get());
				} catch (Exception e) {
					System.out.println(e.getMessage());
					return;
				}
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setHeaderText("Udlejning afleveret og betalt");
				alert.showAndWait();
				monthChanged();

			}
		}
	}

}
