package guifx;

import java.time.LocalDate;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import model.Salg;
import model.Salgslinje;
import model.Udlejning;
import service.Service;

public class SalgsOversigtPane extends GridPane {

	private final DatePicker dpDato;
	private final ListView<Salg> lvwSalg;
	private final ListView<Udlejning> lvwUdlejning;
	private final ListView<Salgslinje> lvwSalgslinjer;

	public SalgsOversigtPane() {
		setPadding(new Insets(10));
		setVgap(10);
		setHgap(10);

		DropShadow ds = new DropShadow();
		ds.setOffsetY(3.0f);
		ds.setColor(Color.color(0.4f, 0.4f, 0.4f));

		Label lblTitel = new Label("Salgsoversigt");
		Label lblDato = new Label("Dato");
		Label lblSalg = new Label("Betalingsform");
		Label lblProdukt = new Label("Produkter");
		Label lblUdlej = new Label("Udlejninger");
		lblTitel.setFont(Font.font("GARAMOND", FontWeight.BOLD, 42));
		lblTitel.setEffect(ds);
		lblTitel.setCache(true);
		lblSalg.setFont(Font.font("GARAMOND", FontWeight.BOLD, 26));
		lblSalg.setEffect(ds);
		lblSalg.setCache(true);
		lblUdlej.setFont(Font.font("GARAMOND", FontWeight.BOLD, 26));
		lblUdlej.setEffect(ds);
		lblUdlej.setCache(true);
		lblProdukt.setFont(Font.font("GARAMOND", FontWeight.BOLD, 26));
		lblProdukt.setEffect(ds);
		lblProdukt.setCache(true);

		lvwSalg = new ListView<>();
		lvwSalgslinjer = new ListView<>();
		lvwSalg.getSelectionModel().selectedItemProperty().addListener(event -> salgChanged());
		lvwUdlejning = new ListView<>();
		lvwUdlejning.getSelectionModel().selectedItemProperty().addListener(event -> udlejningChanged());

		dpDato = new DatePicker(LocalDate.now());
		dpDato.setOnAction(event -> dateChanged());
		dpDato.setPrefWidth(110);

		HBox hbxDato = new HBox(20, lblDato, dpDato);

		add(lblTitel, 0, 0, 4, 1);
		add(hbxDato, 0, 1, 3, 1);
		add(lblSalg, 0, 2);
		add(lblProdukt, 2, 2);
		add(lvwSalg, 0, 3);
		add(lblUdlej, 0, 4);
		add(lvwUdlejning, 0, 5);
		add(lvwSalgslinjer, 2, 3, 1, 3);

		hbxDato.setAlignment(Pos.BASELINE_CENTER);
		GridPane.setHalignment(lblSalg, HPos.CENTER);
		GridPane.setHalignment(lblProdukt, HPos.CENTER);
		GridPane.setHalignment(lblTitel, HPos.CENTER);
		GridPane.setHalignment(lblUdlej, HPos.CENTER);

		dateChanged();
	}

	// Rydder eventuelle salgslinjer. Sætter salg og udlejninger til den nye
	// dato.
	private void dateChanged() {

		lvwSalgslinjer.getItems().clear();
		LocalDate dato = dpDato.getValue();
		lvwSalg.getItems().setAll(Service.getSalgPaaDato(dato));
		lvwUdlejning.getItems().setAll(Service.getUdlejningerPaaDato(dato));

	}

	// Sætter salgslinje listview til det markerede salg på salgslistview
	private void salgChanged() {

		if (lvwSalg.getSelectionModel().getSelectedItem() != null) {
			lvwUdlejning.getSelectionModel().clearSelection();
			Salg salg = lvwSalg.getSelectionModel().getSelectedItem();
			lvwSalgslinjer.getItems().setAll(salg.getSalgslinjer());
		}
	}

	// Sætter salgslinje listview til den markerede udlejning på
	// udlejningslistview
	private void udlejningChanged() {
		if (lvwUdlejning.getSelectionModel().getSelectedItem() != null) {
			lvwSalg.getSelectionModel().clearSelection();
			Udlejning udlejning = lvwUdlejning.getSelectionModel().getSelectedItem();
			lvwSalgslinjer.getItems().setAll(udlejning.getSalgslinjer());
		}
	}
}
