package guifx;

import javafx.animation.FadeTransition;
import javafx.animation.PathTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Reflection;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.CubicCurveTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.util.Duration;
import service.Service;

public class MainApp extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		Service.initializeStorage();
		Service.initializeObjects();
		BorderPane pane = new BorderPane();
		pane.setPadding(new Insets(0, 10, 0, 10));
		stage.setTitle("Kasseapperat");
		stage.setResizable(false);
		Scene scene = new Scene(pane);
		initContent(pane);

		stage.setScene(scene);
		stage.show();

	}

	private MenuBar mb;
	private Menu mFile, mEdit, mView;
	private MenuItem miSalg, miSalgsOversigt, miRundvisning, miQuit, miUdlejningsKalender, miOpdaterPrisliste,
			miProdukt, miKlippekortOversigt;

	private void initContent(BorderPane root) {

		mb = new MenuBar();
		mFile = new Menu("Filer");
		mEdit = new Menu("Rediger");
		mView = new Menu("Vis");
		mb.getMenus().addAll(mFile, mEdit, mView);

		DropShadow ds = new DropShadow();
		ds.setOffsetY(2.0f);
		ds.setColor(Color.color(0.4f, 0.4f, 0.4f));

		Label lblTitel = new Label("Kasseapparat");
		lblTitel.setFont(Font.font("Garamond", FontWeight.BOLD, 80));
		lblTitel.setTextFill(Color.DARKGREEN);
		lblTitel.setEffect(ds);
		lblTitel.setCache(true);

		Group group = new Group();

		miProdukt = new MenuItem("Produkt/type");
		miProdukt.setOnAction(event -> updateProdukt(root));
		miSalg = new MenuItem("Salg");
		miSalg.setOnAction(event -> salg(root));
		miQuit = new MenuItem("Afslut");
		miQuit.setOnAction(event -> Platform.exit());
		miRundvisning = new MenuItem("Rundvisning");
		miRundvisning.setOnAction(event -> rundvisning(root));
		miSalgsOversigt = new MenuItem("Salgsoversigt");
		miSalgsOversigt.setOnAction(event -> salgsOversigt(root));
		miKlippekortOversigt = new MenuItem("Klipoversigt");
		miKlippekortOversigt.setOnAction(event -> klipOversigt(root));
		miUdlejningsKalender = new MenuItem("Udlejningskalender");
		miUdlejningsKalender.setOnAction(event -> udlejningsKalender(root));
		miOpdaterPrisliste = new MenuItem("Prisliste");
		miOpdaterPrisliste.setOnAction(event -> updatePrisliste(root));
		mFile.getItems().addAll(miSalg, miRundvisning, new SeparatorMenuItem(), miQuit);
		mEdit.getItems().addAll(miProdukt, miOpdaterPrisliste);
		mView.getItems().addAll(miSalgsOversigt, miUdlejningsKalender, miKlippekortOversigt);

		Image imageSale = new Image("http://findicons.com/files/icons/2836/flatastic_part_4/128/sale.png");
		Image imageSale2 = new Image("http://findicons.com/files/icons/2836/flatastic_part_4/128/sale.png");
		Image imageQuit = new Image("http://findicons.com/files/icons/753/gnome_desktop/64/gnome_application_exit.png");
		Image imageNew = new Image("http://findicons.com/files/icons/2192/flavour_extended/48/beer.png");
		Image imageLogo = new Image("http://aarhusbryghus.dk/wp-content/uploads/2013/12/logo2.png");
		Image imageTour = new Image("http://findicons.com/files/icons/2192/flavour_extended/48/user_group.png");
		Image imageList = new Image("http://findicons.com/files/icons/2166/oxygen/48/fileview_text.png");
		Image imageUdlej = new Image("http://findicons.com/files/icons/2330/open_source_icons_gcons/32/van.png");
		Image imageKlip = new Image("http://findicons.com/files/icons/2448/wpzoom_developer/48/scissors.png");
		ImageView logoImage = new ImageView();
		ImageView quitImage = new ImageView();
		ImageView saleImage = new ImageView();
		ImageView saleImage2 = new ImageView();
		ImageView newImage = new ImageView();
		ImageView tourImage = new ImageView();
		ImageView listImage = new ImageView();
		ImageView udlejImage = new ImageView();
		ImageView klipImage = new ImageView();
		logoImage.setImage(imageLogo);
		quitImage.setImage(imageQuit);
		saleImage.setImage(imageSale);
		saleImage2.setImage(imageSale2);
		newImage.setImage(imageNew);
		tourImage.setImage(imageTour);
		listImage.setImage(imageList);
		udlejImage.setImage(imageUdlej);
		klipImage.setImage(imageKlip);
		logoImage.setFitHeight(300);
		quitImage.setFitHeight(15);
		saleImage.setFitHeight(15);
		saleImage2.setFitHeight(15);
		newImage.setFitHeight(15);
		tourImage.setFitHeight(15);
		listImage.setFitHeight(15);
		udlejImage.setFitHeight(15);
		klipImage.setFitHeight(15);
		quitImage.setPreserveRatio(true);
		saleImage.setPreserveRatio(true);
		saleImage2.setPreserveRatio(true);
		newImage.setPreserveRatio(true);
		tourImage.setPreserveRatio(true);
		listImage.setPreserveRatio(true);
		logoImage.setPreserveRatio(true);
		udlejImage.setPreserveRatio(true);
		klipImage.setPreserveRatio(true);
		logoImage.setEffect(new Reflection());
		logoImage.setCache(true);
		miProdukt.setGraphic(newImage);
		miSalg.setGraphic(saleImage);
		miQuit.setGraphic(quitImage);
		miRundvisning.setGraphic(tourImage);
		miOpdaterPrisliste.setGraphic(listImage);
		miSalgsOversigt.setGraphic(saleImage2);
		miUdlejningsKalender.setGraphic(udlejImage);
		miKlippekortOversigt.setGraphic(klipImage);

		group.getChildren().addAll(logoImage, lblTitel);

		Path path = new Path();
		path.getElements().add(new MoveTo(50, 50));
		path.getElements().add(new CubicCurveTo(50, 120, 0, 400, 300, 240));
		PathTransition pathTransition = new PathTransition();
		pathTransition.setDuration(Duration.seconds(5));
		pathTransition.setPath(path);
		pathTransition.setNode(logoImage);
		pathTransition.setOrientation(PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT);
		pathTransition.setCycleCount(1);
		pathTransition.setAutoReverse(true);
		pathTransition.play();

		FadeTransition ft = new FadeTransition(Duration.seconds(1.5), logoImage);
		ft.setFromValue(0.1);
		ft.setToValue(1);
		ft.setCycleCount(3);
		ft.setAutoReverse(true);
		ft.play();

		root.setCenter(group);
		root.setTop(mb);
	}

	// sætter center af borderpane til rundvisning
	private void rundvisning(BorderPane root) {
		RundvisningsPane p = new RundvisningsPane();
		root.setCenter(p);
	}
	// sætter center af borderpane til salg
	private void salg(BorderPane root) {
		SalgsPane p = new SalgsPane();
		root.setCenter(p);
	}
	// sætter center af borderpane til salgsoversigt
	private void salgsOversigt(BorderPane root) {
		SalgsOversigtPane p = new SalgsOversigtPane();
		root.setCenter(p);
	}
	// sætter center af borderpane til klipoversigt
	private void klipOversigt(BorderPane root) {
		KlipOversigtPane p = new KlipOversigtPane();
		root.setCenter(p);
	}
	// sætter center af borderpane til produkt(type) administerering
	private void updateProdukt(BorderPane root) {
		ProduktPane p = new ProduktPane();
		root.setCenter(p);
	}
	// sætter center af borderpane til prisliste administerering
	private void updatePrisliste(BorderPane root) {
		PrisListePane p = new PrisListePane();
		root.setCenter(p);
	}
	// sætter center af borderpane til udlejningskalender
	private void udlejningsKalender(BorderPane root) {
		UdlejningsKalenderPane p = new UdlejningsKalenderPane();
		root.setCenter(p);
	}

}
