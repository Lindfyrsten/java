package guifx;

import java.time.LocalDate;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Udlejning;
import service.Service;

public class UdlejningsWindow extends Stage {
	private final TextField txfNavn;
	private final TextField txfCpr;
	private final TextField txfNr;
	private Udlejning udlejning;
	private final DatePicker dpStart, dpSlut;
	private final Label lblError;
	public UdlejningsWindow() {

		setTitle("Udlejning");
		setResizable(false);
		initModality(Modality.APPLICATION_MODAL);

		GridPane pane = new GridPane();
		pane.setPadding(new Insets(10));
		pane.setVgap(10);
		pane.setHgap(10);

		Scene scene = new Scene(pane);
		setScene(scene);

		DropShadow ds = new DropShadow();
		ds.setOffsetY(3.0f);
		ds.setColor(Color.color(0.4f, 0.4f, 0.4f));

		Label lblTitel = new Label("Udlejning");
		lblTitel.setFont(Font.font("GARAMOND", FontWeight.BOLD, 42));
		lblTitel.setEffect(ds);
		lblTitel.setCache(true);
		Label lblNavn = new Label("Navn");
		Label lblCpr = new Label("Cpr nr.");
		Label lblNr = new Label("Tlf nummer");
		Label lblDatoFra = new Label("Udlejningsdato");
		Label lblDatoTil = new Label("Afleveringsdato");
		lblError = new Label();
		lblError.setTextFill(Color.RED);

		txfNavn = new TextField();
		txfCpr = new TextField();
		txfNr = new TextField();

		Button btnOk = new Button("OK");
		btnOk.setOnAction(event -> okAction());
		Button btnCancel = new Button("Annuller");
		btnCancel.setOnAction(event -> cancelAction());

		dpStart = new DatePicker();
		dpSlut = new DatePicker();
		dpSlut.setDisable(true);
		Service.fjernTidligereDato(dpStart, LocalDate.now());
		dpStart.setOnAction(event -> dateChanged());

		pane.add(lblTitel, 0, 0, 2, 1);
		pane.add(lblNavn, 0, 1);
		pane.add(txfNavn, 1, 1);
		pane.add(lblCpr, 0, 2);
		pane.add(txfCpr, 1, 2);
		pane.add(lblNr, 0, 3);
		pane.add(txfNr, 1, 3);
		pane.add(lblDatoFra, 0, 4);
		pane.add(dpStart, 1, 4);
		pane.add(lblDatoTil, 0, 5);
		pane.add(dpSlut, 1, 5);
		pane.add(lblError, 0, 6, 2, 1);
		pane.add(btnOk, 0, 7);
		pane.add(btnCancel, 1, 7);

		GridPane.setHalignment(lblTitel, HPos.CENTER);

	}

	// Gør datoer på og før valgte start dato utilgængelige ved valg af
	// slutdato.
	private void dateChanged() {
		if (dpStart.getValue() != null) {
			dpSlut.setDisable(false);
			Service.fjernTidligereDato(dpSlut, dpStart.getValue().plusDays(1));
			dpSlut.setValue(dpStart.getValue().plusDays(1));
		}
	}

	// Sætter de nødvendige oplysninger på nyoprettet rundvisning
	private void okAction() {
		String navn = txfNavn.getText();
		String cprNr = txfCpr.getText();
		int kontaktNr = -1;
		try {
			kontaktNr = Integer.parseInt(txfNr.getText());
		} catch (Exception e) {
			// do nothing
		}
		LocalDate startDato = dpStart.getValue();
		LocalDate slutDato = dpSlut.getValue();
		try {
			udlejning = Service.createUdlejning(navn, cprNr, kontaktNr, startDato, slutDato);
		} catch (Exception e) {
			lblError.setText(e.getMessage());
			return;
		}
		hide();
	}

	// Annuller og luk vindue
	private void cancelAction() {
		hide();
	}

	public Udlejning getUdlejning() {
		return udlejning;
	}
}
