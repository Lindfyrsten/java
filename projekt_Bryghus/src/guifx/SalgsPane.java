package guifx;

import java.util.ArrayList;
import java.util.Optional;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import model.Betalingsform;
import model.Pris;
import model.Prisliste;
import model.ProduktType;
import model.Rabat;
import model.Salg;
import model.Salgslinje;
import model.Sampakning;
import model.Udlejning;
import service.Service;

public class SalgsPane extends BorderPane {
	private final TabPane tabPane;
	private Tab tab;
	private ToggleGroup groupRabat, groupSalgUdlej;
	private final ListView<Pris> lvwPriser = new ListView<>();
	private final ListView<Salgslinje> lvwKurv = new ListView<>();
	private Button btnKurv, btnBetal, btnSlet;
	private TextField txfRabat;
	private final int initialValue = 1;
	private final Spinner<Integer> spinner = new Spinner<>();
	private SpinnerValueFactory<Integer> valueFactory;
	private final ComboBox<ProduktType> cbType = new ComboBox<>();
	private Prisliste prisliste;
	private HBox hbxRabat, btnBox;
	private Label lblPctKr;
	private final Label lblListe = new Label("");
	private Rabat rabat;
	private Salg salg;

	public SalgsPane() {
		salg = Service.createSalg();
		tabPane = new TabPane();
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		for (Prisliste pl : Service.getPrislister()) {
			String s = pl.getNavn();
			tab = new Tab(s);
			tabPane.getTabs().add(tab);
		}
		setTop(tabPane);
		tabPane.getSelectionModel().selectedItemProperty().addListener(event -> tabChanged());

		GridPane pane = new GridPane();
		pane.setPadding(new Insets(10, 10, 10, 10));
		pane.setVgap(10);
		pane.setHgap(10);
		initContent(pane);
		setCenter(pane);
	}

	private void initContent(GridPane pane) {
		groupRabat = new ToggleGroup();
		groupSalgUdlej = new ToggleGroup();
		hbxRabat = new HBox(10);
		String[] rabatter = {"Ingen rabat", "Procent rabat", "Flad rabat"};
		RadioButton rb;
		for (String element : rabatter) {
			rb = new RadioButton();
			rb.setToggleGroup(groupRabat);
			rb.setText(element);
			rb.setUserData(element);
			hbxRabat.getChildren().add(rb);
		}
		RadioButton rbSalg = new RadioButton();
		RadioButton rbUdlej = new RadioButton();
		rbSalg.setToggleGroup(groupSalgUdlej);
		rbUdlej.setToggleGroup(groupSalgUdlej);
		rbSalg.setText("Salg");
		rbUdlej.setText("Udlejning");
		rbSalg.setUserData("Salg");
		rbUdlej.setUserData("Udlejning");
		HBox hbxSalgUdlej = new HBox(10, rbSalg, rbUdlej);

		groupRabat.getToggles().get(0).setSelected(true);
		groupRabat.selectedToggleProperty().addListener(event -> toggleRadioButtonRabat());
		groupSalgUdlej.getToggles().get(0).setSelected(true);
		groupSalgUdlej.selectedToggleProperty().addListener(event -> toggleRadioButtonSalgUdlej());

		DropShadow ds = new DropShadow();
		ds.setOffsetY(3.0f);
		ds.setColor(Color.color(0.4f, 0.4f, 0.4f));

		Label lblTitel = new Label("Salg");
		Label lblKurv = new Label("Kurv");
		Label lblAntal = new Label("Antal: ");
		Label lblType = new Label("Vælg type");
		lblTitel.setFont(Font.font("GARAMOND", FontWeight.BOLD, 42));
		lblTitel.setEffect(ds);
		lblTitel.setCache(true);
		lblListe.setEffect(ds);
		lblListe.setCache(true);
		lblListe.setFont(Font.font("GARAMOND", FontWeight.BOLD, 26));
		lblKurv.setEffect(ds);
		lblKurv.setCache(true);
		lblKurv.setFont(Font.font("GARAMOND", FontWeight.BOLD, 26));
		btnKurv = new Button("Tilføj til kurv");
		btnKurv.setOnAction(event -> kurv());
		btnBetal = new Button("Betal/udlej");
		btnBetal.setOnAction(event -> betal());
		btnBetal.setPrefSize(90, 50);
		btnSlet = new Button("Fjern fra kurv");
		btnSlet.setOnAction(event -> slet());
		txfRabat = new TextField();
		txfRabat.setMaxWidth(75);
		txfRabat.setVisible(false);
		lblPctKr = new Label("");

		HBox hbxType = new HBox(10, lblType, cbType);
		btnBox = new HBox(10, lblAntal, spinner, btnKurv, btnSlet, btnBetal);
		hbxRabat.getChildren().addAll(txfRabat, lblPctKr);

		lvwPriser.getSelectionModel().selectedItemProperty().addListener(event -> productChanged());
		cbType.getSelectionModel().selectedItemProperty().addListener(event -> typeChanged());

		valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 99, initialValue);
		spinner.setPrefWidth(60);
		spinner.setValueFactory(valueFactory);
		spinner.setDisable(true);

		pane.add(lblTitel, 0, 0, 2, 1);
		pane.add(lblListe, 0, 1);
		pane.add(lblKurv, 1, 1);
		pane.add(hbxType, 0, 3);
		pane.add(hbxSalgUdlej, 0, 2);
		pane.add(lvwKurv, 1, 2, 1, 3);
		pane.add(lvwPriser, 0, 4);
		pane.add(hbxRabat, 0, 5, 2, 1);
		pane.add(btnBox, 0, 6, 2, 1);

		GridPane.setHalignment(lblListe, HPos.CENTER);
		GridPane.setHalignment(lblKurv, HPos.CENTER);
		GridPane.setHalignment(lblTitel, HPos.CENTER);
		hbxRabat.setAlignment(Pos.BASELINE_CENTER);
		btnBox.setAlignment(Pos.BASELINE_CENTER);
		hbxType.setAlignment(Pos.BASELINE_CENTER);
		hbxSalgUdlej.setAlignment(Pos.BASELINE_CENTER);

		GridPane.setHalignment(txfRabat, HPos.LEFT);
		GridPane.setHalignment(lblPctKr, HPos.CENTER);

		tabChanged();
		productChanged();
	}

	// sætter prislisten hvis tab skiftes
	private void tabChanged() {
		cbType.getItems().clear();
		String tab = tabPane.getSelectionModel().getSelectedItem().getText();
		for (Prisliste pl : Service.getPrislister()) {
			if (pl.getNavn().equalsIgnoreCase(tab)) {
				prisliste = pl;
				lblListe.setText(pl.getNavn());
				toggleRadioButtonSalgUdlej();
			}
		}
	}

	// indsætter Pris på listview, som er tilknyttet prisliste, ved ændring af
	// produkttype
	private void typeChanged() {
		lvwPriser.getItems().clear();
		ProduktType pt = cbType.getSelectionModel().getSelectedItem();
		for (Pris p : Service.getPriser(prisliste)) {
			if (p.getProdukt().getProduktType() == pt) {
				lvwPriser.getItems().add(p);
			}
		}
	}

	// Opdaterer gui funktioner afhængig af om en Pris er valgt eller ej
	private void productChanged() {
		Pris p = lvwPriser.getSelectionModel().getSelectedItem();
		if (p != null) {
			spinner.setDisable(false);
			spinner.getValueFactory().setValue(initialValue);
			btnKurv.setDisable(false);
		} else {
			spinner.setDisable(true);
			btnKurv.setDisable(true);
		}
	}

	// opdaterer gui funktioner afhængig af hvilken rabat radiobutton er valgt
	private void toggleRadioButtonRabat() {
		String s = groupRabat.getSelectedToggle().getUserData().toString();
		if (s.equalsIgnoreCase("Ingen rabat")) {
			txfRabat.setVisible(false);
			lblPctKr.setVisible(false);
		} else {
			txfRabat.setVisible(true);
			lblPctKr.setVisible(true);
			if (s.equalsIgnoreCase("Procent rabat")) {
				lblPctKr.setText("Procent");
			} else {
				lblPctKr.setText("Kroner");
			}

		}
	}

	// opdaterer Pris listview afhængig af hvilken produkttype radiobutton er
	// valgt
	private void toggleRadioButtonSalgUdlej() {
		ArrayList<ProduktType> typer = new ArrayList<>();
		for (Pris p : prisliste.getPriser()) {
			ProduktType pt = p.getProdukt().getProduktType();
			if (groupSalgUdlej.getSelectedToggle().getUserData().equals("Udlejning")) {
				if (pt.isUdlejningsProdukt() && !typer.contains(pt)) {
					typer.add(pt);
				}
			} else {
				if (pt.isSalgsProdukt() && !typer.contains(pt)) {
					typer.add(pt);
				}
			}
		}
		cbType.getItems().clear();
		cbType.getItems().setAll(typer);
		cbType.getSelectionModel().select(0);
	}

	// opretter ny salgslinje til salget ved tilføjelse til kurv
	private void kurv() {
		Pris p = lvwPriser.getSelectionModel().getSelectedItem();
		RadioButton toggleNr = (RadioButton) groupRabat.getSelectedToggle();
		String rbString = toggleNr.getText();

		// tjekker om der er valgt rabat
		if (rbString.equalsIgnoreCase("Ingen rabat")) {
			rabat = Service.createIngenRabat();
		} else {
			double pris = -1;
			try {
				pris = Double.parseDouble(txfRabat.getText());
			} catch (Exception e) {
				// do nothing
			}
			try {
				if (rbString.equalsIgnoreCase("Procent rabat")) {
					rabat = Service.createProcentRabat(pris);
				} else {
					rabat = Service.createPrisRabat(pris);
				}
			} catch (Exception e) {
				txfRabat.setPromptText(e.getMessage());
				return;
			}

		}
		int antal = spinner.getValue();
		// hvis Pris allerede er tilføjet salget, lægges antal oveni det
		// eksisterende
		for (Salgslinje s : salg.getSalgslinjer()) {
			if (p.getProdukt() == s.getProdukt()) {
				antal = antal + s.getAntal();
			}
		}
		// tjekker om produkt er på lager
		if (!Service.checkLager(p.getProdukt(), antal)) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setHeaderText("Ikke nok på lager");
			alert.setContentText("Lagerantal: " + p.getProdukt().getLagerAntal());
			alert.showAndWait();
			return;
		}
		// kalder service metode
		try {
			Service.createSalgslinje(salg, p, spinner.getValue(), rabat);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		valueFactory.setValue(1);
		lvwKurv.getItems().setAll(Service.getSalgslinjer(salg));
	}

	// "betaler" og gemmer salg
	private void betal() {
		if (!lvwKurv.getItems().isEmpty()) {
			boolean gennemførtSalg = false;
			boolean gennemførtUdlejning = false;
			boolean gennemførtPakning = true;
			Salg betalingsSalg = Service.createSalg();
			ArrayList<Salgslinje> udlejningsLinjer = new ArrayList<>();
			for (Salgslinje s : salg.getSalgslinjer()) {
				ProduktType pt = s.getProdukt().getProduktType();
				// hvis produkttype både kan være til udlejning og salg, vælges
				// hvilken det er til
				if (pt.isSalgsProdukt() && pt.isUdlejningsProdukt()) {
					Alert alert = new Alert(AlertType.CONFIRMATION);
					alert.setHeaderText(pt.getNavn() + ": " + s.getProdukt().getNavn()
							+ " er både et salgs- og udlejningsprodukt.");
					alert.setContentText("Skal produktet sælges eller udlejes?");
					ButtonType btnSale = new ButtonType("Sælg");
					ButtonType btnUdlej = new ButtonType("Udlej");
					alert.getButtonTypes().setAll(btnSale, btnUdlej);
					Optional<ButtonType> result = alert.showAndWait();
					if (result.get() == btnSale) {
						betalingsSalg.addSalgslinje(s);
					} else {
						udlejningsLinjer.add(s);
					}
				} else if (pt.isSalgsProdukt()) {
					betalingsSalg.addSalgslinje(s);
				} else {
					udlejningsLinjer.add(s);
				}
			}
			// Hvis produkt fra betalingsalg er sampakninger, tilføjes andre
			// produkter til denne
			for (Salgslinje s : betalingsSalg.getSalgslinjer()) {
				if (s.getProdukt() instanceof Sampakning) {
					Sampakning sampakning = (Sampakning) s.getProdukt();
					// Laver en kopi af sampakningsproduktet og Pris objektet,
					// som kan gemmes istedet for de oprindelige
					// objekter, som helst skal forblive genbrugelige.
					// De oprettes udenom ProduktType og Prisliste for at undgå
					// dupletter.
					Sampakning sKopi = Service.createSampakningKopi(sampakning);
					Pris p = Service.createPrisKopi(sKopi, s.getProduktPris());
					SampakningsWindow sp = new SampakningsWindow(sKopi);
					sp.showAndWait();
					// hvis sampakning annulleres, sættes boolean til false
					if (sKopi.getProdukter().isEmpty()) {
						gennemførtPakning = false;
					} else {
						// Sætter salgslinjens prisobjekt til vores kopi i
						// stedet
						s.setPrisObjekt(p);
					}
				}
			}

			Alert alert = new Alert(AlertType.INFORMATION);
			ChoiceDialog<Betalingsform> betalingsformer = new ChoiceDialog<>();
			betalingsformer.getItems().setAll(Service.getBetalingsformer());
			betalingsformer.setSelectedItem(betalingsformer.getItems().get(0));
			double pantUdlej = 0;
			double pantSalg = 0;
			// hvis der skal oprettes udlejning, åbnes udlejningswindow
			if (!udlejningsLinjer.isEmpty()) {
				UdlejningsWindow window = new UdlejningsWindow();
				window.showAndWait();
				// hvis udlejningswindow lukkes uden at gennemføre, vil
				// u.getNavn() være null og udlejninggennemført forbliver false
				Udlejning u = window.getUdlejning();
				if (u != null) {
					// tilføjer salgslinjen til nyoprettede udlejning
					for (Salgslinje sl : udlejningsLinjer) {
						u.addSalgslinje(sl);
						pantUdlej = u.udregnSamletPant();
					}
					// hvis betalingsSalg er tom, skal der ikke gøres nogen
					// betaling
					if (betalingsSalg.getSalgslinjer().isEmpty()) {
						alert.setHeaderText("Udlejning gennemført");
						if (pantUdlej == 0) {
							Service.saveSalg(u, null);
							gennemførtUdlejning = true;
							alert.showAndWait();

						} else {
							// hvis der skal betales for pant
							betalingsformer.setHeaderText("Vælg betalingsform.");
							betalingsformer.setContentText("At betale: " + pantUdlej + " (pant)");
							Optional<Betalingsform> result = betalingsformer.showAndWait();
							if (result.isPresent()) {
								Service.saveSalg(u, null);
								gennemførtUdlejning = true;
								alert.showAndWait();
							}
						}
					} else {
						// hvis der både skal oprettes udlejning og betales for
						// salg
						double pantSamlet = pantUdlej + betalingsSalg.udregnSamletPant();
						betalingsformer.setHeaderText(
								"Vælg betalingsform:\n(Afregning af udlejningsprodukter sker ved aflevering)");
						betalingsformer.setContentText("At betale: " + (betalingsSalg.udregnSamletPris() + pantSamlet)
								+ " (pant: " + pantSamlet + ")");
						Optional<Betalingsform> result = betalingsformer.showAndWait();
						if (result.isPresent()) {
							Service.saveSalg(betalingsSalg, result.get());
							Service.saveSalg(u, result.get());
							gennemførtSalg = true;
							gennemførtUdlejning = true;
							alert.setHeaderText("Salg og udlejning gennemført");
							alert.showAndWait();
						}
					}
				}
			} else if (gennemførtPakning == true) {
				// hvis der kun skal laves et salg og evt pakning ikke er sat
				// til false
				pantSalg = betalingsSalg.udregnSamletPant();
				betalingsformer.setHeaderText("Vælg betalingsform.");

				betalingsformer.setContentText("At betale: " + betalingsSalg.udregnSamletPris());
				if (pantSalg > 0) {
					betalingsformer.setContentText("At betale: " + (betalingsSalg.udregnSamletPris() + pantSalg)
							+ " (pant: " + pantSalg + ")");
				}
				Optional<Betalingsform> result = betalingsformer.showAndWait();
				if (result.isPresent()) {
					Service.saveSalg(betalingsSalg, result.get());
					gennemførtSalg = true;
					alert.setHeaderText("Salg gennemført");
					alert.showAndWait();

				}
			}
			// hvis både salg og udlejning er blevet gennemført, fjernes de far
			// kurven
			if (gennemførtSalg && gennemførtUdlejning) {
				salg = Service.createSalg();
				lvwKurv.getItems().clear();
			} else if (gennemførtSalg && !gennemførtUdlejning) {
				// hvis kun salgsprodukter skal fjernes
				for (Salgslinje s : betalingsSalg.getSalgslinjer()) {
					salg.removeSalgslinje(s);
					reset();
				}
			} else if (!gennemførtSalg && gennemførtUdlejning) {
				// hvis kun udlejningsprodukter skal fjernes
				for (Salgslinje s : udlejningsLinjer) {
					salg.removeSalgslinje(s);
					reset();
				}
			}
		}
	}

	// fjerner Pris fra kurv og justerer lagerAntal
	private void slet() {
		Salgslinje sl = lvwKurv.getSelectionModel().getSelectedItem();
		if (lvwKurv.getSelectionModel().getSelectedItem() != null) {
			sl.getProdukt().setLagerAntal(sl.getProdukt().getLagerAntal() + sl.getAntal());
			Service.deleteSalgslinje(salg, sl);
			lvwKurv.getItems().setAll(Service.getSalgslinjer(salg));
		}
	}
	private void reset() {
		lvwKurv.getItems().setAll(salg.getSalgslinjer());
		groupRabat.selectToggle((Toggle) hbxRabat.getChildren().get(0));
		txfRabat.clear();
	}
}
