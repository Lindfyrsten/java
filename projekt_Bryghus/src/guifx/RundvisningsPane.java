package guifx;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Optional;
import java.util.TreeSet;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import model.Betalingsform;
import model.Rundvisning;
import service.Service;

public class RundvisningsPane extends BorderPane {
	private TextField txfPris, txfNr;
	private final Spinner<Integer> spinner = new Spinner<>();
	private final int initialValue = 1;
	private final ListView<Rundvisning> lvwRundvisninger = new ListView<>();
	private Button btnOpret, btnBetal, btnAnnuller, btnOk;
	private DatePicker dpDato;
	private ComboBox<LocalTime> cbxStart, cbxSlut;
	private Label lblError;
	SpinnerValueFactory<Integer> valueFactory;
	private Rundvisning rundvisning;

	public RundvisningsPane() {
		GridPane pane = new GridPane();
		pane.setPadding(new Insets(10, 10, 10, 10));
		pane.setVgap(20);
		pane.setHgap(10);
		setCenter(pane);

		initContent(pane);
	}

	public void initContent(GridPane pane) {
		DropShadow ds = new DropShadow();
		ds.setOffsetY(3.0f);
		ds.setColor(Color.color(0.4f, 0.4f, 0.4f));

		Label lblTitel = new Label("Rundvisning");
		lblTitel.setFont(Font.font("GARAMOND", FontWeight.BOLD, 42));
		lblTitel.setEffect(ds);
		lblTitel.setCache(true);
		Label lblOpret = new Label("Opret");
		lblOpret.setFont(Font.font("GARAMOND", FontWeight.BOLD, 26));
		lblOpret.setEffect(ds);
		lblOpret.setCache(true);
		Label lblRundvisninger = new Label("Aktive rundvisninger");
		lblRundvisninger.setFont(Font.font("GARAMOND", FontWeight.BOLD, 26));
		lblRundvisninger.setEffect(ds);
		lblRundvisninger.setCache(true);
		Label lblPersoner = new Label("Antal personer: ");
		spinner.setPrefWidth(60);

		Label lblNr = new Label("Kontakt nr.: ");
		txfNr = new TextField();
		Label lblPris = new Label("Pris pr. person: ");
		txfPris = new TextField();
		Label lblDato = new Label("Dato");
		Label lblStart = new Label("Start tid");
		Label lblSlut = new Label("Slut tid");
		dpDato = new DatePicker();
		dpDato = Service.fjernTidligereDato(dpDato, LocalDate.now().plusDays(1));
		cbxStart = new ComboBox<>();
		cbxSlut = new ComboBox<>();
		cbxStart.getItems().setAll(setTimes(LocalTime.of(9, 0), LocalTime.of(17, 0)));
		cbxStart.setOnAction(event -> adjustSlutTid(cbxStart.getValue(), LocalTime.of(17, 0)));
		cbxSlut.getItems().setAll(setTimes(LocalTime.of(10, 0), LocalTime.of(17, 0)));
		btnOpret = new Button("Ny rundvisning");
		btnOpret.setPrefSize(110, 40);
		btnOpret.setOnAction(event -> opret());
		btnBetal = new Button("Betal");
		btnBetal.setPrefSize(110, 40);
		btnBetal.setOnAction(event -> betal());
		btnAnnuller = new Button("Annuller");
		btnAnnuller.setPrefSize(110, 40);
		btnAnnuller.setOnAction(event -> clear());
		btnOk = new Button("OK");
		btnOk.setPrefSize(110, 40);
		btnOk.setOnAction(event -> okAction());
		lblError = new Label();
		lblError.setTextFill(Color.RED);
		lvwRundvisninger.setPrefHeight(200);
		lvwRundvisninger.getSelectionModel().selectedItemProperty().addListener(event -> selectionChanged());
		HBox hbxBtn = new HBox(20, btnOpret, btnOk, btnAnnuller, btnBetal);

		pane.add(lblTitel, 0, 0, 3, 1);
		pane.add(lblOpret, 0, 1, 2, 1);
		pane.add(lblRundvisninger, 2, 1);
		pane.add(lblPersoner, 0, 2);
		pane.add(spinner, 1, 2);
		pane.add(lblNr, 0, 3);
		pane.add(txfNr, 1, 3);
		pane.add(lblPris, 0, 4);
		pane.add(txfPris, 1, 4);
		pane.add(lblDato, 0, 5);
		pane.add(dpDato, 1, 5);
		pane.add(lblStart, 0, 6);
		pane.add(cbxStart, 1, 6);
		pane.add(lblSlut, 0, 7);
		pane.add(cbxSlut, 1, 7);
		pane.add(hbxBtn, 0, 8, 3, 1);
		pane.add(lblError, 1, 9, 2, 1);
		pane.add(lvwRundvisninger, 2, 2, 1, 6);

		hbxBtn.setAlignment(Pos.BASELINE_CENTER);
		GridPane.setHalignment(lblTitel, HPos.CENTER);
		GridPane.setHalignment(lblOpret, HPos.CENTER);
		GridPane.setHalignment(btnBetal, HPos.RIGHT);
		GridPane.setHalignment(btnBetal, HPos.RIGHT);
		clear();
	}

	// Tilføjer alle timer fra og med start tiden til og med sluttiden et
	// treeset<LocalTime> og returnerer det.
	private TreeSet<LocalTime> setTimes(LocalTime startInterval, LocalTime slutInterval) {
		TreeSet<LocalTime> times = new TreeSet<>();
		for (LocalTime t = startInterval; t.isBefore(slutInterval); t = t.plusHours(1)) {
			times.add(t);
		}
		return times;
	}

	// Skaber nyt tidsinterval på mulig valg af sluttid, afhængig af valget af
	// starttid. Slut tid sættes til tidligst at kunne vælges en time efter
	// starttid.
	private void adjustSlutTid(LocalTime start, LocalTime slut) {
		if (start != null) {
			cbxSlut.getItems().setAll(setTimes(start.plusHours(1), slut));
			cbxSlut.getSelectionModel().select(0);
		}
	}

	// Opdater gui objects, til oprettelse af ny rundvisning
	private void opret() {
		lvwRundvisninger.getSelectionModel().clearSelection();
		lvwRundvisninger.setDisable(true);
		dpDato.setValue(LocalDate.now().plusDays(1));
		btnOpret.setDisable(true);
		btnOk.setDisable(false);
		btnAnnuller.setDisable(false);
		valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 99, initialValue);
		spinner.setValueFactory(valueFactory);
		enableFields();
	}

	// Opret eller opdater rundvisning
	private void okAction() {

		int kontaktNr = -1;
		double prisPrPerson = -1;

		try {
			kontaktNr = Integer.parseInt(txfNr.getText());
			prisPrPerson = Double.parseDouble(txfPris.getText());
		} catch (NumberFormatException ex) {
			// do nothing
		}
		LocalDate dato = dpDato.getValue();
		LocalTime start = cbxStart.getValue();
		LocalTime slut = cbxSlut.getValue();
		int antalPersoner = spinner.getValue();
		try {
			if (rundvisning != null) {
				Service.updateRundvisning(rundvisning, antalPersoner, prisPrPerson, kontaktNr, start, slut, dato);
			} else {
				Service.createRundvisning(antalPersoner, prisPrPerson, kontaktNr, dato, start, slut);
			}
		} catch (Exception e) {
			lblError.setText(e.getMessage());
			return;
		}

		clear();
	}

	// Rydder alle gui objects og alt sættes tilbage til start indstillinger
	private void clear() {

		btnAnnuller.setDisable(true);
		btnOk.setDisable(true);
		btnBetal.setDisable(true);
		btnOpret.setDisable(false);
		txfNr.clear();
		txfPris.clear();
		cbxStart.getSelectionModel().clearSelection();
		cbxSlut.setValue(null);
		lvwRundvisninger.setDisable(false);
		lvwRundvisninger.getItems().setAll(Service.getAktiveRundvisninger());
		lblError.setText("");
		dpDato.setValue(null);
		disableFields();

	}

	// Ændrer gui objects afhængig af rundvisning valgt
	private void selectionChanged() {
		rundvisning = lvwRundvisninger.getSelectionModel().getSelectedItem();
		if (rundvisning != null) {
			// gør det muligt at opdatere rundvisninger som endnu ikke er
			// foregået
			if (rundvisning.getStartTid().isAfter(LocalDateTime.now())) {
				enableFields();
				btnOk.setDisable(false);
			}
			valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 99, rundvisning.getAntalPersoner());
			spinner.setValueFactory(valueFactory);
			txfNr.setText("" + rundvisning.getKontaktNr());
			txfPris.setText("" + rundvisning.getPrisPrPerson());
			dpDato.setValue(rundvisning.getStartTid().toLocalDate());
			cbxStart.setValue(rundvisning.getStartTid().toLocalTime());
			cbxSlut.setValue(rundvisning.getSlutTid().toLocalTime());
			btnOpret.setDisable(true);
			btnAnnuller.setDisable(false);
			// gør det muligt at betale, hvis rundvisning endnu ikke er betalt
			if (rundvisning.getBetalingsform() != null) {
				btnBetal.setDisable(true);
			} else {
				btnBetal.setDisable(false);
			}

		} else {
			clear();
		}
	}

	// Sætter betalingsform af rundvisning for at simulere betaling
	private void betal() {
		ChoiceDialog<Betalingsform> dialog = new ChoiceDialog<>();
		dialog.getItems().setAll(Service.getUsableBetalingsformer());
		dialog.setTitle("Betal for rundvisning");
		dialog.setHeaderText("Vælg betalingsform");
		dialog.setSelectedItem(dialog.getItems().get(0));
		Optional<Betalingsform> result = dialog.showAndWait();
		if (result.isPresent()) {
			Service.betalRundvisning(rundvisning, result.get());
		}
		clear();
	}

	// Gør gui objects til oprettelse og opdateringer utilgængelige
	private void disableFields() {
		txfNr.setDisable(true);
		txfPris.setDisable(true);
		cbxStart.setDisable(true);
		cbxSlut.setDisable(true);
		spinner.setDisable(true);
		dpDato.setDisable(true);

	}

	// Gør gui objects til oprettelse og opdateringer tilgængelige
	private void enableFields() {
		txfNr.setDisable(false);
		txfPris.setDisable(false);
		cbxStart.setDisable(false);
		cbxSlut.setDisable(false);
		spinner.setDisable(false);
		dpDato.setDisable(false);
	}
}
