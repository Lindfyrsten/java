package guifx;

import java.util.ArrayList;
import java.util.Optional;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Produkt;
import model.ProduktType;
import model.Sampakning;
import service.Service;

public class SampakningsWindow extends Stage {
	private final ListView<Produkt> lvwFlasker, lvwGlas, lvwProdukter;
	private final Label lblFlaskeCount, lblGlasCount, lblError;
	private final Sampakning sp;
	private int flaskeCount, glasCount;
	private final ArrayList<Produkt> produkter;

	public SampakningsWindow(Sampakning sp) {
		this.sp = sp;
		flaskeCount = sp.getFlaskAntal();
		glasCount = sp.getGlasAntal();
		produkter = new ArrayList<>();

		setTitle("Sammenpakning");
		setResizable(false);
		initModality(Modality.APPLICATION_MODAL);
		initStyle(StageStyle.UNDECORATED);

		GridPane pane = new GridPane();
		pane.setPadding(new Insets(10, 10, 10, 50));
		pane.setVgap(10);
		pane.setHgap(10);

		Scene scene = new Scene(pane, 600, 650);
		setScene(scene);

		DropShadow ds = new DropShadow();
		ds.setOffsetY(3.0f);
		ds.setColor(Color.color(0.4f, 0.4f, 0.4f));

		Label lblTitel = new Label("Sampakning");
		lblTitel.setFont(Font.font("GARAMOND", FontWeight.BOLD, 42));
		lblTitel.setEffect(ds);
		lblTitel.setCache(true);
		lblFlaskeCount = new Label("Tilføj flasker: " + flaskeCount);
		lblFlaskeCount.setFont(Font.font("GARAMOND", 24));
		lblFlaskeCount.setEffect(ds);
		lblFlaskeCount.setCache(true);
		lblGlasCount = new Label("Tilføj glas: " + glasCount);
		lblGlasCount.setFont(Font.font("GARAMOND", 24));
		lblGlasCount.setEffect(ds);
		lblGlasCount.setCache(true);
		Label lblPakke = new Label("Pakningsliste");
		lblPakke.setFont(Font.font("GARAMOND", 24));
		lblPakke.setEffect(ds);
		lblPakke.setCache(true);
		lblError = new Label("Ingen på lager");
		lblError.setTextFill(Color.RED);
		lblError.setVisible(false);

		lvwFlasker = new ListView<>();
		lvwFlasker.setPrefHeight(200);
		lvwGlas = new ListView<>();
		lvwGlas.setPrefHeight(200);
		lvwProdukter = new ListView<>();
		lvwProdukter.setPrefSize(200, 200);
		lvwFlasker.getSelectionModel().selectedItemProperty().addListener(event -> flaskChanged());
		lvwGlas.getSelectionModel().selectedItemProperty().addListener(event -> glasChanged());

		Button btnAdd = new Button("Tilføj");
		btnAdd.setOnAction(event -> addProduct());
		Button btnOk = new Button("OK");
		btnOk.setOnAction(event -> okAction());
		Button btnCancel = new Button("Annuller");
		btnCancel.setOnAction(event -> cancelAction());
		Button btnRemove = new Button("Fjern produkt");
		btnRemove.setOnAction(event -> removeProdukt());
		HBox hbxBtns = new HBox(30, btnAdd, btnCancel, btnOk, btnRemove);

		pane.add(lblTitel, 0, 0, 2, 1);
		pane.add(lblFlaskeCount, 0, 1);
		pane.add(lblGlasCount, 1, 1);
		pane.add(lvwFlasker, 0, 2);
		pane.add(lvwGlas, 1, 2);
		pane.add(hbxBtns, 0, 3, 2, 1);
		pane.add(lvwProdukter, 0, 4, 2, 1);
		pane.add(lblError, 0, 5, 2, 1);

		hbxBtns.setAlignment(Pos.BASELINE_CENTER);
		GridPane.setHalignment(lblTitel, HPos.CENTER);
		GridPane.setHalignment(lblFlaskeCount, HPos.CENTER);
		GridPane.setHalignment(lblGlasCount, HPos.CENTER);
		GridPane.setHalignment(lvwProdukter, HPos.CENTER);
		setItems();

	}

	// tilføjer tilgængelige flasker og glas til listviews
	private void setItems() {
		for (ProduktType pt : Service.getProduktTyper()) {
			if (pt.getNavn().equalsIgnoreCase("Flaske")) {
				if (flaskeCount > 0) {
					lvwFlasker.getItems().setAll(pt.getProdukter());
				} else {
					lvwFlasker.setDisable(true);
				}
			}
			if (pt.getNavn().equalsIgnoreCase("Glas")) {
				if (glasCount > 0) {
					lvwGlas.getItems().setAll(pt.getProdukter());
				} else {
					lvwGlas.setDisable(true);
				}
			}
		}
	}

	// tilføjer en flaske eller et glas til sampakningen
	private void addProduct() {
		// hvis flaskeCount = 0, skal der ikke tilføjes flere flasker
		if (flaskeCount > 0) {
			Produkt p = lvwFlasker.getSelectionModel().getSelectedItem();
			if (p != null) {
				// hvis lager ikke er tomt, tilføjes produktet og lagerAntal +
				// flaskeCount justeres
				if (!Service.checkTomtLager(p)) {
					produkter.add(p);
					p.setLagerAntal(p.getLagerAntal() - 1);
					flaskeCount--;
					updateLabels();
					lvwProdukter.getItems().setAll(produkter);
					lblError.setVisible(false);
				} else {
					lblError.setVisible(true);
				}
			}
		}
		// hvis glasCount = 0, skal der ikke tilføjes flere flasker
		if (glasCount > 0) {
			Produkt p = lvwGlas.getSelectionModel().getSelectedItem();
			if (p != null) {
				// hvis lager ikke er tomt, tilføjes produktet og lagerAntal +
				// glasCount justeres
				if (!Service.checkTomtLager(p)) {
					produkter.add(p);
					p.setLagerAntal(p.getLagerAntal() - 1);
					glasCount--;
					updateLabels();
					lvwProdukter.getItems().setAll(produkter);
					lblError.setVisible(false);
				} else {
					lblError.setVisible(true);
				}
			}
		}
	}

	// opretter sampakningen af valgte produkter
	private void okAction() {
		// hvis antallet af flasker og glas passer
		try {
			Service.addToSampakning(sp, produkter);
		} catch (Exception e) {
			lblError.setText(e.getMessage());
			return;
		}
		close();
	}

	// anuller oprettelse af sampakning og luk vindue
	private void cancelAction() {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setHeaderText("Annuller køb af sampakning");
		alert.setContentText("Vil du forlade købet af sampakning?");
		Optional<ButtonType> result = alert.showAndWait();
		// hvis produkter allerede er tilføjet de, justeres lagerAntal
		if (!produkter.isEmpty()) {
			for (Produkt p : produkter) {
				p.setLagerAntal(p.getLagerAntal() + 1);
			}
		}
		if (result.get() == ButtonType.OK) {
			close();
		}
	}

	// fjerne produkt fra sampakning og justerer lagerAntal + count
	private void removeProdukt() {
		Produkt p = lvwProdukter.getSelectionModel().getSelectedItem();
		if (p != null) {
			produkter.remove(p);
			p.setLagerAntal(p.getLagerAntal() + 1);
			if (p.getProduktType().getNavn().equalsIgnoreCase("Flaske")) {
				flaskeCount++;
				updateLabels();
			} else {
				glasCount++;
				updateLabels();
			}
		}
		lvwProdukter.getItems().setAll(produkter);
	}

	// opdater labels og gui funktioner
	private void updateLabels() {
		lblGlasCount.setText("Tilføj glas: " + glasCount);
		lblFlaskeCount.setText("Tilføj flasker: " + flaskeCount);
		if (glasCount == 0) {
			lvwGlas.setDisable(true);
			lvwGlas.getSelectionModel().clearSelection();
		} else {
			lvwGlas.setDisable(false);
		}
		if (flaskeCount == 0) {
			lvwFlasker.setDisable(true);
			lvwFlasker.getSelectionModel().clearSelection();
		} else {
			lvwFlasker.setDisable(false);
		}
	}

	private void flaskChanged() {
		if (lvwFlasker.getSelectionModel().getSelectedItem() != null) {
			lvwGlas.getSelectionModel().clearSelection();
		}
	}
	private void glasChanged() {
		if (lvwGlas.getSelectionModel().getSelectedItem() != null) {
			lvwFlasker.getSelectionModel().clearSelection();
		}
	}
}
