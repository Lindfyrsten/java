package guifx;

import javafx.beans.value.ChangeListener;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import model.PantProdukt;
import model.Produkt;
import model.ProduktType;
import service.Service;

public class ProduktPane extends GridPane {

	private final ListView<ProduktType> lvwTyper = new ListView<>();
	private final ListView<Produkt> lvwProdukt = new ListView<>();
	private final Button btnAnnuller, btnOpretProdukt, btnOpretType, btnOk;
	private final TextField txfNavn, txfLagerAntal, txfPantPris;
	private final CheckBox cbxUdlejningsprodukt, cbxSalgsprodukt, cbxPantProdukt;
	private ProduktType pt;
	private final Label lblError, lblAntal, lblNavn;
	private Produkt produkt;
	private int typeIndex;
	private final ChangeListener<Produkt> produktListener;
	ChangeListener<ProduktType> typeListener;

	public ProduktPane() {

		setPadding(new Insets(10, 10, 10, 10));
		setVgap(10);
		setHgap(10);
		Label lblTitel = new Label("Opret Produkt/Type");
		DropShadow ds = new DropShadow();
		ds.setOffsetY(3.0f);
		ds.setColor(Color.color(0.4f, 0.4f, 0.4f));
		lblTitel.setFont(Font.font("GARAMOND", FontWeight.BOLD, 36));
		lblTitel.setEffect(ds);
		lblTitel.setCache(true);
		GridPane.setHalignment(lblTitel, HPos.CENTER);

		txfNavn = new TextField();
		txfNavn.setPromptText("Navn");
		txfLagerAntal = new TextField();
		txfLagerAntal.setPromptText("Lager antal");
		txfPantPris = new TextField();
		txfPantPris.setMaxWidth(80);
		txfPantPris.setPromptText("Pant pris");

		cbxUdlejningsprodukt = new CheckBox("Udlejningsprodukt");
		cbxUdlejningsprodukt.setVisible(false);
		cbxSalgsprodukt = new CheckBox("Salgsprodukt");
		cbxSalgsprodukt.setVisible(false);
		cbxPantProdukt = new CheckBox("Pant");
		cbxPantProdukt.setVisible(false);
		cbxPantProdukt.setOnAction(event -> pantProduktChanged());
		btnAnnuller = new Button("Annuller");
		btnOpretProdukt = new Button("Nyt Produkt");
		btnOpretType = new Button("Ny Produkttype");
		btnOk = new Button("OK");
		btnAnnuller.setOnAction(event -> clear());
		btnOpretProdukt.setOnAction(event -> opretProdukt());
		btnOpretType.setOnAction(event -> opretProduktType());
		btnOk.setOnAction(event -> okAction());

		Label lblType = new Label("Type");
		lblType.setEffect(ds);
		lblType.setCache(true);
		lblType.setFont(Font.font("GARAMOND", FontWeight.BOLD, 18));
		Label lblProdukt = new Label("Produkt");
		lblProdukt.setEffect(ds);
		lblProdukt.setCache(true);
		lblProdukt.setFont(Font.font("GARAMOND", FontWeight.BOLD, 18));
		lblNavn = new Label("Navn");
		lblAntal = new Label("Lager antal");
		lblError = new Label();
		lblError.setStyle("-fx-text-fill: red");

		HBox btnBox = new HBox(70, btnOpretType, btnOpretProdukt);
		HBox confBox = new HBox(30, btnOk, btnAnnuller);
		add(lblTitel, 0, 0, 2, 1);
		add(lblType, 0, 1);
		add(lvwTyper, 0, 2);
		add(lblProdukt, 1, 1);
		add(lvwProdukt, 1, 2);
		add(btnBox, 0, 3, 2, 1);
		add(lblNavn, 0, 4);
		add(lblAntal, 1, 4);
		add(txfNavn, 0, 5);
		add(txfLagerAntal, 1, 5);
		add(cbxUdlejningsprodukt, 0, 6);
		add(cbxSalgsprodukt, 0, 6);
		add(cbxPantProdukt, 0, 6);
		add(txfPantPris, 0, 6);
		add(lblError, 0, 7);
		add(confBox, 1, 7);
		btnOk.setMinSize(50, 30);
		btnAnnuller.setMinHeight(30);

		GridPane.setHalignment(txfPantPris, HPos.CENTER);
		GridPane.setHalignment(cbxSalgsprodukt, HPos.RIGHT);
		confBox.setAlignment(Pos.BASELINE_RIGHT);
		GridPane.setHalignment(lblProdukt, HPos.CENTER);
		GridPane.setHalignment(lblType, HPos.CENTER);
		btnBox.setAlignment(Pos.BASELINE_CENTER);

		produktListener = (ov, oldProdukt, newProdukt) -> produktChanged();
		typeListener = (ov, oldProduktType, newProduktType) -> typeChanged();
		clear();
		lvwTyper.getItems().setAll(Service.getProduktTyper());
		lvwProdukt.getSelectionModel().selectedItemProperty().addListener(produktListener);
		lvwTyper.getSelectionModel().selectedItemProperty().addListener(typeListener);
		lvwTyper.getSelectionModel().select(0);
	}

	// Gør controls klar til oprettelse af produkt
	private void opretProdukt() {

		if (lvwTyper.getSelectionModel().getSelectedItem() == null) {
			lblError.setText("Vælg produkttype");
			return;
		}
		if (lvwTyper.getSelectionModel().getSelectedItem().isUdlejningsProdukt()) {
			cbxPantProdukt.setVisible(true);
			cbxPantProdukt.setSelected(false);

		}
		disableOptions();
		lvwProdukt.getSelectionModel().selectedItemProperty().removeListener(produktListener);
		lvwTyper.getSelectionModel().selectedItemProperty().removeListener(typeListener);
		lvwProdukt.getSelectionModel().clearSelection();
		lblNavn.setVisible(true);
		txfNavn.clear();
		txfNavn.setVisible(true);
		lblAntal.setVisible(true);
		txfLagerAntal.setVisible(true);
		txfLagerAntal.clear();
		cbxUdlejningsprodukt.setVisible(false);
		cbxSalgsprodukt.setVisible(false);
	}

	// Gør controls klar til oprettelse af produkttype
	private void opretProduktType() {
		disableOptions();
		lblNavn.setVisible(true);
		txfNavn.clear();
		txfNavn.setVisible(true);
		cbxUdlejningsprodukt.setVisible(true);
		cbxSalgsprodukt.setVisible(true);
		cbxUdlejningsprodukt.setSelected(false);
		cbxPantProdukt.setVisible(false);
		txfPantPris.setVisible(false);
		cbxSalgsprodukt.setSelected(false);
		lvwTyper.getSelectionModel().clearSelection();
		lvwProdukt.getSelectionModel().clearSelection();
	}

	// Opret eller opdater produkt/produkttype afhængig af hvilke objekter er
	// valgt
	private void okAction() {
		pt = lvwTyper.getSelectionModel().getSelectedItem();
		produkt = lvwProdukt.getSelectionModel().getSelectedItem();

		int lagerAntal = -1;
		double pantPris = -1;
		String navn = txfNavn.getText();
		// hvis der skal oprettes produkttype
		if (pt == null && produkt == null) {
			boolean udlejningsProdukt = false;
			boolean salgsprodukt = false;
			if (cbxUdlejningsprodukt.isSelected()) {
				udlejningsProdukt = true;
			}
			if (cbxSalgsprodukt.isSelected()) {
				salgsprodukt = true;
			}
			try {
				pt = Service.createProduktType(navn, udlejningsProdukt, salgsprodukt);
			} catch (Exception e) {
				lblError.setText(e.getMessage());
				return;
			}
			clear();
			lvwTyper.getItems().setAll(Service.getProduktTyper());
			lvwTyper.getSelectionModel().select(pt);

		}
		// hvis der skal oprettes produkt
		else if (produkt == null && txfLagerAntal.isVisible()) {

			try {
				lagerAntal = Integer.parseInt(txfLagerAntal.getText().trim());
				pantPris = Double.parseDouble(txfPantPris.getText().trim());
			} catch (NumberFormatException ex) {
				// do nothing
			}
			try {
				if (cbxPantProdukt.isSelected()) {
					produkt = Service.createPantProdukt(pt, navn, lagerAntal, pantPris);
				} else {
					produkt = Service.createProdukt(pt, navn, lagerAntal);
				}
			} catch (Exception e) {
				lblError.setText(e.getMessage());
				return;
			}
			clear();
			lvwTyper.getSelectionModel().select(typeIndex);
		}

		// hvis type skal opdateres
		else if (produkt == null) {

			boolean udlejningsProdukt = false;
			boolean salgsProdukt = false;
			if (cbxUdlejningsprodukt.isSelected()) {
				udlejningsProdukt = true;
			}
			if (cbxSalgsprodukt.isSelected()) {
				salgsProdukt = true;
			}
			try {
				Service.updateProduktType(pt, navn, salgsProdukt, udlejningsProdukt);
			} catch (Exception e) {
				lblError.setText(e.getMessage());
				return;
			}
			clear();
			lvwTyper.getItems().setAll(Service.getProduktTyper());
			lvwTyper.getSelectionModel().select(pt);
		}

		// hvis produkt skal opdateres
		else {
			try {
				lagerAntal = Integer.parseInt(txfLagerAntal.getText().trim());
			} catch (NumberFormatException ex) {
				// do nothing
			}
			int index = lvwProdukt.getSelectionModel().getSelectedIndex();
			try {
				Service.updateProdukt(produkt, navn, lagerAntal);
			} catch (Exception e) {
				lblError.setText(e.getMessage());
				return;
			}
			clear();
			lvwTyper.getSelectionModel().select(typeIndex);
			lvwProdukt.getSelectionModel().select(index);
		}
	}

	// Opdater controls når produkt vælges
	private void produktChanged() {
		produkt = lvwProdukt.getSelectionModel().getSelectedItem();
		if (produkt != null) {
			lblNavn.setVisible(true);
			btnOk.setDisable(false);
			btnAnnuller.setDisable(false);
			lvwTyper.getSelectionModel().clearSelection();
			cbxSalgsprodukt.setVisible(false);
			cbxUdlejningsprodukt.setVisible(false);
			lvwTyper.getSelectionModel().clearSelection();
			txfNavn.setText(produkt.getNavn());
			lblAntal.setVisible(true);
			txfLagerAntal.setVisible(true);
			txfLagerAntal.setText("" + produkt.getLagerAntal());
			// hvis pantprodukt, markeres checkbox og textfield pris gøres
			// synlig med pris
			if (produkt instanceof PantProdukt) {
				cbxPantProdukt.setVisible(true);
				cbxPantProdukt.setSelected(true);
				txfPantPris.setVisible(true);
				txfPantPris.setText("" + ((PantProdukt) produkt).getPantPris());
			} else {
				cbxPantProdukt.setVisible(false);
				cbxPantProdukt.setSelected(false);
				txfPantPris.setVisible(false);
				txfPantPris.clear();
			}
		}
	}

	// Opdater controls når produkttype vælges. Listview produkter ændres til at
	// indeholde produkter af produkttypen.
	private void typeChanged() {
		pt = lvwTyper.getSelectionModel().getSelectedItem();
		if (pt != null) {
			lblNavn.setVisible(true);
			btnOk.setDisable(false);
			btnAnnuller.setDisable(false);
			lvwProdukt.getSelectionModel().clearSelection();
			cbxPantProdukt.setVisible(false);
			cbxSalgsprodukt.setVisible(true);
			cbxUdlejningsprodukt.setVisible(true);
			txfPantPris.setVisible(false);
			txfLagerAntal.setVisible(false);
			lblAntal.setVisible(false);
			lblError.setText("");
			typeIndex = lvwTyper.getSelectionModel().getSelectedIndex();
			lvwProdukt.getItems().setAll(pt.getProdukter());
			txfNavn.setText(pt.getNavn());
			txfNavn.setVisible(true);
			cbxUdlejningsprodukt.setSelected(pt.isUdlejningsProdukt());
			cbxSalgsprodukt.setSelected(pt.isSalgsProdukt());
		}

	}

	// Ryd alle textfield, checkboxes etc. Clear både produkt og type selection.
	private void clear() {
		btnAnnuller.setDisable(true);
		btnOk.setDisable(true);
		btnOpretProdukt.setDisable(false);
		btnOpretType.setDisable(false);
		cbxUdlejningsprodukt.setSelected(false);
		cbxUdlejningsprodukt.setVisible(false);
		cbxSalgsprodukt.setSelected(false);
		cbxSalgsprodukt.setVisible(false);
		cbxPantProdukt.setSelected(false);
		cbxPantProdukt.setVisible(false);
		txfLagerAntal.clear();
		txfLagerAntal.setVisible(false);
		txfNavn.clear();
		txfNavn.setVisible(false);
		txfPantPris.clear();
		txfPantPris.setVisible(false);
		lblError.setText("");
		lblAntal.setVisible(false);
		lblNavn.setVisible(false);
		lvwTyper.getSelectionModel().clearSelection();
		lvwProdukt.getSelectionModel().clearSelection();
		lvwProdukt.getSelectionModel().selectedItemProperty().addListener(produktListener);
		lvwTyper.getSelectionModel().selectedItemProperty().addListener(typeListener);
	}

	// Gør knapperne til oprettelse utilgængelig når oprettelse/opdatering er i
	// gang. Annuller og OK gøres tilgængelige istedet.
	private void disableOptions() {
		btnOpretProdukt.setDisable(true);
		btnOpretType.setDisable(true);
		btnAnnuller.setDisable(false);
		btnOk.setDisable(false);
		lblError.setText("");
	}

	// Gør textfield med pantpris synlig/usynlig afhængig af om checkbox pant er
	// checket af.
	private void pantProduktChanged() {
		if (cbxPantProdukt.isSelected()) {
			txfPantPris.setVisible(true);
		} else {
			txfPantPris.setVisible(false);
		}
	}

}
