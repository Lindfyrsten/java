package guifx;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Pris;
import model.Prisliste;
import model.Produkt;
import model.ProduktType;
import service.Service;

public class PrisWindow extends Stage {
	private ComboBox<ProduktType> cbxTyper;
	private ComboBox<Produkt> cbxProdukter;
	private final Label lblError = new Label();
	private Button btnOk;
	private Button btnCancel;
	private TextField txfPris;
	private final Prisliste prisliste;

	public PrisWindow(Prisliste prisliste) {
		this.prisliste = prisliste;
		setTitle("Tilføj Produkt");
		setResizable(false);
		initModality(Modality.APPLICATION_MODAL);

		GridPane pane = new GridPane();
		initContent(pane);
		Scene scene = new Scene(pane);
		setScene(scene);
	}

	private void initContent(GridPane pane) {

		pane.setPadding(new Insets(10));
		pane.setHgap(20);
		pane.setVgap(10);
		DropShadow ds = new DropShadow();
		ds.setOffsetY(3.0f);
		ds.setColor(Color.color(0.4f, 0.4f, 0.4f));

		Label lblTitel = new Label("Tilføj Produkt");
		Label lblType = new Label("Vælg Type");
		Label lblProdukt = new Label("Vælg Produkt");
		Label lblPris = new Label("Pris");
		lblTitel.setFont(Font.font("GARAMOND", FontWeight.BOLD, 32));
		lblTitel.setEffect(ds);
		lblTitel.setCache(true);
		lblError.setTextFill(Color.RED);

		btnOk = new Button("OK");
		btnCancel = new Button("Annuller");
		btnOk.setOnAction(event -> okAction());
		btnCancel.setOnAction(event -> cancelAction());

		txfPris = new TextField();
		txfPris.setMaxWidth(75);

		HBox hbxBtn = new HBox(20, btnOk, btnCancel);

		cbxTyper = new ComboBox<>();
		cbxProdukter = new ComboBox<>();
		cbxTyper.setOnAction(event -> typeChanged());
		cbxTyper.getItems().setAll(Service.getProduktTyper());
		cbxProdukter.setPrefWidth(140);
		cbxTyper.setPrefWidth(140);

		pane.add(lblTitel, 0, 0, 2, 1);
		pane.add(lblType, 0, 1);
		pane.add(cbxTyper, 1, 1);
		pane.add(lblProdukt, 0, 2);
		pane.add(cbxProdukter, 1, 2);
		pane.add(lblPris, 0, 3);
		pane.add(txfPris, 1, 3);
		pane.add(lblError, 0, 4, 2, 1);
		pane.add(hbxBtn, 0, 5, 2, 1);

		hbxBtn.setAlignment(Pos.BASELINE_CENTER);
		GridPane.setHalignment(lblTitel, HPos.CENTER);

	}

	private void okAction() {
		ProduktType pt = cbxTyper.getSelectionModel().getSelectedItem();
		if (pt == null) {
			lblError.setText("Vælg type");
			return;
		}
		Produkt p = cbxProdukter.getSelectionModel().getSelectedItem();
		double pris = -1;
		try {
			pris = Double.parseDouble(txfPris.getText().trim());
		} catch (NumberFormatException ex) {
			// do nothing
		}
		try {
			Service.createPris(prisliste, p, pris);
		} catch (Exception e) {
			lblError.setText(e.getMessage());
			return;
		}
		hide();
	}

	// ved ændring af produkttype i combobox
	private void typeChanged() {
		cbxProdukter.getItems().clear();
		lblError.setText("");
		ProduktType pt = cbxTyper.getValue();
		if (pt != null) {
			cbxProdukter.setDisable(false);
			cbxProdukter.getItems().setAll(pt.getProdukter());
			for (Pris p : prisliste.getPriser()) {
				Produkt produkt = p.getProdukt();
				if (cbxProdukter.getItems().contains(produkt)) {
					cbxProdukter.getItems().remove(produkt);
				}
			}
			// hvis alle produkter under produkttypen allerede er tilføjet
			// listen
			if (cbxProdukter.getItems().isEmpty()) {
				cbxProdukter.setPromptText("Alle tilføjet");
				cbxProdukter.setDisable(true);
				txfPris.setDisable(true);
			} else {
				cbxProdukter.setDisable(false);
				cbxProdukter.getSelectionModel().select(0);
				txfPris.setDisable(false);
			}
		}
	}

	private void cancelAction() {
		hide();
	}
}
