package guifx;

import java.util.ArrayList;
import java.util.Optional;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import model.Pris;
import model.Prisliste;
import model.ProduktType;
import service.Service;

public class PrisListePane extends BorderPane {
	private final TabPane tabPane;
	private Tab tab;
	private final ComboBox<ProduktType> cbTyper = new ComboBox<>();
	private Label lblListe, lblNavn, lblError;
	private Prisliste prisliste;
	private final ListView<Pris> lvwPriser = new ListView<>();
	private Button btnAddProduct, btnRemoveProduct, btnDeleteList, btnOk, btnCreateList, btnCancel, btnUpdate;
	private TextField txfNavn;

	public PrisListePane() {
		tabPane = new TabPane();
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		setTabs(tabPane);
		tabPane.getSelectionModel().selectedItemProperty().addListener(event -> tabChanged());

		GridPane pane = new GridPane();
		pane.setPadding(new Insets(10, 10, 10, 10));
		pane.setVgap(10);
		pane.setHgap(10);

		setTop(tabPane);
		initContent(pane);
		setCenter(pane);
		tabChanged();
		clear();
	}

	private void initContent(GridPane pane) {

		DropShadow ds = new DropShadow();
		ds.setOffsetY(3.0f);
		ds.setColor(Color.color(0.4f, 0.4f, 0.4f));

		lblListe = new Label();
		lblNavn = new Label("Navn:");
		Label lblTitel = new Label("Opdater prisliste");
		Label lblType = new Label("Vælg type");
		lblTitel.setFont(Font.font("GARAMOND", FontWeight.BOLD, 42));
		lblTitel.setEffect(ds);
		lblTitel.setCache(true);
		lblListe.setEffect(ds);
		lblListe.setCache(true);
		lblListe.setFont(Font.font("GARAMOND", FontWeight.BOLD, 26));
		lblError = new Label();
		lblError.setStyle("-fx-text-fill: red");

		btnAddProduct = new Button("Tilføj produkt");
		btnRemoveProduct = new Button("Fjern produkt");
		btnDeleteList = new Button("Slet liste");
		btnCreateList = new Button("Ny liste");
		btnCancel = new Button("Annuller");
		btnUpdate = new Button("Opdater");
		btnOk = new Button("OK");
		btnCreateList.setPrefSize(75, 40);
		btnAddProduct.setOnAction(event -> addPris());
		btnCreateList.setOnAction(event -> createList());
		btnCancel.setOnAction(event -> clear());
		btnOk.setOnAction(event -> okAction());
		btnUpdate.setOnAction(event -> updatePris());
		btnDeleteList.setOnAction(event -> deleteList());
		btnRemoveProduct.setOnAction(event -> removePris());

		txfNavn = new TextField();

		HBox hbxType = new HBox(10, lblType, cbTyper);
		HBox hbxProduct = new HBox(30, btnAddProduct, btnRemoveProduct, btnUpdate, btnDeleteList);
		lvwPriser.getSelectionModel().selectedItemProperty().addListener(event -> prisChanged());
		cbTyper.getSelectionModel().selectedItemProperty().addListener(event -> updatePrislist());

		pane.add(lblTitel, 0, 0, 5, 1);
		pane.add(lblListe, 0, 1, 5, 1);
		pane.add(hbxType, 0, 2);
		pane.add(lvwPriser, 0, 3, 1, 3);
		pane.add(btnCreateList, 1, 3, 2, 1);
		pane.add(lblError, 2, 3);
		pane.add(lblNavn, 1, 4);
		pane.add(txfNavn, 2, 4);
		pane.add(btnOk, 1, 5);
		pane.add(btnCancel, 2, 5);
		pane.add(hbxProduct, 0, 6, 3, 1);

		GridPane.setValignment(btnOk, VPos.TOP);
		GridPane.setValignment(btnCancel, VPos.TOP);
		GridPane.setHalignment(btnDeleteList, HPos.RIGHT);
		GridPane.setHalignment(lblListe, HPos.CENTER);
		GridPane.setHalignment(lblTitel, HPos.CENTER);
		GridPane.setHalignment(lblError, HPos.RIGHT);
		hbxType.setAlignment(Pos.BASELINE_CENTER);
		hbxProduct.setAlignment(Pos.BASELINE_CENTER);
	}

	// Ryder alle textfields og sætter funktioner tilbage til start settings
	private void clear() {
		lblError.setText("");
		txfNavn.clear();
		lblNavn.setVisible(false);
		txfNavn.setVisible(false);
		btnOk.setVisible(false);
		btnCancel.setVisible(false);
		btnAddProduct.setDisable(false);
		btnRemoveProduct.setDisable(true);
		btnUpdate.setDisable(true);
		btnCreateList.setDisable(false);
		btnDeleteList.setDisable(false);
	}

	// De unødvendige controls gøres utilgængelige i forbindelse med at oprette
	// ny prisliste
	private void createList() {
		clear();
		btnAddProduct.setDisable(true);
		btnRemoveProduct.setDisable(true);
		lblNavn.setVisible(true);
		txfNavn.setVisible(true);
		btnOk.setVisible(true);
		btnCancel.setVisible(true);
		btnDeleteList.setDisable(true);
		btnCreateList.setDisable(true);
	}

	// Sletter prisliste
	private void deleteList() {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Slet Prisliste");
		alert.setHeaderText("Bekræft sletning af liste");
		alert.setContentText("Vil du fjerne prislisten: " + prisliste.getNavn() + "?");
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK) {
			Service.removePrisliste(prisliste);
			setTabs(tabPane);
		}
	}

	// Tilføj eksisterende produkt til prisliste med en pris
	private void addPris() {
		PrisWindow pw = new PrisWindow(prisliste);
		pw.showAndWait();
		cbTyper.getItems().setAll(Service.getPrislistTyper(prisliste));
		updatePrislist();
	}

	// Opdater pris på prisliste
	private void updatePris() {
		Pris produktPris = lvwPriser.getSelectionModel().getSelectedItem();
		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle("Opdater");
		dialog.setHeaderText("Opdater produktpris");
		dialog.setContentText("Indtast ny pris");
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {
			double pris = -1;
			try {
				pris = Double.parseDouble(result.get().trim());
			} catch (NumberFormatException ex) {
				// do nothing
			}
			try {
				Service.updatePris(produktPris, pris);
			} catch (Exception e) {
				lblError.setText(e.getMessage());
			}
			updatePrislist();
			lvwPriser.getSelectionModel().select(produktPris);
		}

	}

	// Fjern pris fra prisliste
	private void removePris() {
		Pris p = lvwPriser.getSelectionModel().getSelectedItem();
		Service.removePris(prisliste, p);
		updatePrislist();

	}

	// Opret ny prisliste
	private void okAction() {
		String navn = txfNavn.getText();
		if (navn.length() == 0) {
			lblError.setText("Navn skal udfyldes");
			return;
		}
		clear();
		Service.createPrisliste(navn);
		lvwPriser.getItems().clear();
		setTabs(tabPane);
		tabPane.getSelectionModel().selectLast();
	}

	// Sæt prisliste tabs i toppen af borderpane
	private void setTabs(TabPane pane) {
		pane.getTabs().clear();
		ArrayList<Prisliste> list = Service.getPrislister();
		if (!list.isEmpty()) {

			for (Prisliste pl : list) {
				String s = pl.getNavn();
				tab = new Tab(s);
				pane.getTabs().add(tab);
			}
		}
	}

	// Opdater combobox med produkttyper, hvis tab ændres.
	private void tabChanged() {
		if (!tabPane.getTabs().isEmpty()) {
			cbTyper.getItems().clear();
			String tab = tabPane.getSelectionModel().getSelectedItem().getText();
			for (Prisliste pl : Service.getPrislister()) {
				if (pl.getNavn().equalsIgnoreCase(tab)) {
					prisliste = pl;
					lblListe.setText(pl.getNavn());
					cbTyper.getItems().setAll(Service.getPrislistTyper(prisliste));
					cbTyper.getSelectionModel().select(0);
				}
			}
		}
	}

	// Gør unødvendinge control utilgængelige ved markering af pris
	private void prisChanged() {
		Pris p = lvwPriser.getSelectionModel().getSelectedItem();
		if (p != null) {
			btnRemoveProduct.setDisable(false);
			btnUpdate.setDisable(false);
		} else {
			btnRemoveProduct.setDisable(true);
			btnUpdate.setDisable(true);
		}
	}

	// Opdater listview med pris afhængig af valgte produkttype
	private void updatePrislist() {
		ProduktType pt = cbTyper.getSelectionModel().getSelectedItem();
		if (pt != null) {
			lvwPriser.getItems().setAll(Service.getPriserAfType(prisliste, pt));
		} else {
			lvwPriser.getItems().clear();

		}
	}
}
