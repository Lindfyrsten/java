package storage;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.TreeSet;

import model.Betalingsform;
import model.Prisliste;
import model.Produkt;
import model.ProduktType;
import model.Rundvisning;
import model.Salg;
import model.Udlejning;

public class Storage {
	private final HashSet<Produkt> produkter;
	private final TreeSet<ProduktType> produktTyper;
	private final ArrayList<Prisliste> prislister;
	private final ArrayList<Salg> salgList;
	private final ArrayList<Betalingsform> betalingsformer;
	private final ArrayList<Rundvisning> rundvisninger;
	private final ArrayList<Udlejning> udlejninger;
	private static Storage storage;

	private Storage() {
		produkter = new HashSet<>();
		produktTyper = new TreeSet<>();
		prislister = new ArrayList<>();
		salgList = new ArrayList<>();
		rundvisninger = new ArrayList<>();
		udlejninger = new ArrayList<>();
		betalingsformer = new ArrayList<>();
	}

	public static Storage getInstance() {
		if (storage == null) {
			storage = new Storage();
		}
		return storage;
	}

	public ArrayList<Salg> getSalg() {
		return new ArrayList<>(salgList);
	}

	public void addSalg(Salg salg) {
		salgList.add(salg);
	}

	public void removeSalg(Salg salg) {
		salgList.remove(salg);
	}

	public HashSet<Produkt> getProdukter() {
		return new HashSet<>(produkter);
	}

	public TreeSet<ProduktType> getProduktTyper() {
		return new TreeSet<>(produktTyper);
	}

	public ArrayList<Prisliste> getPrislister() {
		return new ArrayList<>(prislister);
	}

	public void addProdukt(Produkt produkt) {
		produkter.add(produkt);
	}

	public void addProduktType(ProduktType pt) {
		produktTyper.add(pt);
	}

	public void addPrisliste(Prisliste liste) {
		prislister.add(liste);
	}

	public void removeProdukt(Produkt produkt) {
		produkter.remove(produkt);
	}

	public void removeProduktType(ProduktType pt) {
		produktTyper.remove(pt);
	}

	public void removePrisliste(Prisliste liste) {
		prislister.remove(liste);
	}

	public ArrayList<Betalingsform> getBetalingsformer() {
		return new ArrayList<>(betalingsformer);
	}

	public void addBetalingsform(Betalingsform betalingsform) {
		betalingsformer.add(betalingsform);
	}

	public ArrayList<Rundvisning> getRundvisninger() {
		return new ArrayList<>(rundvisninger);
	}

	public void addRundvisning(Rundvisning r) {
		rundvisninger.add(r);
	}

	public void addUdlejning(Udlejning u) {
		udlejninger.add(u);
	}

	public ArrayList<Udlejning> getUdlejninger() {
		return new ArrayList<>(udlejninger);
	}
}
