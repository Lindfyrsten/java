package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Modellerer en sampakning af andre produkter
 */
public class Sampakning extends Produkt {

	private final List<Produkt> produkter;
	private final int flaskAntal, glasAntal;

	/**
	 * Opretter en ny sampakning Pre: navn.length > 0 Pre: flaskAntal +
	 * glasAntal > 0
	 *
	 * @param navn
	 * @param lagerAntal
	 * @param flaskAntal
	 * @param glasAntal
	 */
	public Sampakning(String navn, int lagerAntal, int flaskAntal, int glasAntal) {
		super(navn, lagerAntal);
		this.flaskAntal = flaskAntal;
		this.glasAntal = glasAntal;
		produkter = new ArrayList<>();
	}

	/**
	 * Tilføjer en liste af produkter til sampakningen
	 *
	 * @param produkter
	 */
	public void addProdukter(List<Produkt> produkter) {
		this.produkter.addAll(produkter);
	}

	/**
	 * Returnerer antallet af produkter der skal i denne sampakning
	 *
	 * @return flaskAntal + glasAntal
	 */
	public int getAntalProdukter() {
		return flaskAntal + glasAntal;
	}

	/**
	 * Returnerer antallet af flasker der skal i sampakningen
	 *
	 * @return flaskAntal
	 */
	public int getFlaskAntal() {
		return flaskAntal;
	}

	/**
	 * Returnerer antallet af glas der skal i sampakningen
	 *
	 * @return glasAntal
	 */
	public int getGlasAntal() {
		return glasAntal;
	}

	/**
	 * Returnerer de produkter der er i sampakningen
	 *
	 * @return
	 */
	public List<Produkt> getProdukter() {
		return produkter;
	}

	/**
	 * Fjerner alle produkter fra sampakningen
	 */
	public void reset() {
		produkter.clear();
	}

}
