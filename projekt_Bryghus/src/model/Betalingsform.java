package model;

/**
 * Klasse til de forskellige betalingsformer til systemet
 */
public class Betalingsform {
	private String navn;
	private boolean usable;

	/**
	 * Initialiserer en ny betalingsform Pre: navn.length > 0
	 *
	 * @param navn
	 */
	public Betalingsform(String navn) {
		this.navn = navn;
		usable = true;
	}

	/**
	 * Returnerer betalingsformens navn
	 *
	 * @return navn
	 */
	public String getNavn() {
		return navn;
	}

	/**
	 * Ændrer betalingsformens navn Pre: navn.length > 0
	 *
	 * @param navn
	 */
	public void setNavn(String navn) {
		this.navn = navn;
	}

	/**
	 * Sætter hvorvidt en betalingsform kan bruges i systemet
	 * 
	 * @param usable
	 */
	public void setUsable(boolean usable) {
		this.usable = usable;
	}

	/**
	 * Returnerer om en betalingsform kan bruges i systemet
	 * 
	 * @return usable
	 */
	public boolean isUsable() {
		return usable;
	}

	@Override
	public String toString() {
		return navn;
	}
}
