package model;

import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Modellerer en produkt type
 */
public class ProduktType implements Comparable<ProduktType> {
	private boolean udlejningsProdukt, salgsProdukt, pakkeProdkukt;
	private String navn;
	private final TreeSet<Produkt> produkter;

	/**
	 * Oprettet en ny produkt type Pre: navn.length > 0
	 *
	 * @param navn
	 * @param udlejningsProdukt
	 * @param salgsProdukt
	 */
	public ProduktType(String navn, boolean udlejningsProdukt, boolean salgsProdukt) {
		this.navn = navn;
		this.udlejningsProdukt = udlejningsProdukt;
		this.salgsProdukt = salgsProdukt;
		produkter = new TreeSet<>();
	}

	/**
	 * Returnerer produkttypens navn
	 *
	 * @return navn
	 */
	public String getNavn() {
		return navn;
	}

	/**
	 * Returnerer om produkter i denne produkttype kan udlejes i systemet
	 *
	 * @return
	 */
	public boolean isUdlejningsProdukt() {
		return udlejningsProdukt;
	}

	/**
	 * Sætter navnet på produkttypen Pre: navn.length > 0
	 *
	 * @param navn
	 */
	public void setNavn(String navn) {
		this.navn = navn;
	}

	/**
	 * Sætter hvorvidt produkter af denne produkttype kan udlejes i systemet
	 *
	 * @param udlejningsProdukt
	 */
	public void setUdlejningsProdukt(boolean udlejningsProdukt) {
		this.udlejningsProdukt = udlejningsProdukt;
	}

	/**
	 * Returnerer alle produkter af denne produkttype
	 *
	 * @return produkter
	 */
	public ArrayList<Produkt> getProdukter() {
		return new ArrayList<>(produkter);
	}

	/**
	 * Opretter et nyt produkt, tilføjer den til denne produkttype og gør
	 * produktets produkttype til denne type Pre: navn.length > 0
	 *
	 * @param navn
	 * @param lagerAntal
	 * @return
	 */
	public Produkt createProdukt(String navn, int lagerAntal) {
		Produkt produkt = new Produkt(navn, lagerAntal);
		produkter.add(produkt);
		produkt.setProduktType(this);
		return produkt;
	}

	/**
	 * Fjerner et produkt fra produkttypen
	 *
	 * @param produkt
	 */
	public void deleteProdukt(Produkt produkt) {
		produkter.remove(produkt);
	}

	/**
	 * Opretter et nyt produkt af typen PantProdkukt, tilføjer den til denne
	 * produkttype og gør produktets produkttype til denne type Pre: navn.length
	 * > 0 Pre: pantPris > 0
	 *
	 * @param navn
	 * @param lagerAntal
	 * @param pantPris
	 * @return
	 */
	public PantProdukt createPantProdukt(String navn, int lagerAntal, double pantPris) {
		PantProdukt pp = new PantProdukt(navn, lagerAntal, pantPris);
		produkter.add(pp);
		pp.setProduktType(this);
		return pp;
	}

	/**
	 * Opretter et nyt produkt af typen Sampakning, tilføjer den til denne
	 * produkttype og gør produktets produkttype til denne type Pre: navn.length
	 * > 0 Pre: flaskAntal >= 0 Pre: glasAntal >= 0
	 *
	 * @param navn
	 * @param lagerAntal
	 * @param flaskAntal
	 * @param glasAntal
	 * @return
	 */
	public Sampakning createSampakning(String navn, int lagerAntal, int flaskAntal, int glasAntal) {
		Sampakning sp = new Sampakning(navn, lagerAntal, flaskAntal, glasAntal);
		produkter.add(sp);
		sp.setProduktType(this);
		return sp;
	}

	/**
	 * Returnerer hvorvidt produkter af denne produkttype kan sælges i systemet
	 *
	 * @return salgsProdukt
	 */
	public boolean isSalgsProdukt() {
		return salgsProdukt;
	}

	/**
	 * Sætter hvorvidt produkter af denne produkttype kan sælges i systemet
	 *
	 * @param salgsProdukt
	 */
	public void setSalgsProdukt(boolean salgsProdukt) {
		this.salgsProdukt = salgsProdukt;
	}

	/**
	 * Returnerer hvorvidt produkter af denne produkttype kan bruges i
	 * sampakninger
	 *
	 * @return pakkeProdukt
	 */
	public boolean isPakkeProdukt() {
		return pakkeProdkukt;
	}

	/**
	 * Sætter hvorvidt produkter af denne produkttype kan bruges i sampakninger
	 *
	 * @param pakkeProdukt
	 */
	public void setPakkeProdkukt(boolean pakkeProdukt) {
		pakkeProdkukt = pakkeProdukt;
	}

	@Override
	public String toString() {
		return navn;
	}

	@Override
	public int compareTo(ProduktType o) {
		return navn.compareTo(o.getNavn());
	}
}
