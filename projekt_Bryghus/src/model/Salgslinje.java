package model;

/**
 * Modellerer en salgslinje
 */
public class Salgslinje {
	private final int antal;
	private Pris prisObjekt;
	private final Rabat rabat;

	/**
	 * Opretter en ny salgslinje
	 *
	 * @param pris
	 * @param antal
	 * @param rabat
	 */
	public Salgslinje(Pris pris, int antal, Rabat rabat) {
		this.antal = antal;
		prisObjekt = pris;
		this.rabat = rabat;
	}

	/**
	 * Returnerer antallet på denne salgslinje
	 *
	 * @return
	 */
	public int getAntal() {
		return antal;
	}

	/**
	 * Returnerer det enkelte produkts pris på prislisten som er tilknyttet
	 *
	 * @return
	 */
	public double getProduktPris() {
		return prisObjekt.getPris();
	}

	public void setPrisObjekt(Pris prisObjekt) {
		this.prisObjekt = prisObjekt;
	}

	/**
	 * Returnerer selve produktet som er tilknyttet prisen
	 *
	 * @return
	 */
	public Produkt getProdukt() {
		return prisObjekt.getProdukt();
	}

	/**
	 * Returnerer pris afhængig af valgte rabat og antal
	 *
	 * @return
	 */
	public double getPris() {
		return rabat.beregnPris(prisObjekt.getPris() * antal);
	}

	/**
	 * Returnerer rabat typen
	 *
	 * @return
	 */
	public Rabat getRabat() {
		return rabat;
	}

	@Override
	public String toString() {
		String pris = antal + "x " + getProdukt().getNavn() + ": " + getPris() + " kr";
		if (getProdukt() instanceof Sampakning) {
			pris = pris + " (Produkter: ";
			for (Produkt p : ((Sampakning) getProdukt()).getProdukter()) {
				pris = pris + p.getNavn() + ", ";
			}
			pris = pris.substring(0, pris.length() - 2);
			pris = pris + " )";
		}
		if (getProdukt() instanceof PantProdukt) {
			pris = pris + " (pant: " + ((PantProdukt) getProdukt()).getPantPris() * antal + ")";
		}
		if (rabat instanceof PrisRabat) {
			pris = pris + " ( Rabat: " + rabat.getRabat() + " kr.)";
		} else if (rabat instanceof ProcentRabat) {
			pris = pris + " ( Rabat: " + rabat.getRabat() + "%)";
		}
		return pris;
	}

}
