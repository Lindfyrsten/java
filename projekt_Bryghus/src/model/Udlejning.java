package model;

import java.time.LocalDate;

/**
 * Modellerer en udlejning i systemet
 */
public class Udlejning extends Salg {
	private LocalDate udlejningsDato, afleveringsDato;
	private int kontaktNr;
	private String navn;
	private String cprNr;
	private boolean afleveret;
	private Betalingsform afregningsBetaling;

	/**
	 * Oprettet en ny udlejning i systemet Pre: navn.length > 0 Pre:
	 * cprNr.length == 10 Pre: udlejningsDato < afleveringsDato
	 * 
	 * @param navn
	 * @param cprNr
	 * @param kontaktNr
	 * @param udlejningsDato
	 * @param afleveringsDato
	 */
	public Udlejning(String navn, String cprNr, int kontaktNr, LocalDate udlejningsDato, LocalDate afleveringsDato) {
		super();
		this.navn = navn;
		this.cprNr = cprNr;
		this.kontaktNr = kontaktNr;
		this.udlejningsDato = udlejningsDato;
		this.afleveringsDato = afleveringsDato;
		setAfleveret(false);
	}

	/**
	 * Returnerer om udlejningen er afleveret
	 * 
	 * @return afleveret
	 */
	public boolean isAfleveret() {
		return afleveret;
	}

	/**
	 * Ændrer tilstanden på om udlejningen er afleveret
	 * 
	 * @param afleveret
	 */
	public void setAfleveret(boolean afleveret) {
		this.afleveret = afleveret;
	}

	/**
	 * Returnerer navnet på personen der har lejet produkterne
	 * 
	 * @return navn
	 */
	public String getNavn() {
		return navn;
	}

	/**
	 * Sætter navnet forbundet med udlejningen til et andet navn Pre:
	 * navn.length > 0
	 * 
	 * @param navn
	 */
	public void setNavn(String navn) {
		this.navn = navn;
	}

	/**
	 * Returnerer cpr-nummeret på personen der har lejet produkterne
	 * 
	 * @return
	 */
	public String getCprNr() {
		return cprNr;
	}

	/**
	 * Sætter cpr-nummeret forbundet med udlejningen til noget andet Pre:
	 * cprNr.length == 10
	 * 
	 * @param cprNr
	 */
	public void setCprNr(String cprNr) {
		this.cprNr = cprNr;
	}

	/**
	 * Returnerer kontakt-nummeret på personen der har lejet produkterne
	 * 
	 * @return
	 */
	public int getKontaktNr() {
		return kontaktNr;
	}

	/**
	 * Sætter et nyt kontakt nummer
	 * 
	 * @param kontaktNr
	 */
	public void setKontaktNr(int kontaktNr) {
		this.kontaktNr = kontaktNr;
	}

	/**
	 * Returnerer datoen for hvornår udlejningen starter
	 * 
	 * @return
	 */
	public LocalDate getUdlejningsDato() {
		return udlejningsDato;
	}

	/**
	 * Sætter en ny startdato for udlejningen
	 * 
	 * @param udlejningsDato
	 */
	public void setUdlejningsDato(LocalDate udlejningsDato) {
		this.udlejningsDato = udlejningsDato;
	}

	/**
	 * Returnerer datoen for hvornår udlejningen skal afleveres
	 * 
	 * @return
	 */
	public LocalDate getAfleveringsDato() {
		return afleveringsDato;
	}

	/**
	 * Ændrer afleveringsdatoen
	 * 
	 * @param afleveringsDato
	 */
	public void setAfleveringsDato(LocalDate afleveringsDato) {
		this.afleveringsDato = afleveringsDato;
	}

	/**
	 * Returnerer hvilken betalingsform blev brugt til afregningen af
	 * udlejningen
	 * 
	 * @return
	 */
	public Betalingsform getAfregningsBetaling() {
		return afregningsBetaling;
	}

	/**
	 * Sætter hvilken betalingsform blev brugt til afregningen af udlejningen
	 * 
	 * @param afregningsBetaling
	 */
	public void setAfregningsBetaling(Betalingsform afregningsBetaling) {
		this.afregningsBetaling = afregningsBetaling;
	}

	@Override
	public String toString() {
		return navn + " " + udlejningsDato + " til " + afleveringsDato;
	}
}
