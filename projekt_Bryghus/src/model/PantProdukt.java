package model;

/**
 * Klasse til produkter hvor der skal medregnes pant
 */
public class PantProdukt extends Produkt {
	private double pantPris;

	/**
	 * Initialiserer et nyt pant produkt Pre: navn.length > 0, pantPris > 0
	 * 
	 * @param navn
	 * @param lagerAntal
	 * @param pantPris
	 */
	public PantProdukt(String navn, int lagerAntal, double pantPris) {
		super(navn, lagerAntal);
		this.pantPris = pantPris;
	}

	/**
	 * Returnerer pant prisen
	 * 
	 * @return pantPris
	 */
	public double getPantPris() {
		return pantPris;
	}

	/**
	 * Ændrer pant prisen Pre: pantPris > 0
	 * 
	 * @param pantPris
	 */
	public void setPantPris(double pantPris) {
		this.pantPris = pantPris;
	}

	@Override
	public String toString() {
		return super.toString() + " - pant: " + pantPris;
	}

}
