package model;

/**
 * Klasse til at modellere en pris for et produkt
 */
public class Pris implements Comparable<Pris> {
	private Produkt produkt;
	private double pris;

	/**
	 * Laver en ny pris Pre: produkt =! null Pre: pris >= 0
	 * 
	 * @param produkt
	 * @param pris
	 */
	public Pris(Produkt produkt, double pris) {
		this.produkt = produkt;
		this.pris = pris;
	}

	/**
	 * Returnerer talværdien for prisen
	 * 
	 * @return pris
	 */
	public double getPris() {
		return pris;
	}

	/**
	 * Returnerer det produkt, som prisen er lavet til
	 * 
	 * @return produkt
	 */
	public Produkt getProdukt() {
		return produkt;
	}

	public void setProdukt(Produkt produkt) {
		this.produkt = produkt;
	}

	/**
	 * Sætter talværdien for prisen Pre: pris >= 0
	 * 
	 * @param pris
	 */
	public void setPris(double pris) {
		this.pris = pris;
	}

	@Override
	public String toString() {
		if (produkt instanceof PantProdukt) {
			return produkt.getNavn() + " -  " + pris + " kr." + " (pant: " + ((PantProdukt) produkt).getPantPris()
					+ ")";
		}
		return produkt.getNavn() + "  -   " + pris + " kr.";
	}

	@Override
	public int compareTo(Pris o) {
		if (produkt.compareTo(o.getProdukt()) == 0) {
			return (int) (pris - o.getPris());
		} else {
			return produkt.compareTo(o.getProdukt());
		}
	}
}
