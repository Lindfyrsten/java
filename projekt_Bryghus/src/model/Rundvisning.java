package model;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Modellerer et salg af en rundvisning
 */
public class Rundvisning implements Comparable<Rundvisning> {
	private int antalPersoner, kontaktNr;
	private LocalDateTime startTid, slutTid;
	private double prisPrPerson;
	private LocalDate dato;
	private Betalingsform betalingsform;

	/**
	 * Opretter en ny rundvisning Pre: antalPersoner > 0 Pre: prisPrPerson > 0
	 * Pre: startTid < slutTid
	 * 
	 * @param antalPersoner
	 * @param prisPrPerson
	 * @param startTid
	 * @param slutTid
	 * @param kontaktNr
	 */
	public Rundvisning(int antalPersoner, double prisPrPerson, LocalDateTime startTid, LocalDateTime slutTid,
			int kontaktNr) {
		this.antalPersoner = antalPersoner;
		this.prisPrPerson = prisPrPerson;
		this.startTid = startTid;
		this.slutTid = slutTid;
		this.kontaktNr = kontaktNr;
		this.dato = LocalDate.now();
	}

	/**
	 * Returnerer antal af personer på denne rundvisning
	 * 
	 * @return antalPersoner
	 */
	public int getAntalPersoner() {
		return antalPersoner;
	}

	/**
	 * Sætter antal af personer til denne rundvisning
	 * 
	 * @param antalPersoner
	 */
	public void setAntalPersoner(int antalPersoner) {
		this.antalPersoner = antalPersoner;
	}

	/**
	 * Returnerer prisen pr person
	 * 
	 * @return prisPrPerson
	 */
	public double getPrisPrPerson() {
		return prisPrPerson;
	}

	/**
	 * Sætter priser pr person Pre: prisPrPerson > 0
	 * 
	 * @param prisPrPerson
	 */
	public void setPrisPrPerson(double prisPrPerson) {
		this.prisPrPerson = prisPrPerson;
	}

	/**
	 * Returnerer hvornår rundvisningen starter
	 * 
	 * @return startTid
	 */
	public LocalDateTime getStartTid() {
		return startTid;
	}

	/**
	 * Ændrer starttiden for rundvisningen
	 * 
	 * @param startTid
	 */
	public void setStartTid(LocalDateTime startTid) {
		this.startTid = startTid;
	}

	/**
	 * Reutnrerer hvornår rundvisningen slutter
	 * 
	 * @return slutTid
	 */
	public LocalDateTime getSlutTid() {
		return slutTid;
	}

	/**
	 * Ændrer sluttiden for rundvisningen
	 * 
	 * @param slutTid
	 */
	public void setSlutTid(LocalDateTime slutTid) {
		this.slutTid = slutTid;
	}

	/**
	 * Returnerer kontakt nummeret på rundvisningen
	 * 
	 * @return kontaktNr
	 */
	public int getKontaktNr() {
		return kontaktNr;
	}

	/**
	 * Sætter nyt kontakt nummer på rundvisningen
	 * 
	 * @param kontaktNr
	 */
	public void setKontaktNr(int kontaktNr) {
		this.kontaktNr = kontaktNr;
	}

	/**
	 * Beregner den samlede pris for rundvisningen
	 * 
	 * @return den samlede pris for rundvisningen
	 */
	public double getSamletPris() {
		return prisPrPerson * antalPersoner;
	}

	/**
	 * Returnerer den betalingsform der blev brugt til at betale for
	 * rundvisningen
	 * 
	 * @return
	 */
	public Betalingsform getBetalingsform() {
		return betalingsform;
	}

	/**
	 * Sætter betalingsformen der bruges til at betale for rundvisningen
	 * 
	 * @param betalingsform
	 */
	public void setBetalingsform(Betalingsform betalingsform) {
		this.betalingsform = betalingsform;
	}

	/**
	 * Returnerer datoen for hvornår rundvisningen blev solgt
	 * 
	 * @return dato
	 */
	public LocalDate getDato() {
		return dato;
	}

	@Override
	public String toString() {
		if (startTid.isBefore(LocalDateTime.now()) && betalingsform == null) {
			return "(IKKE BETALT) Tlf: " + kontaktNr + " Personer: " + antalPersoner;
		}
		return startTid.toLocalDate() + " fra " + startTid.toLocalTime() + " til " + slutTid.toLocalTime() + " ( "
				+ antalPersoner + " personer )";
	}

	@Override
	public int compareTo(Rundvisning o) {
		return startTid.compareTo(o.getStartTid());
	}

}
