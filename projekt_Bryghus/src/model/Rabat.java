package model;

public interface Rabat {
	/**
	 * Returnerer prisen efter rabatten er trukket fra
	 * 
	 * @param pris
	 * @return
	 */
	double beregnPris(double pris);

	/**
	 * Returnerer rabattens størrelse
	 * 
	 * @return
	 */
	double getRabat();
}
