package model;

/**
 * Modellerer en flad rabat for en pris (fast beløb)
 */
public class PrisRabat implements Rabat {

	private double pris;

	/**
	 * Opretter en ny PrisRabat
	 * Pre: pris >= 0
	 * @param pris
	 */
	public PrisRabat(double pris) {
		this.pris = pris;
	}

	@Override
	public double beregnPris(double pris) {
		return pris - this.pris;
	}

	@Override
	public double getRabat() {
		return pris;
	}

}
