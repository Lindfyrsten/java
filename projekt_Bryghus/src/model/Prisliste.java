package model;

import java.util.TreeSet;

/**
 * Klasse som modellerer en prisliste, så produkter kan have forskellige priser
 * alt efter arrangement
 */
public class Prisliste {
	private String navn;
	private final TreeSet<Pris> priser = new TreeSet<>();

	/**
	 * Laver en ny prisliste
	 * Pre: navn.length > 0
	 * @param navn
	 */
	public Prisliste(String navn) {
		this.navn = navn;
	}

	/**
	 * Returnerer navnet på prislisten
	 * @return navn
	 */
	public String getNavn() {
		return navn;
	}

	/**
	 * Sætter navnet på prislisten
	 * Pre: navn.length > 0
	 * @param navn
	 */
	public void setNavn(String navn) {
		this.navn = navn;
	}

	/**
	 * Returnerer alle priser i prislisten
	 * @return priser
	 */
	public TreeSet<Pris> getPriser() {
		return new TreeSet<>(priser);
	}

	/**
	 * Tilføjer en pris for et produkt til prislisten
	 * Pre: produkt != null
	 * Pre: pris >= 0
	 * @param produkt
	 * @param pris
	 * @return nyoprettet Pris-objekt
	 */
	public Pris addPris(Produkt produkt, double pris) {
		Pris p = new Pris(produkt, pris);
		priser.add(p);
		return p;
	}

	/**
	 * Fjerner en Pris fra prislisten
	 * Pre: pris != null
	 * @param pris
	 */
	public void removePris(Pris pris) {
		priser.remove(pris);
	}
}
