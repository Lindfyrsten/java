package model;

/**
 * Klasse til salgslinjer, som ikke skal have rabat
 */
public class IngenRabat implements Rabat {

	public IngenRabat() {

	}

	/**
	 * Returnerer den originale pris
	 */
	@Override
	public double beregnPris(double pris) {
		return pris;
	}

	/**
	 * Returnerer 0
	 */
	@Override
	public double getRabat() {
		return 0;
	}

}
