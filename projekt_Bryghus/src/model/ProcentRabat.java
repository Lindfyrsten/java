package model;

/**
 * Modellerer en procent rabat
 */
public class ProcentRabat implements Rabat {
    
    private double procent;
    
    /**
     * Opretter en ny Procent Rabat
     * Pre: procent >= 0
     * @param procent
     */
    public ProcentRabat(double procent) {
        this.procent = procent;
    }
    
    @Override
    public double beregnPris(double pris) {
        return pris - (procent / 100 * pris);
    }
    
    @Override
    public double getRabat() {
        return procent;
    }
    
}
