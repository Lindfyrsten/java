package model;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Modellerer et salg i systemet
 */
public class Salg {
	private LocalDate dato;
	private Betalingsform betalingsform;
	private final ArrayList<Salgslinje> salgslinjer;

	/**
	 * Opretter et nyt salg
	 */
	public Salg() {
		betalingsform = null;
		salgslinjer = new ArrayList<>();
	}

	/**
	 * Tilføjer en salgslinje til salget
	 *
	 * @param s
	 */
	public void addSalgslinje(Salgslinje s) {
		salgslinjer.add(s);
	}

	/**
	 * Fjerner en salgslinje fra salget
	 *
	 * @param s
	 */
	public void removeSalgslinje(Salgslinje s) {
		salgslinjer.remove(s);
	}

	/**
	 * Returnerer datoen for salget stars
	 *
	 * @return dato
	 */
	public LocalDate getDato() {
		return dato;
	}

	public void setDato(LocalDate dato) {
		this.dato = dato;
	}

	/**
	 * Returnerer salgslinjer på salget
	 *
	 * @return salgslinjer
	 */
	public ArrayList<Salgslinje> getSalgslinjer() {
		return new ArrayList<>(salgslinjer);
	}

	/**
	 * Returnerer betalingsformen brugt til at betale for salget
	 *
	 * @return
	 */
	public Betalingsform getBetalingsform() {
		return betalingsform;
	}

	/**
	 * Sætter betalingsformen der bruges til at betalge for salget
	 *
	 * @param betalingsform
	 */
	public void setBetalingsform(Betalingsform betalingsform) {
		this.betalingsform = betalingsform;
	}

	/**
	 * Returnerer samlede pris for salgslinjerne, uden pant.
	 *
	 * @return
	 */
	public double udregnSamletPris() {
		double pris = 0;
		for (Salgslinje s : salgslinjer) {
			pris += s.getPris();
		}
		return pris;
	}

	/**
	 * Returnerer samlede pantpris, for pantprodukterne
	 *
	 * @return
	 */
	public double udregnSamletPant() {
		double pris = 0;
		for (Salgslinje s : salgslinjer) {
			if (s.getProdukt() instanceof PantProdukt) {
				pris += ((PantProdukt) s.getProdukt()).getPantPris() * s.getAntal();
			}
		}
		return pris;
	}

	/**
	 * Opretter og tilføjer en salgslinje
	 *
	 * @param pris
	 * @param antal
	 * @param rabat
	 * @return
	 */
	public Salgslinje createSalgslinje(Pris pris, int antal, Rabat rabat) {
		Salgslinje s = new Salgslinje(pris, antal, rabat);
		double prisCheck = s.getPris();
		if (prisCheck < 0) {
			throw new RuntimeException("Ugyldig rabat");
		}
		addSalgslinje(s);
		return s;
	}

	@Override
	public String toString() {
		return betalingsform.getNavn() + " (" + udregnSamletPris() + " kr)";
	}
}
