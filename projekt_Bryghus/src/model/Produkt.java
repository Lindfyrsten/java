package model;

/**
 * Modellerer et produkt i systemet
 */
public class Produkt implements Comparable<Produkt> {
	private String navn;
	private int lagerAntal;
	private ProduktType produktType;

	/**
	 * Oprettet et nyt produkt Pre: navn.length > 0 Pre: lagerAntal >= 0
	 *
	 * @param navn
	 * @param lagerAntal
	 */
	public Produkt(String navn, int lagerAntal) {
		this.navn = navn;
		this.lagerAntal = lagerAntal;
	}

	/**
	 * Returnerer navnet på produktet
	 *
	 * @return navn
	 */
	public String getNavn() {
		return navn;
	}

	/**
	 * Sætter navnet på produktet Pre: navn.length > 0
	 *
	 * @param navn
	 */
	public void setNavn(String navn) {
		this.navn = navn;
	}

	/**
	 * Returnerer lagerantallet for produktet
	 *
	 * @return lagerAntal
	 */
	public int getLagerAntal() {
		return lagerAntal;
	}

	/**
	 * Sætter lagerantallet til et nyt antal Pre: lagerAntal >= 0
	 *
	 * @param lagerAntal
	 */
	public void setLagerAntal(int lagerAntal) {
		this.lagerAntal = lagerAntal;
	}

	/**
	 * Returnerer produktets ProduktType
	 *
	 * @return produktType
	 */
	public ProduktType getProduktType() {
		return produktType;
	}

	/**
	 * Sætter produktets ProduktType Pre: pt != null
	 *
	 * @param pt
	 */
	public void setProduktType(ProduktType pt) {
		produktType = pt;
	}

	@Override
	public String toString() {
		return navn;
	}

	@Override
	public int compareTo(Produkt o) {
		return navn.compareTo(o.getNavn());
	}
}
