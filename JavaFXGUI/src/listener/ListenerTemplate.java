package listener;

import javafx.beans.value.ChangeListener;
import javafx.scene.control.ListView;

public class ListenerTemplate {
    ListView<String> lvwNames = new ListView<>();
    
    ChangeListener<String> listener =
        (ov, oldValue, newValue) -> selectionChanged();

//        lvwString.getSelectionModel().selectedItemProperty().addListener(listener);
    private void selectionChanged() {
        // action when listener changed
        String newString = lvwNames.getSelectionModel().getSelectedItem();
        if (newString != null) {
//            txfName.setText(newString);
        }
        else {
//            txfName.clear();
        }
    }
}
