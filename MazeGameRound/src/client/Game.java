package client;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.util.Duration;
public class Game extends GridPane {

	private Image image_ammo, image_dead;
	private Image image_bomb, bombCenter, bombVertical, bombHorizontal, bombVerticalUp, bombVerticalDown,
			bombHorizontalLeft, bombHorizontalRight, bombUpgrade;
	private Image image_wall3, image_wall4, image_wall5;
	private Image hero_right, hero_left, hero_up, hero_down, hero_rightRed, hero_leftRed, hero_upRed, hero_downRed;
	private Image fireDown, fireHorizontal, fireLeft, fireRight, fireUp, fireVertical, fireWallEast, fireWallNorth,
			fireWallSouth, fireWallWest;
	public static Image imgMusicOn, imgMusicOff;
	private ImageView bomb;
	private ArrayList<Image> walls = new ArrayList<>();
	public String name;
	private Label[][] fields;
	private TextArea scoreList;
	private Text scoreLabel, ammoLabel, instructionsLabel, lblWaiting;
	public boolean shooting = false;
	public boolean bombing = false;
	public boolean dead = true;
	private MediaPlayer mp;
	private Media soundBombCharge, soundExplosion, soundShoot, soundScream, soundReload;
	private int size;
	public static Button btnMusic;
	public static StackPane board;

	public Game(String name) {
		this.name = name;
		size = Client.size;
		setHgap(10);
		setVgap(10);
		setPadding(new Insets(40, 10, 10, 10));
		loadImages();
		initContent();
	}

	private void initContent() {
		lblWaiting = new Text();
		lblWaiting.setText("Waiting for other players to join");
		lblWaiting.setFont(Font.font("IMPACT", FontWeight.BOLD, 36));
		lblWaiting.setFill(Color.DARKRED);
		scoreLabel = new Text();
		scoreLabel.setVisible(false);
		scoreLabel.setFont(Font.font("IMPACT", FontWeight.BOLD, 36));
		scoreLabel.setFill(Color.DARKRED);
		ammoLabel = new Text();
		ammoLabel.setVisible(false);
		ammoLabel.setFont(Font.font("IMPACT", FontWeight.BOLD, 24));
		ammoLabel.setFill(Color.DARKRED);
		instructionsLabel = new Text("\tSPACE: Shoot\t\t\t\tB: Bomb\t\t\tEscape: Quit");
		instructionsLabel.setVisible(false);
		instructionsLabel.setFont(Font.font("IMPACT", FontWeight.BOLD, 20));
		instructionsLabel.setFill(Color.DARKRED);
		scoreList = new TextArea();
		scoreList.setFont(Font.font("IMPACT", FontWeight.BOLD, 20));
		scoreList.setEditable(false);
		scoreList.setMaxHeight(200);
		scoreList.setVisible(false);
		board = new StackPane();
		btnMusic = new Button();
		btnMusic.setOnMouseClicked(e -> toggleMusic());
		GridPane boardGrid = new GridPane();
		// adding our different wall images to the walls list, so we can choose random wall image for each wall later
		Collections.addAll(walls, image_wall3, image_wall4, image_wall5);
		BackgroundImage tableImage = new BackgroundImage(
				new Image(getClass().getResourceAsStream("Image/table.jpg/"), Client.scene_width, Client.scene_height,
						false, true),
				BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
				BackgroundSize.DEFAULT);
		Background table = new Background(tableImage);
		setBackground(table);
		// creating a multidemensional array to act as our "board" that is 20x20 grids
		fields = new Label[20][20];
		for (int j = 0; j < 20; j++) {
			for (int i = 0; i < 20; i++) {
				// creating and inserting a blank label into each grid
				fields[i][j] = new Label("", null);
				boardGrid.add(fields[i][j], i, j);
			}
		}
		board.getChildren().add(boardGrid);
		add(lblWaiting, 0, 0);
		add(board, 0, 1, 1, 3);
		add(scoreLabel, 1, 1);
		add(scoreList, 1, 2);
		add(ammoLabel, 1, 3);
		add(btnMusic, 1, 4);
		add(instructionsLabel, 0, 4);
		GridPane.setHalignment(scoreLabel, HPos.CENTER);
		GridPane.setValignment(scoreList, VPos.TOP);
		GridPane.setValignment(ammoLabel, VPos.TOP);
		GridPane.setHalignment(ammoLabel, HPos.CENTER);
		GridPane.setHalignment(lblWaiting, HPos.CENTER);
		GridPane.setValignment(lblWaiting, VPos.TOP);
		GridPane.setHalignment(btnMusic, HPos.RIGHT);
	}

	/**
	 * Create a wall in the grid
	 *
	 * @param x The x coordinate
	 * @param y The y coordinate
	 */
	public void createWalls(String[] list) throws Exception {
		// choose a random wall image from walls list
		for (int i = 1; i < list.length; i++) {
			int x = Integer.parseInt(list[i]);
			int y = Integer.parseInt(list[i + 1]);
			int random = (int) (Math.random() * walls.size());
			ImageView img = new ImageView(walls.get(random));
			img.setOpacity(.9);
			// change the graphics of the label located in x,y to the chosen image
			fields[x][y].setGraphic(img);
			i++;
		}
	}

	/**
	 * Remove a player from the game
	 *
	 * @param x The x coordinate
	 * @param y The y coordinate
	 */
	public void removePlayer(int x, int y) throws Exception {
		// player removed, so we replace his image with null
		fields[x][y].setGraphic(null);
	}

	/**
	 * Spawn a player on the map
	 *
	 * @param x The x coordinate
	 * @param y The y coordinate
	 * @param playerName The name of the player
	 */
	public void playerSpawn(int x, int y, String playerName, int ammo, int players) {
		lblWaiting.setText("Waiting for round to finish");
		// check if player is you or opponent and choose the correct image
		if (name.equals(playerName)) {
			getChildren().remove(lblWaiting);
			fields[x][y].setGraphic(new ImageView(hero_up));
			// show GUI elements
			scoreList.setVisible(true);
			scoreLabel.setVisible(true);
			ammoLabel.setVisible(true);
			instructionsLabel.setVisible(true);
			ammoLabel.setText("Ammo: " + ammo);
			scoreLabel.setText("Scores");
		} else {
			fields[x][y].setGraphic(new ImageView(hero_upRed));
		}
	}

	/**
	 * Player has moved
	 *
	 * @param fromX The x position player moved from
	 * @param fromY The y position player moved from
	 * @param toX The x position player moved to
	 * @param toY The y position player moved to
	 * @param direction The direction of the move
	 * @param playerName The name of the player that moved
	 */
	public void playerMove(int fromX, int fromY, int toX, int toY, Direction direction, String playerName) {
		// change the graphics of the label located in x,y
		// player removed, so we replace his image with null
		if (fields[fromX][fromY].getGraphic() != bomb) {
			fields[fromX][fromY].setGraphic(null);
		}
		// change the graphics of the label depending on direction and wether the player is you or an opponent
		if (direction == Direction.RIGHT) {
			if (playerName.equals(name)) {
				fields[toX][toY].setGraphic(new ImageView(hero_right));
			} else {
				fields[toX][toY].setGraphic(new ImageView(hero_rightRed));
			}
		}
		if (direction == Direction.LEFT) {
			if (playerName.equals(name)) {
				fields[toX][toY].setGraphic(new ImageView(hero_left));
			} else {
				fields[toX][toY].setGraphic(new ImageView(hero_leftRed));
			}
		}
		if (direction == Direction.UP) {
			if (playerName.equals(name)) {
				fields[toX][toY].setGraphic(new ImageView(hero_up));
			} else {
				fields[toX][toY].setGraphic(new ImageView(hero_upRed));
			}
		}
		if (direction == Direction.DOWN) {
			if (playerName.equals(name)) {
				fields[toX][toY].setGraphic(new ImageView(hero_down));
			} else {
				fields[toX][toY].setGraphic(new ImageView(hero_downRed));
			}
		}
	}

	/**
	 * Shoots with a player
	 *
	 * @param fromX The first x coordinate in the path
	 * @param fromY The first y coordinate in the path
	 * @param toX The last x coordinate in the path
	 * @param toY The last y coordinate in the path
	 * @param direction Direction of the shot
	 * @param playerName The name of the shooter
	 * @param shotX The x coordinate of the player shot : -1 if none.
	 * @param shotY The y coordinate of the player shot : -1 if none.
	 */
	public void playerShoot(int fromX, int fromY, int toX, int toY, Direction direction, String playerName, int ammo) {
		MediaPlayer mediaShoot = new MediaPlayer(soundShoot);
		mediaShoot.setVolume(.5);
		mediaShoot.play();
		// if we are the shooter
		if (playerName.equals(name)) {
			// stop mediaPlayer first to play it from start again, in case it's already running
			shooting = true;
			ammoLabel.setText("Ammo: " + ammo);
		}
		// set x,y to the path start coordinates
		int x = fromX;
		int y = fromY;
		Image wall;
		Image fire;
		Image firePath;
		int delta_x, delta_y;
		// sets the image and delta_x/y depending on the direction of the shot
		if (direction == Direction.UP) {
			wall = fireWallNorth;
			fire = fireUp;
			firePath = fireVertical;
			delta_x = 0;
			delta_y = -1;
		} else if (direction == Direction.DOWN) {
			wall = fireWallSouth;
			fire = fireDown;
			firePath = fireVertical;
			delta_x = 0;
			delta_y = 1;
		} else if (direction == Direction.LEFT) {
			wall = fireWallWest;
			fire = fireLeft;
			firePath = fireHorizontal;
			delta_x = -1;
			delta_y = 0;
		} else {
			wall = fireWallEast;
			fire = fireRight;
			firePath = fireHorizontal;
			delta_x = 1;
			delta_y = 0;
		}
		// keep going until we meet the end coordinates
		while (x != toX || y != toY) {
			// if x,y is the first coordinate on the path we choose the fire image
			if (x == fromX && y == fromY) {
				fields[x][y].setGraphic(new ImageView(fire));
			}
			// otherwise we choose the path image
			else {
				fields[x][y].setGraphic(new ImageView(firePath));
			}
			x += delta_x;
			y += delta_y;
		}
		// loop ends before last coordinate of the path and x,y is now the coordinates of the last grid before a wall
		// so we set the wall image
		fields[x][y].setGraphic(new ImageView(wall));
		// sets up a timeline with a duration, so the shot remains visible for a certain time and disables us from
		// shooting again until that time is up
		Timeline shoot = new Timeline(new KeyFrame(Duration.seconds(.3), e -> {
			int xx = toX;
			int yy = toY;
			// remove the label graphics we added earlier
			fields[xx][yy].setGraphic(null);
			// keep removing until we meet out path x,y start
			while (xx != fromX || yy != fromY) {
				xx -= delta_x;
				yy -= delta_y;
				fields[xx][yy].setGraphic(null);
			}
		}));
		// play the timeline
		shoot.play();
		// when finished, we want to be able to shoot again
		shoot.setOnFinished(e -> {
			if (playerName.equals(name)) {
				shooting = false;
			}
		});
	}

	public void playerBomb(int centerX, int centerY, int up, int down, int left, int right, String playerName) {
		if (playerName.equals(name)) {
			bombing = true;
		}
		MediaPlayer charge = new MediaPlayer(soundBombCharge);
		charge.setVolume(.5);
		MediaPlayer explode = new MediaPlayer(soundExplosion);
		explode.setVolume(.5);
		charge.play();
		bomb = new ImageView(image_bomb);
		fields[centerX][centerY].setGraphic(bomb);
		Timeline bomb = new Timeline(new KeyFrame(Duration.seconds(2.8), e -> {
			fields[centerX][centerY].setGraphic(new ImageView(bombCenter));
			int delta_x, delta_y;
			int x, y;
			int count;
			// up
			count = up;
			delta_x = 0;
			delta_y = -1;
			x = centerX;
			y = centerY;
			while (count > 0) {
				x += delta_x;
				y += delta_y;
				if (count == 1) {
					fields[x][y].setGraphic(new ImageView(bombVerticalUp));
				} else {
					fields[x][y].setGraphic(new ImageView(bombVertical));
				}
				count--;
			}
			// down
			count = down;
			delta_x = 0;
			delta_y = 1;
			x = centerX;
			y = centerY;
			while (count > 0) {
				x += delta_x;
				y += delta_y;
				if (count == 1) {
					fields[x][y].setGraphic(new ImageView(bombVerticalDown));
				} else {
					fields[x][y].setGraphic(new ImageView(bombVertical));
				}
				count--;
			}
			// left
			count = left;
			delta_x = -1;
			delta_y = 0;
			x = centerX;
			y = centerY;
			while (count > 0) {
				x += delta_x;
				y += delta_y;
				if (count == 1) {
					fields[x][y].setGraphic(new ImageView(bombHorizontalLeft));
				} else {
					fields[x][y].setGraphic(new ImageView(bombHorizontal));
				}
				count--;
			}
			// right
			count = right;
			delta_x = 1;
			delta_y = 0;
			x = centerX;
			y = centerY;
			while (count > 0) {
				x += delta_x;
				y += delta_y;
				if (count == 1) {
					fields[x][y].setGraphic(new ImageView(bombHorizontalRight));
				} else {
					fields[x][y].setGraphic(new ImageView(bombHorizontal));
				}
				count--;
			}
		}));
		Timeline remove = new Timeline(new KeyFrame(Duration.seconds(.5), e -> {
			int delta_x, delta_y;
			int x, y;
			int count;
			fields[centerX][centerY].setGraphic(null);
			// up
			count = up;
			delta_x = 0;
			delta_y = -1;
			x = centerX;
			y = centerY;
			while (count > 0) {
				x += delta_x;
				y += delta_y;
				fields[x][y].setGraphic(null);
				count--;
			}
			// down
			count = down;
			delta_x = 0;
			delta_y = 1;
			x = centerX;
			y = centerY;
			while (count > 0) {
				x += delta_x;
				y += delta_y;
				fields[x][y].setGraphic(null);
				count--;
			}
			// left
			count = left;
			delta_x = -1;
			delta_y = 0;
			x = centerX;
			y = centerY;
			while (count > 0) {
				x += delta_x;
				y += delta_y;
				fields[x][y].setGraphic(null);
				count--;
			}
			// right
			count = right;
			delta_x = 1;
			delta_y = 0;
			x = centerX;
			y = centerY;
			while (count > 0) {
				x += delta_x;
				y += delta_y;
				fields[x][y].setGraphic(null);
				count--;
			}
		}));
		bomb.play();
		bomb.setOnFinished(e -> {
			charge.stop();
			explode.play();
			remove.play();
			if (playerName.equals(name)) {
				bombing = false;
			}
		});
	}

	/**
	 * Updates scoreArea to fit the current score of all players
	 *
	 * @param scores An array of strings containing playernames and their points
	 */
	public void updateScore(String[] scores) {
		StringBuffer b = new StringBuffer(100);
		for (int i = 1; i < scores.length; i++) {
			b.append(scores[i] + "\r\n");
		}
		scoreList.setText(b.toString());
	}

	public void roundFinished(String name, int kills) {
		// disable actions
		dead = true;
		Timer timer = new Timer(true);
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				Platform.runLater(() -> {
					// RoundFinishedWindow rf = new RoundFinishedWindow(name, kills);
					board.getChildren().add(new RoundFinishedWindow(name, kills));
					// rf.showAndWait();
				});
			}
		}, 3000);
		Timer close = new Timer(true);
		close.schedule(new TimerTask() {

			@Override
			public void run() {
				Platform.runLater(() -> {
					// RoundFinishedWindow rf = new RoundFinishedWindow(name, kills);
					board.getChildren().remove(1);
					// rf.showAndWait();
				});
			}
		}, 15000);
	}

	public void roundStart() {
		dead = false;
		clearBoard();
	}

	public void clearBoard() {
		for (int j = 0; j < 20; j++) {
			for (int i = 0; i < 20; i++) {
				fields[i][j].setGraphic(null);
			}
		}
	}

	public void updateAmmo(int ammo) {
		mp = new MediaPlayer(soundReload);
		mp.setVolume(.5);
		mp.play();
		ammoLabel.setText("Ammo: " + ammo);
	}

	public void spawnAmmo(int x, int y) {
		fields[x][y].setGraphic(new ImageView(image_ammo));
	}

	public void spawnUpgrade(int x, int y) {
		fields[x][y].setGraphic(new ImageView(bombUpgrade));
	}

	public void playerDead(int x, int y, String playerName) {
		mp = new MediaPlayer(soundScream);
		mp.setVolume(0.2);
		mp.play();
		if (playerName.equals(name)) {
			dead = true;
		}
		Timeline remove = new Timeline(new KeyFrame(Duration.seconds(.3), e -> {
			fields[x][y].setGraphic(new ImageView(image_dead));
		}));
		remove.play();
	}

	private void toggleMusic() {
		if (Client.playMusic) {
			Client.playMusic = false;
			btnMusic.setGraphic(new ImageView(imgMusicOff));
			Client.mp.pause();
		} else {
			Client.playMusic = true;
			btnMusic.setGraphic(new ImageView(imgMusicOn));
			Client.mp.play();
		}
	}

	public void loadImages() {
		image_wall3 = new Image(getClass().getResourceAsStream("Image/wall3.png"), size, size, false, false);
		image_wall4 = new Image(getClass().getResourceAsStream("Image/wall4.png"), size, size, false, false);
		image_wall5 = new Image(getClass().getResourceAsStream("Image/wall5.png"), size, size, false, false);
		hero_right = new Image(getClass().getResourceAsStream("Image/heroRight.png"), size, size, false, false);
		hero_left = new Image(getClass().getResourceAsStream("Image/heroLeft.png"), size, size, false, false);
		hero_up = new Image(getClass().getResourceAsStream("Image/heroUp.png"), size, size, false, false);
		hero_down = new Image(getClass().getResourceAsStream("Image/heroDown.png"), size, size, false, false);
		hero_rightRed = new Image(getClass().getResourceAsStream("Image/heroRightRed.png"), size, size, false, false);
		hero_leftRed = new Image(getClass().getResourceAsStream("Image/heroLeftRed.png"), size, size, false, false);
		hero_upRed = new Image(getClass().getResourceAsStream("Image/heroUpRed.png"), size, size, false, false);
		hero_downRed = new Image(getClass().getResourceAsStream("Image/heroDownRed.png"), size, size, false, false);
		fireDown = new Image(getClass().getResourceAsStream("Image/fireDown.png"), size, size, false, false);
		fireUp = new Image(getClass().getResourceAsStream("Image/fireUp.png"), size, size, false, false);
		fireLeft = new Image(getClass().getResourceAsStream("Image/fireLeft.png"), size, size, false, false);
		fireRight = new Image(getClass().getResourceAsStream("Image/fireRight.png"), size, size, false, false);
		fireHorizontal = new Image(getClass().getResourceAsStream("Image/fireHorizontal.png"), size, size, false,
				false);
		fireVertical = new Image(getClass().getResourceAsStream("Image/fireVertical.png"), size, size, false, false);
		fireWallEast = new Image(getClass().getResourceAsStream("Image/fireWallEast.png"), size, size, false, false);
		fireWallNorth = new Image(getClass().getResourceAsStream("Image/fireWallNorth.png"), size, size, false, false);
		fireWallWest = new Image(getClass().getResourceAsStream("Image/fireWallWest.png"), size, size, false, false);
		fireWallSouth = new Image(getClass().getResourceAsStream("Image/fireWallSouth.png"), size, size, false, false);
		image_ammo = new Image(getClass().getResourceAsStream("Image/ammo2.png"), size, size, false, false);
		image_dead = new Image(getClass().getResourceAsStream("Image/dead.png"), size, size, false, false);
		image_bomb = new Image(getClass().getResourceAsStream("Image/bomb.png"), size, size, false, false);
		bombCenter = new Image(getClass().getResourceAsStream("Image/bombCenter.png"), size, size, false, false);
		bombVertical = new Image(getClass().getResourceAsStream("Image/bombVertical.png"), size, size, false, false);
		bombVerticalUp = new Image(getClass().getResourceAsStream("Image/bombVerticalUp.png"), size, size, false,
				false);
		bombVerticalDown = new Image(getClass().getResourceAsStream("Image/bombVerticalDown.png"), size, size, false,
				false);
		bombHorizontal = new Image(getClass().getResourceAsStream("Image/bombHorizontal.png"), size, size, false,
				false);
		bombHorizontalLeft = new Image(getClass().getResourceAsStream("Image/bombHorizontalLeft.png"), size, size,
				false, false);
		bombHorizontalRight = new Image(getClass().getResourceAsStream("Image/bombHorizontalRight.png"), size, size,
				false, false);
		bombUpgrade = new Image(getClass().getResourceAsStream("Image/bombUpgrade.png"), size, size, false, false);
		imgMusicOn = new Image(getClass().getResourceAsStream("Image/music_on.png"), 20, 20, false, false);
		imgMusicOff = new Image(getClass().getResourceAsStream("Image/music_off.png"), 20, 20, false, false);
		// audio
		soundShoot = new Media(new File("src/client/Sound/shoot.wav/").toURI().toString());
		soundBombCharge = new Media(new File("src/client/Sound/BombCharge.mp3/").toURI().toString());
		soundExplosion = new Media(new File("src/client/Sound/Explosion.wav/").toURI().toString());
		soundScream = new Media(new File("src/client/Sound/scream.wav/").toURI().toString());
		soundReload = new Media(new File("src/client/Sound/reload.mp3/").toURI().toString());
		// IntelliJ
		// soundShoot = new Media(new File("MazeGame/src/client/Sound/shoot.wav/").toURI().toString());
		// soundBombCharge = new Media(new File("MazeGame/src/client/Sound/BombCharge.mp3/").toURI().toString());
		// soundExplosion = new Media(new File("MazeGame/src/client/Sound/Explosion.wav/").toURI().toString());
		// soundScream = new Media(new File("MazeGame/src/client/Sound/scream.wav/").toURI().toString());
		// soundReload = new Media(new File("MazeGame/src/client/Sound/reload.mp3/").toURI().toString());
	}
}
