package client;
import java.util.Optional;
import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.StageStyle;
import javafx.util.Pair;
public class Menu extends GridPane {

	private Button btnJoin, btnCreate, btnMusic;
	private String ip = "", name = "";
	private int port;
	private Label lblServer;
	private Image imgMusicOn, imgMusicOff;

	public Menu() {
		setPadding(new Insets(10));
		setVgap(20);
		setHgap(50);
		// setGridLinesVisible(true);
		// ----
		imgMusicOn = new Image(getClass().getResourceAsStream("Image/music_on.png"), 20, 20, false, false);
		imgMusicOff = new Image(getClass().getResourceAsStream("Image/music_off.png"), 20, 20, false, false);
		Image imgSocket = new Image(getClass().getResourceAsStream("Image/socketWinner.png"), 250, 400, false, false);
		ImageView socket = new ImageView(imgSocket);
		btnJoin = new Button("Join game");
		btnJoin.setMinWidth(320);
		btnJoin.setStyle("-fx-font: 30 arial; -fx-base: #ee2211;");
		btnJoin.setOnAction(e -> join());
		btnCreate = new Button("Create game");
		btnCreate.setMinWidth(320);
		btnCreate.setStyle("-fx-font: 30 arial; -fx-base: #ee2211;");
		btnCreate.setOnAction(e -> {
			try {
				create();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		});
		btnMusic = new Button("", new ImageView(imgMusicOn));
		btnMusic.setOnAction(e -> toggleMusic());
		lblServer = new Label();
		lblServer.setVisible(false);
		lblServer.setFont(Font.font("IMPACT", FontWeight.BOLD, 36));
		lblServer.setMinWidth(Client.scene_width);
		lblServer.setAlignment(Pos.BASELINE_CENTER);
		add(socket, 0, 0, 2, 1);
		add(btnJoin, 0, 1);
		add(btnCreate, 1, 1);
		add(lblServer, 0, 2, 2, 1);
		add(btnMusic, 1, 3);
		GridPane.setHalignment(socket, HPos.CENTER);
		GridPane.setHalignment(btnJoin, HPos.RIGHT);
		GridPane.setHalignment(btnCreate, HPos.LEFT);
		GridPane.setHalignment(lblServer, HPos.CENTER);
		GridPane.setHalignment(btnMusic, HPos.RIGHT);
		GridPane.setValignment(btnMusic, VPos.BOTTOM);
		TextInputDialog dialog = new TextInputDialog();
		dialog.setContentText("Enter your name:");
		dialog.initStyle(StageStyle.UNDECORATED);
		while (name.length() < 1) {
			Optional<String> result = dialog.showAndWait();
			if (result.isPresent() && result.get().length() > 0) {
				String modified = "";
				for (char c : result.get().toCharArray()) {
					if (c == 'æ' || c == 'Æ') {
						modified += "ae";
					} else if (c == 'ø' || c == 'Ø') {
						modified += "oe";
					} else if (c == 'å' || c == 'Å') {
						modified += "aa";
					} else {
						modified += c;
					}
				}
				name = modified;
			}
		}
		Client.createGame(name);
	}

	private void join() {
		Dialog<Pair<String, String>> dialog = new Dialog<>();
		dialog.setTitle("TestName");
		// Set the button types.
		ButtonType connectButtonType = new ButtonType("Connect", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(connectButtonType, ButtonType.CANCEL);
		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));
		TextField txfIp = new TextField();
		TextField txfPort = new TextField();
		grid.add(new Label("Server IP:"), 0, 0);
		grid.add(txfIp, 1, 0);
		grid.add(new Label("Server port:"), 0, 1);
		grid.add(txfPort, 1, 1);
		// Enable/Disable login button depending on whether a username was entered.
		Node connectButton = dialog.getDialogPane().lookupButton(connectButtonType);
		connectButton.setDisable(true);
		// Do some validation
		txfIp.textProperty().addListener((observable, oldValue, newValue) -> {
			connectButton.setDisable(newValue.trim().isEmpty());
		});
		dialog.getDialogPane().setContent(grid);
		// Request focus on the username field by default.
		Platform.runLater(() -> {
			txfIp.requestFocus();
			if (ip.length() > 0) {
				txfIp.setText(ip);
			}
			if (port > 0) {
				txfPort.setText("" + port);
			}
		});
		// Convert the result to a username-password-pair when the login button is clicked.
		dialog.setResultConverter(dialogButton -> {
			if (dialogButton == connectButtonType) {
				return new Pair<>(txfIp.getText(), txfPort.getText());
			}
			return null;
		});
		Optional<Pair<String, String>> result = dialog.showAndWait();
		result.ifPresent(serverInfo -> {
			String srvIp = serverInfo.getKey();
			int srvPort = Integer.parseInt(serverInfo.getValue());
			Client.joinServer(srvIp, srvPort);
		});
	}

	private void create() throws Exception {
		port = -1;
		TextInputDialog dialog = new TextInputDialog();
		dialog.setContentText("Enter port: ");
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {
			try {
				port = Integer.parseInt(result.get());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (port > 0) {
			Alert alert = new Alert(AlertType.INFORMATION);
			ip = Client.createServer(port);
			alert.setTitle("Success");
			alert.setHeaderText("Server created on: ");
			alert.setContentText("IP: " + ip + "\nPort: " + port);
			alert.showAndWait();
			lblServer.setText("IP: " + ip + "\t\t|\tPort: " + port);
			lblServer.setVisible(true);
		}
	}

	private void toggleMusic() {
		if (Client.playMusic) {
			Client.playMusic = false;
			btnMusic.setGraphic(new ImageView(imgMusicOff));
			Client.mp.pause();
		} else {
			Client.playMusic = true;
			btnMusic.setGraphic(new ImageView(imgMusicOn));
			Client.mp.play();
		}
	}
}
