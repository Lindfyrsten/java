package client;
import java.io.File;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
public class RoundFinishedWindow extends StackPane {

	private Label timerLabel = new Label();
	private int seconds = 11;

	public RoundFinishedWindow(String name, int kills) {
		// StackPane pane = new StackPane();
		setMaxSize(Game.board.getWidth(), Game.board.getHeight());
		setPadding(new Insets(150, 0, 0, 0));
		setBackground(new Background(new BackgroundFill(Color.BLACK, null, Insets.EMPTY)));
		// Scene scene = new Scene(pane, 700, 600);
		// setScene(scene);
		// ----
		MediaPlayer count = new MediaPlayer(new Media(new File("src/client/Sound/countdown.mp3/").toURI().toString()));
		count.setVolume(.3);
		count.play();
		DropShadow ds = new DropShadow();
		ds.setOffsetY(10.0f);
		ds.setColor(Color.DARKRED);
		Label lblWinner = new Label();
		if (name.equals("draw")) {
			lblWinner.setText("ROUND DRAW!");
		} else {
			lblWinner.setText(name + " WON!");
		}
		lblWinner.setFont(Font.font("IMPACT", FontWeight.BOLD, 72));
		lblWinner.setTextFill(Color.WHITE);
		lblWinner.setAlignment(Pos.CENTER);
		// lblWinner.setEffect(ds);
		Label lblCount = new Label("Next round in...");
		lblCount.setFont(Font.font("Arial", 48));
		lblCount.setTextFill(Color.GREY);
		// lblCount.setEffect(ds);
		timerLabel.setTextFill(Color.YELLOW);
		timerLabel.setFont(Font.font("IMPACT", FontWeight.BOLD, 128));
		timerLabel.setEffect(ds);
		getChildren().addAll(lblWinner, lblCount, timerLabel);
		StackPane.setAlignment(lblWinner, Pos.BASELINE_CENTER);
		StackPane.setAlignment(lblCount, Pos.TOP_CENTER);
		StackPane.setAlignment(timerLabel, Pos.CENTER);
		StackPane.setMargin(lblCount, new Insets(25, 0, 0, 0));
		StackPane.setMargin(timerLabel, new Insets(25, 0, 0, 0));
		Timer t = new Timer(true);
		t.schedule(new TimerTask() {

			@Override
			public void run() {
				seconds--;
				if (seconds >= 0) {
					Platform.runLater(() -> {
						timerLabel.setText("" + seconds);
					});
				}
				if (seconds == 9) {
					Platform.runLater(() -> {
						timerLabel.setTextFill(Color.GREEN);
					});
				} else if (seconds == 8) {
					Platform.runLater(() -> {
						timerLabel.setTextFill(Color.BLUE);
					});
				} else if (seconds == 7) {
					Platform.runLater(() -> {
						timerLabel.setTextFill(Color.HOTPINK);
					});
				} else if (seconds == 6) {
					Platform.runLater(() -> {
						timerLabel.setTextFill(Color.RED);
					});
				} else if (seconds == 5) {
					Platform.runLater(() -> {
						timerLabel.setTextFill(Color.YELLOW);
					});
				} else if (seconds == 4) {
					Platform.runLater(() -> {
						timerLabel.setTextFill(Color.GREEN);
					});
				} else if (seconds == 3) {
					Platform.runLater(() -> {
						timerLabel.setTextFill(Color.AQUAMARINE);
					});
				} else if (seconds == 2) {
					Platform.runLater(() -> {
						timerLabel.setTextFill(Color.BLUE);
					});
				} else if (seconds == 1) {
					Platform.runLater(() -> {
						timerLabel.setTextFill(Color.PURPLE);
					});
				} else if (seconds == 0) {
					Platform.runLater(() -> {
						timerLabel.setTextFill(Color.GREEN);
						timerLabel.setText("GO!");
					});
				} else if (seconds == -1) {
					t.cancel();
					count.stop();
				}
			}
		}, 1000, 1000);
	}
}
