package client;
import java.io.File;
import java.io.IOException;
import java.net.Socket;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.util.Duration;
import server.Server;
public class Client extends Application {

	public static final int size = 25;
	public static final int scene_height = size * 20 + 100;
	public static final int scene_width = size * 20 + 200;
	private static Game game;
	private static Socket clientSocket;
	private static ClientThread thread;
	private static Server server;
	public static Stage stage;
	private static Scene gameScene, menuScene;
	public static MediaPlayer mp;
	public static boolean playMusic = true;

	@Override
	public void start(Stage primaryStage) throws Exception {
		stage = primaryStage;
		menuScene = new Scene(new Menu(), scene_width, scene_height);
		stage.setScene(menuScene);
		stage.show();
		stage.centerOnScreen();
		stage.setResizable(false);
		stage.setTitle("Socket Game");
		mp = new MediaPlayer(new Media(new File("src/client/Sound/menu.wav/").toURI().toString()));
		mp.setCycleCount(MediaPlayer.INDEFINITE);
		mp.setVolume(.02);
		mp.setStartTime(Duration.ZERO);
		mp.setStopTime(Duration.millis(14670));
		mp.play();
	}

	public static void createGame(String name) {
		game = new Game(name);
		gameScene = new Scene(game, scene_width, scene_height);
		gameScene.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
			switch (event.getCode()) {
				case UP :
					try {
						move(0, -1, Direction.UP);
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;
				case DOWN :
					try {
						move(0, +1, Direction.DOWN);
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;
				case LEFT :
					try {
						move(-1, 0, Direction.LEFT);
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;
				case RIGHT :
					try {
						move(+1, 0, Direction.RIGHT);
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;
				case SPACE :
					try {
						shoot();
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;
				case B :
					try {
						dropBomb();
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;
				case ESCAPE :
					try {
						quit();
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;
				default :
					break;
			}
		});
	}

	public static void joinServer(String ip, int port) {
		boolean connected = true;
		try {
			clientSocket = new Socket(ip, port);
			thread = new ClientThread(clientSocket, game);
			thread.start();
		} catch (Exception e) {
			// ignore
			connected = false;
		}
		if (connected) {
			stage.setScene(gameScene);
			stage.setTitle("Connected to: " + ip + ":" + port);
			Timeline fade = new Timeline(new KeyFrame(Duration.seconds(1), new KeyValue(mp.volumeProperty(), 0)));
			fade.play();
			mp = new MediaPlayer(new Media(new File("src/client/Sound/game.wav/").toURI().toString()));
			mp.setCycleCount(MediaPlayer.INDEFINITE);
			mp.setVolume(0);
			mp.setStartTime(Duration.ZERO);
			mp.setStopTime(Duration.millis(11200));
			if (playMusic) {
				Game.btnMusic.setGraphic(new ImageView(Game.imgMusicOn));
				mp.play();
				Timeline start = new Timeline(
						new KeyFrame(Duration.seconds(1), new KeyValue(mp.volumeProperty(), .02)));
				start.play();
			} else {
				Game.btnMusic.setGraphic(new ImageView(Game.imgMusicOff));
				mp.setVolume(.02);
			}
		} else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setHeaderText("Failed to connect to server");
			alert.setContentText("Make sure IP/Port numbers are correct");
			alert.showAndWait();
		}
	}

	public static String createServer(int port) throws Exception {
		server = new Server(port);
		server.start();
		return server.ip;
	}

	private static void move(int delta_x, int delta_y, Direction direction) throws Exception {
		// do not move if player is Game.shooting
		if (!game.dead && !game.shooting) {
			// tell the thread we want to move and include delta_x, delta_y & in which direction
			thread.movePlayer(delta_x, delta_y, direction);
		}
	}

	private static void shoot() throws Exception {
		// do not shoot if player is already within the shoot timeline from a previous shot
		if (!game.dead && !game.shooting) {
			// tell the thread we want to shoot and include the name of our client
			thread.shoot(game.name);
		}
	}

	private static void dropBomb() throws IOException {
		if (!game.dead && !game.bombing && !game.shooting) {
			thread.dropBomb(game.name);
		}
	}

	private static void quit() throws Exception {
		// tell the thread we are quitting
		thread.quit();
		Platform.exit();
		System.exit(0);
	}

	public static void main(String[] args) throws Exception {
		launch(args);
	}
}
