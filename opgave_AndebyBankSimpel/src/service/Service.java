package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import guifx.HistorikTab;
import model.Konto;
import model.Kunde;
import model.Medarbejder;

public class Service {
    private static Connection minConnection;

    public static void runDB() {

        try {
            minConnection = DriverManager.getConnection(
                "jdbc:sqlserver://LINDBJERG-PC;databaseName=AndebyBank;user=sa;password=Krille1987;");
            Statement stmt = minConnection.createStatement();
            stmt.execute(
                "IF OBJECT_ID('trg_kundehistory') IS NOT NULL DROP TRIGGER trg_kundehistory");
            stmt.execute(
                "IF OBJECT_ID('trg_kontohistory') IS NOT NULL DROP TRIGGER trg_kontohistory");
            stmt.execute(
                "IF OBJECT_ID('proc_TaelTransaktioner') IS NOT NULL DROP PROCEDURE proc_TaelTransaktioner");
            stmt.execute(
                "IF OBJECT_ID('proc_KontiPaaAfdeling') IS NOT NULL DROP PROCEDURE proc_KontiPaaAfdeling");
            stmt.execute(
                "IF OBJECT_ID('view_Afdelinger') IS NOT NULL DROP VIEW view_Afdelinger");
            stmt.execute(
                "CREATE TRIGGER trg_kundehistory on Kunde for delete as insert into KundeHistory select * from deleted");
            stmt.execute(
                "CREATE TRIGGER trg_kontohistory on Konto for delete as insert into KontoHistory select regNr, ktoNr, tekst, saldo from deleted");
            stmt.execute(
                "create procedure proc_TaelTransaktioner as select ktoNr, sum(beløb) from Transaktion group by ktoNr");
            stmt.execute(
                "create procedure proc_KontiPaaAfdeling as select A.navn, A.regNr, count(*) KontiPaaAfdeling from Afdeling A inner join Konto K on A.regNr = K.regNr group by A.navn, A.regNr");
            stmt.execute(
                "create view view_Afdelinger as select A.navn, M.titel, M.navn as Medarbejdernavn, M.adresse, M.postNr, P.bynavn from Afdeling A, PostDistrikt P, Medarbejder M where A.regNr = M.afdeling and A.postNr = P.postNr group by A.regNr, M.postNr, P.bynavn, A.navn, M.titel, M.navn, M.adresse");
            if (stmt != null) {
                stmt.close();
            }
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }
    
    public static Connection getMinConnection() {
        return minConnection;
    }
    
    public static Kunde opretKunde(String cprNr, String navn, String adresse, int postNr) {
        Kunde kunde = new Kunde(cprNr, navn, adresse, postNr);
        String sql = "insert into Kunde values (?,?,?,?)";
        PreparedStatement prestmt;
        try {
            prestmt = minConnection.prepareStatement(sql);
            prestmt.clearParameters();
            prestmt.setString(1, cprNr);
            prestmt.setString(2, navn);
            prestmt.setString(3, adresse);
            prestmt.setInt(4, postNr);
            prestmt.execute();
            System.out.println("Kunden er nu oprettet");
            if (prestmt != null) {
                prestmt.close();
            }
        }
        catch (SQLException e) {
            System.out.println("fejlSQL:  " + e.getMessage());
        }
        return kunde;
    }

    public static Konto opretKonto(int regNr, int ktoNr, String tekst, double saldo,
        double renteInd, double renteUd, Kunde kunde) {
        Konto konto = new Konto(regNr, ktoNr, tekst, saldo, renteInd, renteUd);
        try {
            String k = "insert into Konto values (?,?,?,?,?,?)";
            String khk = "insert into KundeHarKonto values (?,?,?)";
            PreparedStatement prestmt;
            prestmt = minConnection.prepareStatement(k);
            prestmt.clearParameters();
            prestmt.setInt(1, regNr);
            prestmt.setInt(2, ktoNr);
            prestmt.setString(3, tekst);
            prestmt.setDouble(4, saldo);
            prestmt.setDouble(5, renteInd);
            prestmt.setDouble(6, renteUd);
            prestmt.execute();
            prestmt = minConnection.prepareStatement(khk);
            prestmt.clearParameters();
            prestmt.setString(1, kunde.getCprNr());
            prestmt.setInt(2, regNr);
            prestmt.setInt(3, ktoNr);
            prestmt.execute();
            System.out.println("Konto er nu oprettet");
            if (prestmt != null) {
                prestmt.close();
            }
        }
        catch (SQLException e) {
            System.out.println("fejlSQL:  " + e.getMessage());
        }
        return konto;
    }

    public static ArrayList<Kunde> getKunder() {
        ArrayList<Kunde> kunder = new ArrayList<>();
        try {
            Statement stmt = minConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                ResultSet.CONCUR_UPDATABLE);

            ResultSet res = stmt.executeQuery("select cprNr, navn, adresse, postNr from Kunde");
            while (res.next()) {
                String cprNr = res.getString(1);
                String navn = res.getString(2);
                String adresse = res.getString(3);
                int postNr = res.getInt(4);
                kunder.add(new Kunde(cprNr, navn, adresse, postNr));
            }
            if (res != null) {
                res.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        catch (SQLException e) {
            System.out.println("fejl: " + e.getMessage());
        }
        return kunder;
    }

    public static ArrayList<Konto> getKontoer(Kunde k) {
        ArrayList<Konto> kontoer = new ArrayList<>();
        try {
            Statement stmt = minConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                ResultSet.CONCUR_UPDATABLE);

            ResultSet res = stmt.executeQuery(
                "select K.regNr, K.ktoNr, K.tekst, K.saldo, K.renteIndlån, K.renteUdlån from Konto K, KundeHarKonto Khk where Khk.cprNr = '"
                    + k.getCprNr() + "' and K.ktoNr = Khk.ktonr;");
            while (res.next()) {
                int regNr = res.getInt(1);
                int ktoNr = res.getInt(2);
                String tekst = res.getString(3);
                double saldo = res.getDouble(4);
                double renteInd = res.getDouble(5);
                double renteUd = res.getDouble(6);
                kontoer.add(new Konto(regNr, ktoNr, tekst, saldo, renteInd, renteUd));
            }
            if (res != null) {
                res.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        catch (SQLException e) {
            System.out.println("fejl: " + e.getMessage());
        }
        return kontoer;
    }

    public static void sletKunde(String cprNr) {
        try {
            Statement stmt = minConnection.createStatement();
            stmt.execute(
                "delete Konto from Konto inner join KundeHarKonto on Konto.ktoNr = KundeHarKonto.ktonr and KundeHarKonto.cprNr = '"
                    + cprNr + "'");
            stmt.execute("delete from KundeHarKonto where cprNr = '" + cprNr + "'");
            stmt.execute("delete from Kunde where '" + cprNr + "' = cprNr");
            System.out.println("Kunden og tilknyttede kontoer er nu slettet");
            HistorikTab.updateHistorik();
            if (stmt != null) {
                stmt.close();
            }

        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static ArrayList<Konto> getKontoHistorik() {
        ArrayList<Konto> kontoer = new ArrayList<>();
        try {
            Statement stmt = minConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                ResultSet.CONCUR_UPDATABLE);

            ResultSet res = stmt.executeQuery(
                "select regNr, ktoNr, tekst, saldo from KontoHistory");
            while (res.next()) {
                int regNr = res.getInt(1);
                int ktoNr = res.getInt(2);
                String tekst = res.getString(3);
                double saldo = res.getDouble(4);
                kontoer.add(new Konto(regNr, ktoNr, tekst, saldo));
            }
            if (res != null) {
                res.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        catch (SQLException e) {
            System.out.println("fejl: " + e.getMessage());
        }
        return kontoer;
    }

    public static ArrayList<Kunde> getKundeHistorik() {
        ArrayList<Kunde> kunder = new ArrayList<>();
        try {
            Statement stmt = minConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                ResultSet.CONCUR_UPDATABLE);

            ResultSet res =
                stmt.executeQuery("select cprNr, navn, adresse, postNr from KundeHistory");
            while (res.next()) {
                String cprNr = res.getString(1);
                String navn = res.getString(2);
                String adresse = res.getString(3);
                int postNr = res.getInt(4);
                kunder.add(new Kunde(cprNr, navn, adresse, postNr));
            }
            if (res != null) {
                res.close();
            }
            if (stmt != null) {
                stmt.close();
            }
        }
        catch (SQLException e) {
            System.out.println("fejl: " + e.getMessage());
        }
        return kunder;

    }

    public static ArrayList<String> taelTransaktioner() {
        ArrayList<String> transaktioner = new ArrayList<>();
        try {
            Statement stmt = minConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                ResultSet.CONCUR_UPDATABLE);
            
            ResultSet res;
            res = stmt.executeQuery("proc_TaelTransaktioner");
            while (res.next()) {
                transaktioner.add("Kontonummer: " + res.getInt(1) + " Transaktioner i alt: "
                    + res.getDouble(2) + " kr.");
            }
            if (res != null) {
                res.close();
            }
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        return transaktioner;

    }
    
    public static ArrayList<String> kontiPaaAfdeling() {
        ArrayList<String> konit = new ArrayList<>();
        try {
            Statement stmt = minConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                ResultSet.CONCUR_UPDATABLE);
            
            ResultSet res;
            res = stmt.executeQuery("proc_KontiPaaAfdeling");
            while (res.next()) {
                konit.add("Afdelingsnavn: " + res.getString(1));
                konit.add("Reg. nr: "
                    + res.getInt(2) + " Konti: " + res.getInt(3));
                konit.add("*********************************************");
            }
            if (res != null) {
                res.close();
            }
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        
        return konit;

    }
    
    public static ArrayList<Medarbejder> viewMedarbejdere() {
        ArrayList<Medarbejder> afdelinger = new ArrayList<>();
        try {
            Statement stmt = minConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                ResultSet.CONCUR_UPDATABLE);
            
            ResultSet res;
            res = stmt.executeQuery("select * from view_Afdelinger");
            while (res.next()) {
                String afd = res.getString(1);
                String titel = res.getString(2);
                String navn = res.getString(3);
                String adr = res.getString(4);
                int post = res.getInt(5);
                String by = res.getString(6);
                afdelinger.add(new Medarbejder(afd, titel, navn, adr, post, by));

            }
            if (res != null) {
                res.close();
            }
        }
        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return afdelinger;

    }
}
