package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Kunde {
    private StringProperty navn, adresse, cprNr;
    private IntegerProperty postNr;
    
    public Kunde(String cprNr, String navn, String adresse, int postNr) {
        this.cprNr = new SimpleStringProperty(cprNr);
        this.navn = new SimpleStringProperty(navn);
        this.adresse = new SimpleStringProperty(adresse);
        this.postNr = new SimpleIntegerProperty(postNr);

    }
    
    public String getNavn() {
        return navn.get();
    }

    public String getCprNr() {
        return cprNr.get();
    }

    public String getAdresse() {
        return adresse.get();
    }

    public int getPostNr() {
        return postNr.get();
    }
    
    @Override
    public String toString() {
        return navn.get() + " CPR: " + cprNr.get();
    }
    
}
