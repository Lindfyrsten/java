package model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Medarbejder {
    private StringProperty navn, adr, titel, by, afd;
    private IntegerProperty post;

    public Medarbejder(String afd, String titel, String navn, String adr, int post, String by) {
        this.afd = new SimpleStringProperty(afd);
        this.titel = new SimpleStringProperty(titel);
        this.navn = new SimpleStringProperty(navn);
        this.adr = new SimpleStringProperty(adr);
        this.post = new SimpleIntegerProperty(post);
        this.by = new SimpleStringProperty(by);
        
    }
    
    public String getAfd() {
        return afd.get();
    }

    public String getTitel() {
        return titel.get();
    }

    public String getNavn() {
        return navn.get();
    }
    
    public String getAdr() {
        return adr.get();
    }

    public int getPost() {
        return post.get();
    }

    public String getBy() {
        return by.get();
    }

}
