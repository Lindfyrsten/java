package model;

public class Konto {
    private int regNr, ktoNr;
    private String tekst;
    private double saldo, renteInd, renteUd;

    public Konto(int regNr, int ktoNr, String tekst, double saldo, double renteInd,
        double renteUd) {
        this.regNr = regNr;
        this.ktoNr = ktoNr;
        this.tekst = tekst;
        this.saldo = saldo;
        this.renteInd = renteInd;
        this.renteUd = renteUd;
        
    }
    
    public Konto(int regNr, int ktoNr, String tekst, double saldo) {
        this.regNr = regNr;
        this.ktoNr = ktoNr;
        this.tekst = tekst;
        this.saldo = saldo;
        
    }
    
    public int getKtoNr() {
        return ktoNr;
    }
    
    public double getRenteInd() {
        return renteInd;
    }
    
    public double getRenteUd() {
        return renteUd;
    }

    public int getRegNr() {
        return regNr;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public double getSaldo() {
        return saldo;
    }
    
    public void setTekst(String tekst) {
        this.tekst = tekst;
    }

    public String getTekst() {
        return tekst;
    }
    
    @Override
    public String toString() {
        return tekst + " (Saldo: " + saldo + ")";
    }
}
