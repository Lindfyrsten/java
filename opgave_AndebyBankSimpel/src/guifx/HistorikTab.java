package guifx;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import model.Konto;
import model.Kunde;
import service.Service;

/**
 * @author Kristian Lindbjerg
 */
public class HistorikTab extends GridPane {
    private static ListView<Konto> lvwKonto;
    private static TableView<Kunde> tblKunder;
    private static ObservableList<Kunde> kunderObservable;
    private static TableColumn<Kunde, String> cprCol;
    private static TableColumn<Kunde, String> navnCol;
    private static TableColumn<Kunde, String> adrCol;
    private static TableColumn<Kunde, Integer> postCol;

    public HistorikTab() {
        setPadding(new Insets(10, 10, 10, 0));
        setVgap(20);
        setHgap(20);
        
        initContent();

    }

    private void initContent() {
        Label lblHistorik = new Label("Historik");
        Label lblKunder = new Label("Kunder");
        Label lblKonto = new Label("Kontoer");
        lblHistorik.setFont(Font.font("Arial", 18));

        lvwKonto = new ListView<>();
        tblKunder = new TableView<>();
        tblKunder.setPrefWidth(520);
        tblKunder.setItems(kunderObservable);
        cprCol = new TableColumn<>("Cpr nummer");
        cprCol.setCellValueFactory(new PropertyValueFactory<>("cprNr"));
        navnCol = new TableColumn<>("Navn");
        navnCol.setCellValueFactory(new PropertyValueFactory<>("navn"));
        adrCol = new TableColumn<>("Adresse");
        adrCol.setCellValueFactory(new PropertyValueFactory<>("adresse"));
        postCol = new TableColumn<>("Postnummer");
        postCol.setCellValueFactory(new PropertyValueFactory<>("postNr"));
        tblKunder.getColumns().setAll(cprCol, navnCol, adrCol, postCol);

        add(lblHistorik, 0, 0, 3, 1);
        add(lblKunder, 1, 1);
        add(tblKunder, 1, 2);
        add(lblKonto, 2, 1);
        add(lvwKonto, 2, 2);
        setHalignment(lblHistorik, HPos.CENTER);
        setHalignment(lblKonto, HPos.CENTER);
        setValignment(lblKonto, VPos.BOTTOM);
        setHalignment(lblKunder, HPos.CENTER);
        updateHistorik();
        
    }

    public static void updateHistorik() {
        lvwKonto.getItems().setAll(Service.getKontoHistorik());
        kunderObservable = FXCollections.observableArrayList(Service.getKundeHistorik());
        tblKunder.setItems(kunderObservable);
    }
    
}
