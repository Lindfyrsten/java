package guifx;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import service.Service;

/**
 * @author Kristian Lindbjerg
 */
public class TransaktionTab extends GridPane {

    private Button btnTaelSaldo, btnKontiPaaAfdeling;
    
    private ListView<String> lvwTransaktioner;
    private ListView<String> lvwKontoer;

    public TransaktionTab() {
        setPadding(new Insets(10, 10, 10, 60));
        setVgap(20);
        setHgap(50);

        initContent();

    }

    private void initContent() {
        Label lblTransaktion = new Label("Transaktionsliste");
        lblTransaktion.setFont(Font.font("Arial", 18));
        Label lblKonti = new Label("Konti pr afdeling");

        btnTaelSaldo = new Button("Se samlede transaktioner");
        btnTaelSaldo.setOnAction(event -> taelTransaktioner());
        btnKontiPaaAfdeling = new Button("Se konti på afdelinger");
        btnKontiPaaAfdeling.setOnAction(event -> kontoPaaAfdeling());
        lvwTransaktioner = new ListView<>();
        lvwTransaktioner.setMinWidth(320);
        lvwKontoer = new ListView<>();
        lvwKontoer.setMinWidth(320);

        add(lblTransaktion, 0, 0);
        add(btnTaelSaldo, 0, 1);
        add(lvwTransaktioner, 0, 2);
        add(lblKonti, 1, 0);
        add(btnKontiPaaAfdeling, 1, 1);
        add(lvwKontoer, 1, 2);
        setHalignment(lblTransaktion, HPos.CENTER);
        setHalignment(btnTaelSaldo, HPos.CENTER);
        setHalignment(lblKonti, HPos.CENTER);
        setHalignment(btnKontiPaaAfdeling, HPos.CENTER);
        
    }

    private void kontoPaaAfdeling() {
        lvwKontoer.getItems().setAll(Service.kontiPaaAfdeling());
    }
    
    private void taelTransaktioner() {
        lvwTransaktioner.getItems().setAll(Service.taelTransaktioner());

    }
    
}
