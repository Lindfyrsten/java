package guifx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import service.Service;

public class MainApp extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) {
        Service.runDB();
        BorderPane bPane = initContent();
        stage.setTitle("Andeby Bank");
        Scene scene = new Scene(bPane, 800, 400);
        stage.setScene(scene);
        stage.show();
//        stage.setResizable(false);

        initPane();
    }
    
    /**
     * @return
     */
    private BorderPane initContent() {
        BorderPane borderPane = new BorderPane();
        TabPane tabPane = new TabPane();
        initTabs(tabPane);
        borderPane.setCenter(tabPane);

        return borderPane;
    }

    private Tab kunde, konto, historik, transaktioner, medarbejder;

    private void initTabs(TabPane pane) {
        kunde = new Tab("Opret Kunde");
        konto = new Tab("Opret Konto");
        historik = new Tab("Historik");
        transaktioner = new Tab("Transaktioner/Afdelingskonti");
        medarbejder = new Tab("Medarbejderliste");
        pane.getTabs().addAll(kunde, konto, historik, transaktioner, medarbejder);
        pane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
    }
    
    public void initPane() {
        KundeTab kTab = new KundeTab();
        kunde.setContent(kTab);
        KontoTab koTab = new KontoTab();
        konto.setContent(koTab);
        HistorikTab hiTab = new HistorikTab();
        historik.setContent(hiTab);
        TransaktionTab tTab = new TransaktionTab();
        transaktioner.setContent(tTab);
        MedarbejderTab mTab = new MedarbejderTab();
        medarbejder.setContent(mTab);
        
    }

    @Override
    public void stop() throws Exception {
        Service.getMinConnection().close();
        super.stop();
    }
    
}
