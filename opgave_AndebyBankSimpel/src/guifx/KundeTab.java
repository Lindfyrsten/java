package guifx;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import model.Kunde;
import service.Service;

/**
 * @author Kristian Lindbjerg
 */
public class KundeTab extends GridPane {
    private TextField txfCpr, txfNavn, txfAdresse, txfPostNr;
    private Button btnOK, btnDelete;
    private TableView<Kunde> tblKunder;
    private ObservableList<Kunde> kunderObservable;
    
    public KundeTab() {
        setPadding(new Insets(10, 10, 10, 10));
        setVgap(20);
        setHgap(20);
        
        initContent();

    }

    private void initContent() {
        
        Label lblOpret = new Label("Opret Kunde");
        lblOpret.setFont(Font.font(18));
        
        txfCpr = new TextField();
        txfCpr.setPromptText("CPR nummer");
        txfNavn = new TextField();
        txfNavn.setPromptText("Navn");
        txfAdresse = new TextField();
        txfAdresse.setPromptText("Adresse");
        txfPostNr = new TextField();
        txfPostNr.setPromptText("Postnummer");
        
        btnOK = new Button("Opret Kunde");
        btnOK.setPrefSize(100, 30);
        btnOK.setOnAction(event -> opretKunde());
        btnDelete = new Button("Slet Kunde");
        btnDelete.setOnAction(event -> sletKunde());
        
        kunderObservable = FXCollections.observableArrayList(Service.getKunder());
        
        tblKunder = new TableView<>();
        tblKunder.setPrefWidth(520);
        tblKunder.setItems(kunderObservable);
        TableColumn<Kunde, String> cprCol = new TableColumn<>("Cpr nummer");
        cprCol.setCellValueFactory(new PropertyValueFactory<>("cprNr"));
        TableColumn<Kunde, String> navnCol = new TableColumn<>("Navn");
        navnCol.setCellValueFactory(new PropertyValueFactory<>("navn"));
        TableColumn<Kunde, String> adrCol = new TableColumn<>("Adresse");
        adrCol.setCellValueFactory(new PropertyValueFactory<>("adresse"));
        TableColumn<Kunde, Integer> postCol = new TableColumn<>("Postnummer");
        postCol.setCellValueFactory(new PropertyValueFactory<>("postNr"));
        tblKunder.getColumns().setAll(cprCol, navnCol, adrCol, postCol);
        
        add(lblOpret, 0, 0, 3, 1);
        add(txfNavn, 0, 2);
        add(txfCpr, 1, 2);
        add(txfAdresse, 0, 3);
        add(txfPostNr, 1, 3);
        add(btnOK, 1, 4);
        add(btnDelete, 1, 5);
        add(tblKunder, 2, 2, 1, 4);
        
        setHalignment(lblOpret, HPos.CENTER);
        setValignment(btnOK, VPos.TOP);
        setValignment(btnDelete, VPos.BOTTOM);
        setHalignment(btnDelete, HPos.RIGHT);
    }
    
    private void opretKunde() {
        
        String cprNr = txfCpr.getText();
        String navn = txfNavn.getText();
        String adresse = txfAdresse.getText();
        int postNr = Integer.parseInt(txfPostNr.getText().trim());
        Service.opretKunde(cprNr, navn, adresse, postNr);
        clear();
        updateList();
        
    }

    private void clear() {
        txfAdresse.clear();
        txfCpr.clear();
        txfNavn.clear();
        txfPostNr.clear();
    }
    
    private void updateList() {
        kunderObservable = FXCollections.observableArrayList(Service.getKunder());
        tblKunder.setItems(kunderObservable);

    }

    private void sletKunde() {
        Kunde k = tblKunder.getSelectionModel().getSelectedItem();
        Service.sletKunde(k.getCprNr());
        updateList();
    }
}
