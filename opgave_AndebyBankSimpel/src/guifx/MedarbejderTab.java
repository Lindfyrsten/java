package guifx;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import model.Medarbejder;
import service.Service;

/**
 * @author Kristian Lindbjerg
 */
public class MedarbejderTab extends GridPane {

    private Button btnMedarbejder;
    
    private TableView<Medarbejder> tblMedarb;
    private ObservableList<Medarbejder> obs;

    public MedarbejderTab() {
        setPadding(new Insets(10, 10, 10, 60));
        setVgap(20);
        setHgap(50);

        initContent();

    }

    private void initContent() {
        Label lblMedarbejder = new Label("Medarbejderliste");
        lblMedarbejder.setFont(Font.font("Arial", 18));

        btnMedarbejder = new Button("Se medarbejderliste");
        btnMedarbejder.setOnAction(event -> medarbejderListe());

        tblMedarb = new TableView<>();

        TableColumn<Medarbejder, String> afdCol = new TableColumn<>("Afdelingsnavn");
        afdCol.setCellValueFactory(new PropertyValueFactory<>("afd"));
        TableColumn<Medarbejder, String> titelCol = new TableColumn<>("Titel");
        titelCol.setCellValueFactory(new PropertyValueFactory<>("titel"));
        TableColumn<Medarbejder, String> navnCol = new TableColumn<>("Navn");
        navnCol.setCellValueFactory(new PropertyValueFactory<>("navn"));
        TableColumn<Medarbejder, String> adrCol = new TableColumn<>("Adresse");
        adrCol.setCellValueFactory(new PropertyValueFactory<>("adr"));
        TableColumn<Medarbejder, Integer> postCol = new TableColumn<>("Postnr");
        postCol.setCellValueFactory(new PropertyValueFactory<>("post"));
        TableColumn<Medarbejder, String> byCol = new TableColumn<>("By");
        byCol.setCellValueFactory(new PropertyValueFactory<>("by"));
        tblMedarb.getColumns().setAll(afdCol, titelCol, navnCol, adrCol, postCol, byCol);
        afdCol.setPrefWidth(120);
        titelCol.setPrefWidth(110);
        navnCol.setPrefWidth(160);
        adrCol.setPrefWidth(160);
        postCol.setPrefWidth(60);
        byCol.setPrefWidth(100);
        add(lblMedarbejder, 0, 0);
        add(btnMedarbejder, 0, 1);
        add(tblMedarb, 0, 2);
        
        setHalignment(lblMedarbejder, HPos.CENTER);
        setHalignment(btnMedarbejder, HPos.CENTER);
        
    }

    private void medarbejderListe() {
        obs = FXCollections.observableArrayList(Service.viewMedarbejdere());
        tblMedarb.setItems(obs);
//        lvwMedarbejdere.getItems().setAll(Service.viewMedarbejdere());

    }
    
}
