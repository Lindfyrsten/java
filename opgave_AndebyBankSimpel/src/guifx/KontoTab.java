package guifx;

import java.util.Optional;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import model.Konto;
import model.Kunde;
import service.Service;

/**
 * @author Kristian Lindbjerg
 */
public class KontoTab extends GridPane {
    private TextField txfKto, txfReg, txfTekst, txfSaldo, txfRenteInd, txfRenteUd, txfCpr, txfNavn;
    private Button btnOpret, btnKunde;
    private ListView<Konto> lvwKonto;
    private Label lblNavn;
    private Kunde k;
    
    public KontoTab() {
        setPadding(new Insets(10, 10, 10, 100));
        setVgap(20);
        setHgap(20);
        
        initContent();

    }

    private void initContent() {
        lblNavn = new Label("");
        Label lblKonto = new Label("Kontoer");
        lblKonto.setFont(Font.font("Arial", 18));
        
        txfKto = new TextField();
        txfKto.setPromptText("Kontonummer");
        txfKto.setDisable(true);
        txfReg = new TextField();
        txfReg.setPromptText("Reg. nr");
        txfReg.setDisable(true);
        txfSaldo = new TextField();
        txfSaldo.setPromptText("Saldo");
        txfSaldo.setDisable(true);
        txfRenteInd = new TextField();
        txfRenteInd.setPromptText("Indlånsrente");
        txfRenteInd.setDisable(true);
        txfRenteUd = new TextField();
        txfRenteUd.setPromptText("Udlånsrente");
        txfRenteUd.setDisable(true);
        txfNavn = new TextField();
        txfNavn.setDisable(true);
        txfCpr = new TextField();
        txfCpr.setDisable(true);
        txfTekst = new TextField();
        txfTekst.setPromptText("Konto navn");
        txfTekst.setDisable(true);
        
        btnOpret = new Button("Opret Konto");
        btnOpret.setDisable(true);
        btnOpret.setOnAction(event -> opretKonto());
        btnKunde = new Button("Vælg Kunde");
        btnKunde.setPrefSize(100, 30);
        btnKunde.setOnAction(event -> kundeValg());

        lvwKonto = new ListView<>();
        
//        setGridLinesVisible(true);
        add(btnKunde, 0, 1);
        add(lblNavn, 1, 1, 2, 1);
        add(txfReg, 0, 2);
        add(txfKto, 1, 2);
        add(txfSaldo, 0, 3);
        add(txfTekst, 1, 3);
        add(txfRenteInd, 0, 4);
        add(txfRenteUd, 1, 4);
        add(btnOpret, 1, 5);

        add(lblKonto, 2, 1);
        add(lvwKonto, 2, 2, 1, 4);
        setHalignment(btnOpret, HPos.LEFT);
        setHalignment(btnKunde, HPos.CENTER);
        setHalignment(lblKonto, HPos.CENTER);
        setValignment(lblKonto, VPos.BOTTOM);
        
    }
    
    private void opretKonto() {
        int ktoNr = Integer.parseInt(txfKto.getText().trim());
        int regNr = Integer.parseInt(txfReg.getText().trim());
        String tekst = txfTekst.getText();
        double saldo = Double.parseDouble(txfSaldo.getText().trim());
        double renteInd = Double.parseDouble(txfRenteInd.getText().trim());
        double renteUd = Double.parseDouble(txfRenteUd.getText().trim());
        Service.opretKonto(regNr, ktoNr, tekst, saldo, renteInd, renteUd, k);
        clear();
        lvwKonto.getItems().setAll(Service.getKontoer(k));
    }
    
    private void kundeValg() {
        ChoiceDialog<Kunde> kunder = new ChoiceDialog<>();
        kunder.setHeaderText("Vælg kunde");
        kunder.setContentText("Vælg kunde:");
        kunder.getItems().setAll(Service.getKunder());
        Optional<Kunde> result = kunder.showAndWait();
        if (result.isPresent()) {
            k = result.get();
            lblNavn.setText(k.getNavn() + " (CPR: " + k.getCprNr() + ")");
            txfKto.setDisable(false);
            txfRenteInd.setDisable(false);
            txfRenteUd.setDisable(false);
            txfReg.setDisable(false);
            txfSaldo.setDisable(false);
            txfTekst.setDisable(false);
            btnOpret.setDisable(false);
            clear();

        }
        lvwKonto.getItems().setAll(Service.getKontoer(k));

    }

    private void clear() {
        txfKto.clear();
        txfRenteInd.clear();
        txfRenteUd.clear();
        txfReg.clear();
        txfSaldo.clear();
        txfTekst.clear();
    }
}
